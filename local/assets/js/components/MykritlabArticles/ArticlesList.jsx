import React from 'react';
import PropTypes from 'prop-types';

const ArticlesItem = ({ item }) => (
  <div className="articles__listItem">
    <a href={item.href} title={item.name} className="articles__listLink">
      {item.name}
    </a>
  </div>
);

const ArticlesList = ({ items, event }) => {
  items.items.sort((a, b) => {
    const nameA = a.name.toLowerCase();
    const nameB = b.name.toLowerCase();
    if (nameA < nameB) {
      return -1;
    }
    if (nameA > nameB) {
      return 1;
    }
    return 0;
  });
  return (
    <div className="articles__elem js-article-elem">
      <div className="articles__caption" role="button" tabIndex={0} onKeyPress={(e) => event(e)} onClick={(e) => event(e)}>
        {items.name}
      </div>
      <div className="articles__list js-articles-list">
        <div className="articles__listWrap js-articles-list-wrap">
          {items.items.map((item) => (
            <ArticlesItem item={item} key={item.name + Math.random()} />
          ))}
        </div>
      </div>
    </div>
  );
};

ArticlesItem.propTypes = {
  item: PropTypes.shape({
    href: PropTypes.string.isRequired,
    name: PropTypes.string.isRequired,
  }).isRequired,
};

ArticlesList.propTypes = {
  items: PropTypes.shape({
    items: PropTypes.instanceOf(Array).isRequired,
    name: PropTypes.string.isRequired,
  }).isRequired,
  event: PropTypes.func.isRequired,
};

export default ArticlesList;
