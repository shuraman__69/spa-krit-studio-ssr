import React from "react";
import Header from "./Header";
import {useSelector} from "react-redux";
import {AppStateType} from "../../redux/store";


const HeaderContainer = () => {
    const {
        name,
        lastName
    } = useSelector((state: AppStateType): { name: string, lastName: string } => state.authPage.user)
    const menu = useSelector((state: AppStateType) => state.data.menu)

    return (
        <Header menu={menu} name={name} lastName={lastName}/>
    );
}

export default HeaderContainer
