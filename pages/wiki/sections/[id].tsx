import React from 'react';
import MainLayout from "../../../components/MainLayout";
import MykritlabArticles from "../../../components/MykritlabArticles/MykritlabArticles";
import {NextThunk, wrapper} from "../../../redux/store";
import {setSelectedSectionThunk} from "../../../redux/reducers/wikiReducer";
import {getUserInfoThunk} from "../../../redux/reducers/authReducer";

const Index = () => {
    return (
        <MainLayout title='Sections | NextJS'>
            <MykritlabArticles/>
        </MainLayout>
    );
}
export default Index;


export const getServerSideProps = wrapper.getServerSideProps(async ({store, params,}) => {
    const dispatch = store.dispatch as NextThunk
    const id: string = params.id.toString()
    await dispatch(await getUserInfoThunk())
    await dispatch(setSelectedSectionThunk(id))
    return {
        props: {}
    }
})
