import React from 'react';
import PropTypes from 'prop-types';
import Search from './Search';
import Logo from './Logo';
import Menu from './Menu';
import User from './User';
import Icon from './Icon';

const MobileMenu = ({ items, clickEvent }) => {
  const isSimpleHead = document.querySelector('.header--simple') !== null;

  return (
    <div className="header__middleWrap">
      <div className="header__middleTop">
        <Icon clickEvent={(e) => clickEvent(e)} />
        <User />
      </div>
      <Logo />
      {!isSimpleHead && <Search />}
      <Menu items={items} />
    </div>
  );
};

MobileMenu.propTypes = {
  items: PropTypes.instanceOf(Array).isRequired,
  clickEvent: PropTypes.func.isRequired,
};

export default MobileMenu;
