import React, { Component } from 'react';

const changeTheme = (status) => {
  const body = document.querySelector('body');
  body.classList[status ? 'add' : 'remove']('dark-theme');
};

class SwitchTheme extends Component {
  constructor(props) {
    super(props);
    this.DARKTHEME = false;
    this.changeInput = React.createRef();
  }

  componentDidMount() {
    this.getUserTheme();
  }

  setInputState() {
    const checked = this.DARKTHEME === true;
    this.changeInput.current.checked = checked;
    changeTheme(checked);
  }

  setChecked(e) {
    e.preventDefault();
    if (!this.changeInput.current.checked) {
      this.changeInput.current.click();
    }
  }

  setUnchecked(e) {
    e.preventDefault();
    if (this.changeInput.current.checked) {
      this.changeInput.current.click();
    }
  }

  setUserTheme() {
    const data = {
      DARKTHEME: this.DARKTHEME,
    };
    const xhr = new XMLHttpRequest();

    const formData = new FormData();

    formData.append('AJAX', 'Y');
    formData.append('PARAMS', JSON.stringify(data));

    xhr.open('GET', '../../api/v3/user/settings.json');
    xhr.send(formData);

    // xhr.onload = function (response) {
    //   const result = JSON.parse(response.currentTarget.response);

    //   if (xhr.status === 200) {}
    // };

    xhr.onerror = function () {
      console.log('ошибка');
    };
  }

  getUserTheme() {
    const self = this;
    const xhr = new XMLHttpRequest();

    const formData = new FormData();

    formData.append('AJAX', 'Y');

    xhr.open('GET', '../../api/v3/user/settings.json');
    xhr.send(formData);

    xhr.onload = function (response) {
      const result = JSON.parse(response.currentTarget.response);

      if (xhr.status === 200) {
        if (Object.keys(result).length > 0) {
          self.DARKTHEME = result.DARKTHEME;
          self.setInputState();
        }
      }
    };
  }

  changeInputState(e) {
    const { checked } = e.target;
    changeTheme(checked);
    this.DARKTHEME = checked;
    this.setUserTheme();
  }

  render() {
    return (
      <div className="switcher">
        <button
          type="button"
          className="switcherButton switcherButton--light"
          onClick={(e) => this.setUnchecked(e)}
        >
          Light
        </button>
        <label htmlFor="switcherInput" className="switcherCheck">
          <input
            id="switcherInput"
            type="checkbox"
            className="switcherInput"
            onChange={(e) => this.changeInputState(e)}
            ref={this.changeInput}
          />
          <span className="switcherToggle" />
        </label>
        <button
          type="button"
          className="switcherButton switcherButton--dark"
          onClick={(e) => this.setChecked(e)}
        >
          Dark
        </button>
      </div>
    );
  }
}

export default SwitchTheme;
