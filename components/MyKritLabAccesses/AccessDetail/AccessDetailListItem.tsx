import React, {FC, useState} from 'react';
import PropTypes from 'prop-types';
import Parser from 'html-react-parser';
import classNames from 'classnames';
import {selectedAccessItemType} from "../../../_types";
import AccessDetailFormEdit from "./AccessDetailFormEdit";

type PropsType = {
    item: selectedAccessItemType
    openDeleteModal: (id: number) => void
    setEditModalIsOpen: (flag: boolean) => void
    openEditModal: (id: number) => void
    editModalIsOpen: boolean
    closeModals: () => void
    likeEvent: (id:number,itemLike:boolean) => void
    event: (e: any) => void
}
const AccessDetailListItem: FC<PropsType> =
    ({
         item, event, likeEvent, openDeleteModal, openEditModal, editModalIsOpen, closeModals, setEditModalIsOpen
     }) => {
        return (

            <div className="accessDetail__item js-copy-row" data-id={item.id}>
                <div className="accessDetail__itemCol accessDetail__itemCol--type">
                    <div className="accessDetail__itemInner">{Parser(item.type)}</div>
                </div>
                <div className="accessDetail__itemCol accessDetail__itemCol--gray accessDetail__itemCol--host">
                    <div
                        className="accessDetail__itemInner js-copy-element"
                        role="button"
                        tabIndex={0}
                        onKeyPress={(e) => event(e)}
                        onClick={(e) => event(e)}
                    >
                        {item.host}
                    </div>
                </div>
                <div className="accessDetail__itemCol accessDetail__itemCol--gray accessDetail__itemCol--notice">
                    <div className="accessDetail__itemInner">
                        <div>{item.notice ? item.notice : '\u2014'}</div>
                    </div>
                </div>
                <div className="accessDetail__itemCol accessDetail__itemCol--login">
                    <div
                        className="accessDetail__itemInner js-copy-element"
                        role="button"
                        tabIndex={0}
                        onKeyPress={(e) => event(e)}
                        onClick={(e) => event(e)}
                    >
                        {item.login}
                    </div>
                </div>
                <div className="accessDetail__itemCol accessDetail__itemCol--pass">
                    <div
                        className="accessDetail__itemInner js-copy-element"
                        role="button"
                        tabIndex={0}
                        onKeyPress={(e) => event(e)}
                        onClick={(e) => event(e)}
                    >
                        {item.password}
                    </div>
                </div>
                <div className="accessDetail__itemCol accessDetail__itemCol--actions">
                    <div className="accessDetail__itemInner">
                        <button type="button" className="accessDetail__icon copyIcon" onClick={(e) => event(e)}>
                            <svg
                                width="19"
                                height="20"
                                viewBox="0 0 19 20"
                                fill="none"
                                xmlns="http://www.w3.org/2000/svg"
                            >
                                <path
                                    d="M13.5776 19.0916H0.682338C0.305402 19.0916 0 18.7862 0 18.4093V5.51437C0 5.13743 0.305402 4.83203 0.682338 4.83203H13.5776C13.9545 4.83203 14.2599 5.13773 14.2599 5.51437V18.4093C14.2599 18.7859 13.9545 19.0916 13.5776 19.0916Z"/>
                                <path
                                    d="M18.3173 14.2599H16.9319C16.555 14.2599 16.2496 13.9542 16.2496 13.5776C16.2496 13.2006 16.555 12.8952 16.9319 12.8952H17.635V1.36497H6.10442V2.1241C6.10442 2.50103 5.79902 2.80643 5.42208 2.80643C5.04515 2.80643 4.73975 2.50074 4.73975 2.1241V0.682338C4.73975 0.305694 5.04515 0 5.42208 0H18.3173C18.6942 0 18.9997 0.305694 18.9997 0.682338V13.5776C18.9997 13.9542 18.6942 14.2599 18.3173 14.2599Z"/>
                            </svg>
                        </button>
                        <button
                            type="button"
                            className={classNames({
                                accessDetail__icon: true,
                                likeIcon: true,
                                'likeIcon--liked': item.like,
                            })}
                            onClick={()=>likeEvent(item.id,item.like)}
                        >
                            <svg
                                className="likeIcon__filled"
                                width="16"
                                height="14"
                                viewBox="0 0 16 14"
                                fill="none"
                                xmlns="http://www.w3.org/2000/svg"
                            >
                                <path
                                    d="M15.5542 6.8769C15.8272 6.16067 16 5.40528 16 4.63309C16 4.05116 15.8941 3.4972 15.6991 2.98241C15.66 2.88169 15.621 2.78657 15.5765 2.69145C15.5374 2.60192 15.4929 2.51239 15.4483 2.42846C15.3758 2.29976 15.2978 2.17106 15.2198 2.04796C15.1362 1.92486 15.047 1.80735 14.9523 1.68985C14.9077 1.63389 14.8575 1.57794 14.8074 1.52198C14.6068 1.30376 14.3894 1.10232 14.1553 0.923261C14.0383 0.833733 13.9157 0.7498 13.7875 0.671463C13.0909 0.246203 12.2717 0 11.3967 0C11.1905 0 10.9843 0.0111909 10.7837 0.0391685C10.7447 0.044764 10.7057 0.0503597 10.6667 0.0559552C10.5886 0.0671463 10.5106 0.083933 10.4326 0.10072C10.3936 0.106315 10.3546 0.117506 10.3156 0.128697C10.1372 0.173461 9.9589 0.223821 9.78614 0.290967C9.10623 0.542766 8.49321 0.956834 8.00279 1.494C7.14455 0.553957 5.92964 0.00559587 4.60885 0.00559587C2.06757 0.00559587 0 2.08154 0 4.63309C0 7.14548 2.21247 9.78657 3.16545 10.805C4.54197 12.2766 6.63741 14 8.00836 14C9.32358 14 11.3856 12.4724 12.8401 10.9616C13.598 10.1727 14.6904 8.8745 15.3758 7.34692C15.4037 7.27978 15.4316 7.21823 15.4594 7.15108C15.4873 7.05596 15.5207 6.96643 15.5542 6.8769Z"/>
                            </svg>
                            <svg
                                className="likeIcon__bordered"
                                width="16"
                                height="14"
                                viewBox="0 0 16 14"
                                fill="none"
                                xmlns="http://www.w3.org/2000/svg"
                            >
                                <path
                                    d="M11.4417 0.627279L11.4417 0.627291L11.4456 0.627059C11.4771 0.625184 11.5117 0.625 11.5679 0.625C13.6632 0.625 15.3777 2.35078 15.375 4.48655V4.48734C15.375 4.68892 15.3614 4.89509 15.3337 5.10923C15.3327 5.11687 15.3316 5.12431 15.3306 5.13158C15.3222 5.19157 15.3154 5.2399 15.3067 5.28826L15.3066 5.28825L15.3052 5.29722C15.2952 5.35777 15.2824 5.42038 15.2667 5.49661L15.2309 5.65981C15.0893 6.26882 14.8373 6.91452 14.469 7.5948L14.4687 7.59525C13.9554 8.54505 13.2154 9.54202 12.328 10.4741C11.4878 11.3553 10.5991 12.0959 9.80265 12.6114C8.97584 13.1464 8.34549 13.375 7.99844 13.375C7.65762 13.375 7.05469 13.1519 6.14936 12.5222C5.35455 11.9667 4.47207 11.1846 3.67171 10.3222C3.20354 9.81595 2.43055 8.91498 1.77796 7.84504C1.11754 6.76226 0.625 5.5781 0.625 4.49051C0.625 2.3508 2.34318 0.625 4.43524 0.625C5.16086 0.625 5.86519 0.833342 6.47752 1.22733C6.87588 1.48641 7.22013 1.81333 7.49721 2.19191L8.00337 2.8835L8.50717 2.19019C8.78406 1.80915 9.12753 1.48331 9.52421 1.22822L9.5243 1.22835L9.53582 1.22057C9.56093 1.20363 9.59065 1.18502 9.61965 1.1679L9.61966 1.16791L9.62337 1.16569C10.1305 0.861429 10.694 0.680339 11.2774 0.63635C11.3318 0.63329 11.3854 0.630282 11.4417 0.627279Z"
                                    strokeWidth="1.25"
                                />
                            </svg>
                        </button>
                        {item.canChange &&
                        <>
                            <button type="button" className={classNames({
                                "accessDetail__icon editIcon": true
                            })} onClick={() => openEditModal(item.id)}>
                                <svg
                                    width="20"
                                    height="20"
                                    viewBox="0 0 20 20"
                                    fill="none"
                                    xmlns="http://www.w3.org/2000/svg"
                                >
                                    <path
                                        fillRule="evenodd"
                                        clipRule="evenodd"
                                        d="M18.1582 4.90611L17.3957 5.66657L16.8876 6.17475L16.3794 6.68293L15.8732 7.19111L15.1099 6.42984L14.6017 5.92166L13.0772 4.39711L12.571 3.88893L11.8077 3.12766L12.3159 2.61947L12.8241 2.11129L13.3323 1.60311L14.0936 0.839844L14.0944 0.84067C15.2164 -0.280496 17.0353 -0.280223 18.159 0.841488C19.283 1.96347 19.281 3.78296 18.159 4.90694L18.1582 4.90611ZM7.75 17C7.33579 17 7 17.3358 7 17.75C7 18.1642 7.33579 18.5 7.75 18.5H16.25C16.6642 18.5 17 18.1642 17 17.75C17 17.3358 16.6642 17 16.25 17H7.75ZM14.989 8.07599L6.08141 16.9784L5.60394 17.4556L5.19739 17.8622L5.16844 17.8332L5 18.0016L0.540079 18.9752L0.468336 18.9912C0.195313 19.0549 -0.0518026 18.8098 0.013962 18.5368L0.029905 18.4651C0.005981 18.0943 0.666667 15.3349 1 14.0016L1.17236 13.8292L1.13991 13.7967L1.54646 13.3902L10.9239 4.01279L11.8077 3.12891L12.569 3.89018L12.6916 4.01279L13.0772 4.39836L14.6017 5.92291L14.9883 6.30948L15.1099 6.43109L15.8731 7.19233L15.8732 7.19236L14.989 8.07599ZM14.1042 7.1931L5.1984 16.0937L2.90681 13.7976L11.8077 4.89667L12.1933 5.28225L13.7179 6.80679L14.1042 7.1931Z"
                                    />
                                </svg>
                            </button>
                            <button type="button" className={classNames({
                                "accessDetail__icon deleteIcon": true
                            })} onClick={() => openDeleteModal(item.id)}>
                                <svg
                                    width="15"
                                    height="15"
                                    viewBox="0 0 15 15"
                                    fill="none"
                                    xmlns="http://www.w3.org/2000/svg"
                                >
                                    <path
                                        d="M8.28151 7.99716L13.7326 2.54609C14.0858 2.1929 14.0858 1.61843 13.7326 1.26524C13.3794 0.912051 12.8049 0.912051 12.4517 1.26524L7.00065 6.71631L1.54959 1.26099C1.1964 0.907796 0.62193 0.907796 0.268739 1.26099C-0.0844527 1.61418 -0.0844527 2.18865 0.268739 2.54184L5.7198 7.9929L0.268739 13.4482C-0.0844527 13.8014 -0.0844527 14.3759 0.268739 14.7291C0.447462 14.9078 0.677249 14.9929 0.911292 14.9929C1.14533 14.9929 1.37512 14.9035 1.55385 14.7291L7.00491 9.27801L12.456 14.7291C12.6347 14.9078 12.8645 14.9929 13.0985 14.9929C13.3326 14.9929 13.5624 14.9035 13.7411 14.7291C14.0943 14.3759 14.0943 13.8014 13.7411 13.4482L8.28151 7.99716Z"/>
                                </svg>
                            </button>
                        </>
                        }
                    </div>
                </div>
                <AccessDetailFormEdit
                    closeModals={closeModals}
                    setEditModalIsOpen={setEditModalIsOpen}
                    open={editModalIsOpen}
                />
            </div>
        );
    }

export default AccessDetailListItem;
