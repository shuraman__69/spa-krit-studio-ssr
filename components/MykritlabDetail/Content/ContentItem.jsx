import React from 'react';
import PropTypes from 'prop-types';
import PerfectScrollbar from 'react-perfect-scrollbar';
import Parser from 'html-react-parser';

const ContentItemRow = ({
                            item, parent, child, event,
                        }) => (
    <div className="post__contentRow" data-id={`p${parent}c${child}`}>
        {item.text && (
            <div className="post__contentText">{Parser(item.text)}</div>
        )}
        {item.code && (
            <div className="post__contentCode js-code">
                <PerfectScrollbar>
                    <div className="post__contentCodeStyled js-copy-code">{Parser(item.code)}</div>
                </PerfectScrollbar>
                <button type="button" className="post__contentCodeCopy" onClick={(e) => event(e)}>
                    <svg viewBox="0 0 16 16" fill="none" xmlns="http://www.w3.org/2000/svg">
                        <path
                            d="M11.6623 15.2749H1.34616C1.04461 15.2749 0.800293 15.0306 0.800293 14.729V4.41306C0.800293 4.11151 1.04461 3.86719 1.34616 3.86719H11.6623C11.9639 3.86719 12.2082 4.11174 12.2082 4.41306V14.729C12.2082 15.0303 11.9639 15.2749 11.6623 15.2749ZM1.89203 14.1829H11.1162V4.95893H1.89203V14.1829Z"/>
                        <path
                            d="M15.4546 11.4079H14.3463C14.0447 11.4079 13.8004 11.1634 13.8004 10.8621C13.8004 10.5605 14.0447 10.3162 14.3463 10.3162H14.9087V1.09197H5.68427V1.69928C5.68427 2.00082 5.43995 2.24515 5.1384 2.24515C4.83685 2.24515 4.59253 2.00059 4.59253 1.69928V0.54587C4.59253 0.244556 4.83685 0 5.1384 0H15.4546C15.7561 0 16.0005 0.244556 16.0005 0.54587V10.8621C16.0005 11.1634 15.7561 11.4079 15.4546 11.4079Z"/>
                    </svg>
                </button>
            </div>
        )}
    </div>
);
const ContentItem = ({item, event, parent}) => {
    const captionExist = item.caption.length > 0;
    const hasChildren = item.description.length > 0;
    return (
        <div className="post__contentItem" data-id={`p${parent}`}>
            {captionExist && <h3 className="post__contentTitle">{item.caption}</h3>}
            {hasChildren
            && item.description.map((elem, key) => (
                <ContentItemRow
                    item={elem}
                    event={event}
                    parent={parent}
                    child={key}
                    key={elem.text + Math.random()}
                />
            ))}
        </div>
    );
};

ContentItemRow.propTypes = {
    event: PropTypes.func.isRequired,
    child: PropTypes.number.isRequired,
    parent: PropTypes.number.isRequired,
    item: PropTypes.shape({
        text: PropTypes.string.isRequired,
        code: PropTypes.string.isRequired,
    }).isRequired,
};

ContentItem.propTypes = {
    event: PropTypes.func.isRequired,
    parent: PropTypes.number.isRequired,
    item: PropTypes.shape({
        description: PropTypes.instanceOf(Array).isRequired,
        caption: PropTypes.string.isRequired,
    }).isRequired,
};

export default ContentItem;
