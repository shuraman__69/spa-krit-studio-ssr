import React, {FC} from 'react';
import classNames from 'classnames';

import AccessDetailListItem from './AccessDetailListItem';
import {selectedAccessItemType} from "../../../_types";


type PropsType = {
    items: selectedAccessItemType[]
    openDeleteModal: (id: number) => void
    openEditModal: (id: number) => void
    setEditModalIsOpen:(flag:boolean) => void
    editModalIsOpen: boolean
    closeModals: () => void
    likeEvent: (id:number,itemLike:boolean) => void
    event: (e: any) => void
}

const AccessDetailList:
    FC<PropsType> = (
    {
        items,
        openDeleteModal,
        likeEvent,
        event,
        editModalIsOpen,
        setEditModalIsOpen,
        openEditModal,
        closeModals
    }) => {
    const notice = items.filter((item) => item.notice.length > 0);
    return (
        <div
            className={classNames({
                accessDetail__list: true,
                'accessDetail__list--wide': notice.length > 0,
            })}
        >
            <div className="accessDetail__listWrap">
                <div className="accessDetail__item accessDetail__item--head">
                    <div
                        className="accessDetail__itemCol accessDetail__itemCol--head accessDetail__itemCol--type"
                        data-title="Вид"
                    />
                    <div
                        className="accessDetail__itemCol accessDetail__itemCol--head accessDetail__itemCol--gray accessDetail__itemCol--host"
                        data-title="Хост"
                    />
                    <div
                        className="accessDetail__itemCol accessDetail__itemCol--head accessDetail__itemCol--gray accessDetail__itemCol--notice"
                        data-title="Прим."
                        data-title-sm="Примечания"
                    />
                    <div
                        className="accessDetail__itemCol accessDetail__itemCol--head accessDetail__itemCol--border accessDetail__itemCol--login"
                        data-title="Логин"
                    />
                    <div
                        className="accessDetail__itemCol accessDetail__itemCol--head accessDetail__itemCol--border accessDetail__itemCol--pass"
                        data-title="Пароль"
                    />
                    <div className="accessDetail__itemCol accessDetail__itemCol--head accessDetail__itemCol--actions"/>
                </div>
                {items.map((item) => (
                    <AccessDetailListItem
                        openDeleteModal={openDeleteModal}
                        openEditModal={openEditModal}
                        editModalIsOpen={editModalIsOpen}
                        setEditModalIsOpen={setEditModalIsOpen}
                        likeEvent={likeEvent}
                        closeModals={closeModals}
                        item={item}
                        event={event}
                        key={item.id + Math.random()}
                    />
                ))}
            </div>
        </div>
    );
};


export default AccessDetailList;
