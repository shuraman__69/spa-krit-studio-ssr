import React, {FC} from "react";
import Logo from "./Logo";
import User from "./User";
import Icon from "./Icon";
import MobileMenu from "./MobileMenu";
import Menu from "./Menu";
import {menuType} from "../../redux/reducers/menuRudecer";
import classNames from "classnames";
import {useHeader} from "../../hooks/useHeader";

export type HeaderPropsType = {
    name: string
    lastName: string
}
const Header: FC<HeaderPropsType & { menu: menuType[] }> = ({name, lastName, menu}) => {
const {pathname,onClickHandler}=useHeader()
    if (pathname === '/' || pathname === '/auth') {
        return (
            <div className="header__wrapper">
                <div className={classNames({
                    "wrapper header__inner": true,
                    "header__isAuth": pathname === '/auth'
                })}>
                    <Logo/>
                    {name && name.length !== 0 && <User name={name} lastName={lastName}/>}
                </div>
            </div>
        );
    }
    return (
        <div className="header">
            <div className="header__wrapper header__wrapper--secondary">
                <div className="wrapper">
                    <div className="header__left">
                        {name.length === 0 ? undefined :
                            <Icon
                                clickEvent={() => onClickHandler()}/>}
                        <Logo/>
                    </div>
                    <div className="header__middle js-side-wrapper">
                        <MobileMenu name={name} lastName={lastName} items={menu}
                                    clickEvent={() => onClickHandler()}/>
                        <div className="header__close">
                            <button type="button" className="header__closeLink js-close-side"
                                    onClick={() => onClickHandler()} title="Закрыть">
                                <svg width="37" height="11" viewBox="0 0 37 11" fill="none"
                                     xmlns="http://www.w3.org/2000/svg">
                                    <path d="M1 10L18.5 1.5L35.5 10" stroke="#E7E7F4" strokeWidth="2"
                                          strokeLinecap="round"/>
                                </svg>
                            </button>
                        </div>
                    </div>
                    <div className="header__right">
                        {name && name.length !== 0 && <User name={name} lastName={lastName}/>}
                    </div>
                </div>
            </div>
            <div className="header__bottom js-side-wrapper">
                <div className="wrapper">
                    <Menu items={menu}/>
                    <div className="header__close">
                        <button type="button" className="header__closeLink js-close-side"
                                onClick={() => onClickHandler()} title="Закрыть">
                            <svg width="37" height="11" viewBox="0 0 37 11" fill="none"
                                 xmlns="http://www.w3.org/2000/svg">
                                <path d="M1 10L18.5 1.5L35.5 10" stroke="#E7E7F4" strokeWidth="2"
                                      strokeLinecap="round"/>
                            </svg>
                        </button>
                    </div>
                </div>
            </div>
        </div>
    );
}

export default Header
