import React, { Component, Fragment } from 'react';
import PropTypes from 'prop-types';

export class BreadCrumbs extends Component {
  constructor(props) {
    super(props);
    this.state = {
      breadcrumbs: [],
    };
  }

  componentDidMount() {
    this.getBreadcrumbs();
  }

  getBreadcrumbs() {
    const self = this;
    const xhr = new XMLHttpRequest();

    const formData = new FormData();

    formData.append('AJAX', 'Y');
    formData.append('PARAMS', JSON.stringify({ URL: window.location.pathname }));

    xhr.open('GET', '../../api/v3/breadcrumbs.json');
    xhr.send(formData);

    xhr.onload = function (response) {
      const result = JSON.parse(response.currentTarget.response);

      if (xhr.status === 200) {
        self.setState({
          breadcrumbs: [...result],
        });
      }
    };

    xhr.onerror = function () {
      console.log('ошибка');
    };
  }

  render() {
    const { breadcrumbs } = this.state;
    const { value } = this.props;
    return (
      <div className="breadCrumbs">
        <ul className="breadCrumbs__list">
          {breadcrumbs.map((item) => (
            <Fragment key={item.label + Math.random()}>
              <li className="breadCrumbs__item">
                <a href={item.href} title={item.label} className="breadCrumbs__link">
                  {item.label}
                </a>
              </li>
              <li className="breadCrumbs__item">
                <span className="breadCrumbs__link breadCrumbs__link--next">
                  <svg
                    width="16"
                    height="6"
                    viewBox="0 0 16 6"
                    fill="none"
                    xmlns="http://www.w3.org/2000/svg"
                  >
                    <path d="M13 1L15 3M15 3L13 5M15 3H0.5" stroke="#8EADFF" />
                  </svg>
                </span>
              </li>
            </Fragment>
          ))}
          <li className="breadCrumbs__item">
            <span className="breadCrumbs__link breadCrumbs__link--last">{value}</span>
          </li>
        </ul>
      </div>
    );
  }
}

BreadCrumbs.propTypes = {
  value: PropTypes.string.isRequired,
};

