import React, {useEffect, useState} from 'react';
import Slider from 'react-slick';
import {useSelector} from "react-redux";
import {menuType} from "../../redux/reducers/menuRudecer";
import MykritlabMenuReactItem from "./MykritlabMenuReactItem";
import {AppStateType} from "../../redux/store";
import MykritlabMenuItem from "./MykritlabMenuItem";

const MyKritLabMenu = () => {
    const {menu} = useSelector((state: AppStateType) => state.data)

    const [slider, _setSlider] = useState<boolean>(false)
    //настройки слайдера
    const settings = {
        dots: true,
        arrows: false,
        infinite: true,
        speed: 500,
        slidesToShow: 1,
        slidesToScroll: 1,
    }

    const setSlider = () => {
        if (window.innerWidth < 768 && !slider) {
            _setSlider(true)
        }
        if (window.innerWidth >= 768 && slider) {
            _setSlider(false)
        }
    };
    useEffect(() => window.removeEventListener('resize', setSlider))

    useEffect(() => {
        setSlider();
        window.addEventListener('resize', setSlider)
    })


    const renderItems = () => {
        return menu.map((item: menuType) => {
            if (item.react) {
                return (
                    <MykritlabMenuReactItem item={item} key={item.id}/>
                )
            }
            return (
                <MykritlabMenuItem item={item} key={item.id}/>
            )
        })
    }

    return (
        <div className="menuSectionListWrap">
            <div className="menuSectionList">
                <h1 className="title title--main">Лаборатория разработки KRIT.STUDIO</h1>
                <div className="menuSectionList__slider js-menu-slider">
                    {slider && <Slider {...settings}>{renderItems()}</Slider>}
                    {!slider && renderItems()}
                </div>
            </div>
        </div>
    );
}


export default MyKritLabMenu;
