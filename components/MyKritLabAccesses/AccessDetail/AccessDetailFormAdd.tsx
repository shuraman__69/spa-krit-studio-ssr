import React, {FC} from 'react';
import Select from 'react-select';
import RenderModal from '../../other/RenderModal';
import {useAccess} from "../../../hooks/useAccess";
import {useSelectValues} from "../../../hooks/useSelectValues";

type PropsType = {
    open: boolean
    setAddModalIsOpen: (flag: boolean) => void
    closeModals: () => void
}

const AccessDetailFormAdd: FC<PropsType> =
    ({
         open,
         setAddModalIsOpen,
         closeModals
     }) => {
        const {onChangeHandler, addAccess, type, host, login, password} = useAccess()
        const {values} = useSelectValues()
        return (
            <div className="accessDetail__add">
                <button type="button" className="accessDetail__addLink" onClick={() => setAddModalIsOpen(true)}>
      <span>
        <svg
            width="31"
            height="11"
            viewBox="0 0 31 11"
            fill="none"
            xmlns="http://www.w3.org/2000/svg"
        >
          <path
              d="M1.25 6.0013H29.5833M29.5833 6.0013L25.4167 1.41797M29.5833 6.0013L25.4167 10.168"
              stroke="white"
              strokeWidth="1.5"
              strokeLinecap="round"
          />
        </svg>
      </span>


                    Добавить новый доступ
                </button>
                <RenderModal isOpen={open} closeModals={closeModals}>
                    <div className="addNewAccess">
                        <div className="addNewAccess__form formBlock" data-form="add">
                            <div className="formBlock__title title">Создать новый доступ</div>
                            <div className="formBlock__content">
                                <div className="formBlock__row">
                                    <div className="formBlock__rowWrap">
                                        <div className="inputHolder">
                                            <input
                                                type="text"
                                                name="host"
                                                className="inputHolder__input"
                                                placeholder="Хост"
                                                onChange={(e) => onChangeHandler(e)}
                                            />
                                        </div>
                                    </div>
                                </div>
                                <div className="formBlock__row">
                                    <div className="formBlock__rowWrap">
                                        <div className="inputHolder">
                                            <Select
                                                name="type"
                                                options={values}
                                                placeholder="Вид доступа"
                                                isSearchable={false}
                                                className="styledSelect"
                                                classNamePrefix="styledSelect"
                                                onChange={(e) => onChangeHandler(e)}
                                            />
                                        </div>
                                    </div>
                                </div>
                                <div className="formBlock__row">
                                    <div className="formBlock__rowWrap">
                                        <div className="inputHolder">
                                            <input
                                                type="text"
                                                name="login"
                                                className="inputHolder__input"
                                                placeholder="Логин"
                                                onChange={(e) => onChangeHandler(e)}
                                            />
                                        </div>
                                    </div>
                                </div>
                                <div className="formBlock__row">
                                    <div className="formBlock__rowWrap">
                                        <div className="inputHolder">
                                            <input
                                                type="text"
                                                name="password"
                                                className="inputHolder__input"
                                                placeholder="Пароль"
                                                onChange={(e) => onChangeHandler(e)}
                                            />
                                        </div>
                                    </div>
                                </div>
                                <div className="formBlock__row">
                                    <div className="formBlock__rowWrap">
                                        <div className="inputHolder">
                                            <input type="text" className="inputHolder__input"
                                                   name='database'
                                                   onChange={(e) => onChangeHandler(e)}
                                                   placeholder="База данных"/>
                                        </div>
                                    </div>
                                </div>
                                <div className="formBlock__row">
                                    <div className="formBlock__rowWrap">
                                        <div className="inputHolder">
                                            <input
                                                type="text"
                                                name="port"
                                                className="inputHolder__input"
                                                placeholder="Порт"
                                                onChange={(e) => onChangeHandler(e)}
                                            />
                                        </div>
                                    </div>
                                </div>
                                <div className="formBlock__row formBlock__row--textarea">
                                    <div className="formBlock__rowWrap">
                                        <div className="inputHolder">
                  <textarea
                      name="notice"
                      className="inputHolder__input inputHolder__input--textarea"
                      placeholder="Примечания"
                      onChange={(e) => onChangeHandler(e)}
                  />
                                        </div>
                                    </div>
                                </div>
                                <div className="formBlock__row formBlock__row--btn">
                                    <div className="formBlock__rowWrap">
                                        <button onClick={() => {
                                            addAccess()
                                            setAddModalIsOpen(false)
                                        }} className="addNewAccess__btn"
                                                disabled={!(host && login && password && type)}>
                  <span>
                    <svg
                        width="36"
                        height="13"
                        viewBox="0 0 31 11"
                        fill="none"
                        xmlns="http://www.w3.org/2000/svg"
                    >
                      <path
                          d="M1.25 6.0013H29.5833M29.5833 6.0013L25.4167 1.41797M29.5833 6.0013L25.4167 10.168"
                          stroke="white"
                          strokeWidth="1.5"
                          strokeLinecap="round"
                      />
                    </svg>
                  </span>
                                            Создать
                                        </button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </RenderModal>
            </div>
        );
    }


export default AccessDetailFormAdd;
