import {InferActionsType} from "../store";
import {settingsAPI, userAPI} from "../../api/api";
import {getMenuThunk} from "./menuRudecer";
import {setSelectedThunk} from "./wikiReducer";

const SET_USER_LINK = 'SET_USER_LINK';
const SET_USER_INFO = 'SET_USER_INFO';
const SET_USER_THEME = 'SET_USER_THEME';
const ERROR = 'ERROR'
const LOADING_TRUE = 'LOADING_TRUE'
const LOADING_FALSE = 'LOADING_FALSE'


export type userInfoType = {
    name: string
    secondName: string
    lastName: string
    email: string
    login: string
}
export type InitialAuthStateType = {
    user: userInfoType
    userLink: string
    DARKTHEME: boolean
    ERROR: boolean
    isLoading: boolean
};
const initialState: InitialAuthStateType = {
    userLink: '',
    DARKTHEME: false,
    user: {
        name: '',
        secondName: '',
        lastName: '',
        email: '',
        login: '',
    },
    ERROR: false,
    isLoading: true
}
const authReducer = (state = initialState, action: ActionsType) => {
    switch (action.type) {
        case SET_USER_LINK:
            return {
                ...state,
                userLink: action.link
            }
        case SET_USER_INFO:
            return {
                ...state,
                user: {...action.userInfo}

            }
        case SET_USER_THEME:
            return {
                ...state,
                DARKTHEME: action.darkTheme
            }
        case ERROR:
            return {
                ...state,
                ERROR: true
            }
        case LOADING_TRUE:
            return {
                ...state,
                isLoading: true
            }
        case LOADING_FALSE:
            return {
                ...state,
                isLoading: false
            }
        default:
            return state;
    }
}
type ActionsType = InferActionsType<typeof actions>
export const actions = {
    setUserLink: (link: string) => ({type: SET_USER_LINK, link}) as const,
    setUserInfo: (userInfo: userInfoType) => ({type: SET_USER_INFO, userInfo}) as const,
    setUserTheme: (darkTheme: boolean) => ({type: SET_USER_THEME, darkTheme}) as const,
    SET_ERROR: () => ({type: ERROR}) as const,
    SET_LOADING_TRUE: () => ({type: LOADING_TRUE}) as const,
    SET_LOADING_FALSE: () => ({type: LOADING_FALSE}) as const,
}



export const SET_ERROR = () => {
    return async (dispatch: any) => {
        await dispatch(actions.SET_ERROR())
    }
}
export const SET_LOADING_TRUE = () => {
    return async (dispatch: any) => {
        await dispatch(actions.SET_LOADING_TRUE())
    }
}
export const SET_LOADING_FALSE = () => {
    return async (dispatch: any) => {
        await dispatch(actions.SET_LOADING_FALSE())
    }
}

export const changeUserThemeThunk = (newTheme: boolean) => {
    return async (dispatch: any) => {
        try {
            const theme = await settingsAPI.changeUserTheme(newTheme)
            if (theme.status === 200) {
                dispatch(actions.setUserTheme(theme.darkTheme))
            }
        } catch (error) {
            await dispatch(SET_ERROR())
        }
    }
}
export const logoutThunk = () => {
    return async (dispatch: any) => {
        try {
            const defaultUserInfo = {
                name: '',
                secondName: '',
                lastName: '',
                email: '',
                login: '',
            }

            await userAPI.logout()
            await dispatch(actions.setUserInfo(defaultUserInfo))
        } catch (error) {
            await dispatch(SET_ERROR())
        }
    }
}
export const getUserInfoThunk = () => {
    return async (dispatch: any) => {
        try {
            await dispatch(SET_LOADING_TRUE())
            let userInfo = await userAPI.getUserInfo()
            debugger
            if (userInfo.status === 403) {
                //ссылка
                const link = await userAPI.getAuthLink()
                await dispatch(actions.setUserLink(link))
            } else if (userInfo.status === 200) {
                //user info
                await dispatch(actions.setUserInfo(userInfo.userInfo))
                const theme = await settingsAPI.getUserTheme()
                await dispatch(actions.setUserTheme(theme.darkTheme))
            }
        } catch (error) {
            await dispatch(SET_ERROR())
        } finally {
            await dispatch(SET_LOADING_FALSE())
        }
    }
}

export default authReducer;