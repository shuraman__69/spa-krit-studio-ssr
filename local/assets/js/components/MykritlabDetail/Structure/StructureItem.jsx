import React from 'react';
import PropTypes from 'prop-types';

const StructureChildItem = ({
  item, parent, child, event,
}) => {
  const { isAnchor } = item;
  if (!isAnchor) {
    return false;
  }
  return (
    <li className="post__structureChildrenItem">
      <button
        type="button"
        className="post__structureChildrenLink"
        title={item.name}
        data-src={`p${parent}c${child}`}
        onClick={(e) => event(e)}
      >
        {item.name}
      </button>
    </li>
  );
};

const StructureItem = ({ item, parent, event }) => {
  const { isAnchor } = item;
  const hasChildren = item.description.length > 0;
  return (
    <li className="post__structureItem">
      {isAnchor && (
        <button
          className="post__structureParent"
          type="button"
          data-src={`p${parent}`}
          onClick={(e) => event(e)}
        >
          {item.caption}
        </button>
      )}
      {hasChildren && (
        <ol className="post__structureChildren">
          {item.description.map((elem, key) => (
            <StructureChildItem
              item={elem}
              parent={parent}
              child={key}
              event={event}
              key={elem.text + Math.random()}
            />
          ))}
        </ol>
      )}
    </li>
  );
};

StructureChildItem.propTypes = {
  item: PropTypes.shape({
    name: PropTypes.string.isRequired,
    isAnchor: PropTypes.bool.isRequired,
  }).isRequired,
  parent: PropTypes.number.isRequired,
  child: PropTypes.number.isRequired,
  event: PropTypes.func.isRequired,
};

StructureItem.propTypes = {
  item: PropTypes.shape({
    description: PropTypes.instanceOf(Array).isRequired,
    caption: PropTypes.string.isRequired,
    isAnchor: PropTypes.bool.isRequired,
  }).isRequired,
  parent: PropTypes.number.isRequired,
  event: PropTypes.func.isRequired,
};

export default StructureItem;
