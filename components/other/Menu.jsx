import React, {useEffect, useRef} from 'react';
import PropTypes from 'prop-types';
import InlineSVG from 'svg-inline-react';
import Parser from 'html-react-parser';
import classNames from 'classnames';

import {useEventListener} from '../hooks';

const MenuItem = ({item, selected}) => (
    <div className={classNames({
        tasksMenu__item: true,
        'tasksMenu__item--active': item.id.toString() === selected,
    })}
    >
        <a href={item.href} className="tasksMenu__itemLink">
            <div className="tasksMenu__itemImg">
                <InlineSVG src={item.img}/>
            </div>
            <div className="tasksMenu__itemText">{Parser(item.title)}</div>
        </a>
    </div>
);

const Menu = ({selected, items}) => {
    const containerRef = useRef();

    const onWheel = (e) => {
        if (e.target.closest('.js-scrolled') !== null) {
            e.preventDefault();
            const container = containerRef.current;
            const containerScrollPosition = container.scrollLeft;

            container.scrollTo({
                top: 0,
                left: containerScrollPosition + e.deltaY,
            });
        }
    };

    useEventListener('wheel', onWheel);

    useEffect(() => {
        const timeout = setTimeout(() => {
            const container = containerRef.current;
            const elem = container.querySelector('.tasksMenu__item--active');
            const containerScrollPosition = container.scrollLeft;
            const position = elem?.getBoundingClientRect() || 0;

            container.scrollTo({
                top: 0,
                left: containerScrollPosition + position.left,
            });
        }, 3000);

        return () => clearTimeout(timeout);
    }, []);

    return (
        <div className="tasksMenu">
            <div className="wrapper">
                <div className="tasksMenu__wrap js-scrolled">
                    <div ref={containerRef}>
                        <div className="tasksMenu__list">
                            {items.map((item) => (
                                <MenuItem item={item} selected={selected} key={item.id + Math.random()}/>
                            ))}
                        </div>
                    </div>
                </div>
            </div>
        </div>
    );
};

MenuItem.propTypes = {
    item: PropTypes.shape({
        id: PropTypes.number.isRequired,
        href: PropTypes.string.isRequired,
        img: PropTypes.string.isRequired,
        title: PropTypes.string.isRequired,
    }).isRequired,
    selected: PropTypes.string.isRequired,
};

Menu.propTypes = {
    items: PropTypes.instanceOf(Array).isRequired,
    selected: PropTypes.string.isRequired,
};

export default Menu;
