export const useSelectValues = () => {
    const values = [
        {
            label: '1C',
            value: '45',
        },
        {
            label: 'Basic auth',
            value: '44',
        },
        {
            label: 'Begun',
            value: '36',
        },
        {
            label: 'CMS',
            value: '21',
        },
        {
            label: 'Facebook',
            value: '27',
        },
        {
            label: 'Youtube',
            value: '29',
        },
        {
            label: 'ВКонтакте',
            value: '30',
        },
        {
            label: 'Гугл+ (Google+)',
            value: '42',
        },
        {
            label: 'Мой Мир (Мэйл)',
            value: '43',
        },
        {
            label: 'FTP',
            value: '20',
        },
        {
            label: 'Intellect Money',
            value: '37',
        },
        {
            label: 'Jivo Site',
            value: '40',
        },
        {
            label: 'MySQL',
            value: '28',
        },
        {
            label: 'PostgreSQL',
            value: '23',
        },
        {
            label: 'RDP',
            value: '46',
        },
        {
            label: 'SSH',
            value: '24',
        },
        {
            label: 'VPN',
            value: '49',
        },
        {
            label: 'Битрикс 24',
            value: '39',
        },
        {
            label: 'Домен',
            value: '32',
        },
        {
            label: 'Доступ к РК (Одноклассники, Мэйл, Медийная реклама Яндекс)',
            value: '25',
        },
        {
            label: 'Одноклассники',
            value: '41',
        },
        {
            label: 'Почта',
            value: '33',
        },
        {
            label: 'Сайт 1С-Битрикс',
            value: '31',
        },
        {
            label: 'Сервисный аккаунт Google (Analytics, AdWords, GMail)',
            value: '38',
        },
        {
            label: 'Сервисный аккаунт Mail (Почта)',
            value: '48',
        },
        {
            label: 'Сервисный аккаунт Яндекс (Метрика, Директ, Маркет, Почта)',
            value: '34',
        },
        {
            label: 'Твиттер',
            value: '22',
        },
        {
            label: 'Хостинг/Сервер',
            value: '35',
        },
        {
            label: 'Google-аккаунт',
            value: '55',
        },
        {
            label: 'Skype',
            value: '54',
        },
        {
            label: 'Яндекс-аккаунт',
            value: '56',
        },
        {
            label: 'Другой',
            value: '26',
        },
    ]


    return {values}
}