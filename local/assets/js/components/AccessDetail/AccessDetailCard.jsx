import React from 'react';
import PropTypes from 'prop-types';

const AccessDetailCard = ({
  date, user, siteUrl, site,
}) => (
  <div className="accessDetail__card">
    <div className="accessDetail__cardRow">
      <div className="accessDetail__cardCaption">Дата регистрации:</div>
      <div className="accessDetail__cardValue">{date}</div>
    </div>
    <div className="accessDetail__cardRow">
      <div className="accessDetail__cardCaption">Регистратор:</div>
      <div className="accessDetail__cardValue">{user}</div>
    </div>
    <div className="accessDetail__cardRow">
      <div className="accessDetail__cardCaption">Сайт:</div>
      <div className="accessDetail__cardValue">
        <a href={siteUrl}>{site}</a>
      </div>
    </div>
  </div>
);

AccessDetailCard.propTypes = {
  date: PropTypes.string,
  user: PropTypes.string,
  siteUrl: PropTypes.string,
  site: PropTypes.string,
};

AccessDetailCard.defaultProps = {
  date: '------',
  user: '------',
  siteUrl: '------',
  site: '------',
};

export default AccessDetailCard;
