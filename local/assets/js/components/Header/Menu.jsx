import React from 'react';
import classNames from 'classnames';

const MenuItem = ({ item }) => (
  <li className="header__menuItem">
    <a
      href={item.href}
      className={classNames({
        "header__menuLink": true,
        "current": item.selected,
      })}
    >
      {item.name}
    </a>
  </li>
);

const Menu = ({ items }) => (
  <div className="header__menu">
    <ul className="header__menuList">
      {items && items.map((item) => <MenuItem item={item} key={item.name + Math.random()} />)}
    </ul>
  </div>
);


export default Menu;
