import React from 'react';
import PropTypes from 'prop-types';

const PostInfo = ({ info }) => (
  <div className="post__info">
    <div className="post__infoItem">
      <div className="post__infoDate">
        <span className="post__infoDateText post__infoDateText--mob">Публ.:&nbsp;</span>
        <span className="post__infoDateText post__infoDateText--desk">Публикация:&nbsp;</span>
        {info.datePublic}
      </div>
      <div className="post__infoUser">{info.userPublic}</div>
    </div>
    <div className="post__infoItem">
      <div className="post__infoDate">
        <span className="post__infoDateText post__infoDateText--mob">Изм.:&nbsp;</span>
        <span className="post__infoDateText post__infoDateText--desk">Изменено:&nbsp;</span>
        {info.dateChange}
      </div>
      <div className="post__infoUser">{info.userChange}</div>
    </div>
  </div>
);

PostInfo.propTypes = {
  info: PropTypes.shape({
    datePublic: PropTypes.string,
    dateChange: PropTypes.string,
    userPublic: PropTypes.string,
    userChange: PropTypes.string,
  }).isRequired,
};

export default PostInfo;
