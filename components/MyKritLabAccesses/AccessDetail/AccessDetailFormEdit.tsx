import React, {FC} from 'react';
import Select from 'react-select';
import RenderModal from '../../other/RenderModal';
import {useAccess} from "../../../hooks/useAccess";
import {useSelector} from "react-redux";
import {AppStateType} from "../../../redux/store";
import {useSelectValues} from "../../../hooks/useSelectValues";
type PropsType={
    open:boolean
    closeModals:()=>void
    setEditModalIsOpen:(flag:boolean)=>void
}
const AccessDetailFormEdit:FC<PropsType> = ({
                                  open,
                                  closeModals,
                                  setEditModalIsOpen
                              }) => {

    const {onChangeHandler, saveAccessEdits} = useAccess()
    const {
        host,
        login,
        password,
        port,
        database,
        notice,
        id
    } = useSelector((state:AppStateType) => state.accesses.selectedAccess)
    const {values} = useSelectValues()

    return (
        <RenderModal isOpen={open} closeModals={closeModals} >
            <div className="editAccess">
                <div className="editAccess__form formBlock"
                     data-form="edit">
                    <div className="formBlock__title title">Редактировать доступ</div>
                    <div className="formBlock__content">
                        <div className="formBlock__row">
                            <div className="formBlock__rowWrap">
                                <div className="inputHolder">
                                    <input
                                        type="text"
                                        name="host"
                                        className="inputHolder__input"
                                        defaultValue={host}
                                        placeholder="Хост"
                                        onChange={(e) => onChangeHandler(e)}
                                    />
                                </div>
                            </div>
                        </div>
                        <div className="formBlock__row">
                            <div className="formBlock__rowWrap">
                                <div className="inputHolder">
                                    <Select
                                        name="type"
                                        options={values}
                                        placeholder="Вид доступа"
                                        isSearchable={false}
                                        className="styledSelect"
                                        classNamePrefix="styledSelect"
                                        onChange={(e) => onChangeHandler(e)}
                                    />
                                </div>
                            </div>
                        </div>
                        <div className="formBlock__row">
                            <div className="formBlock__rowWrap">
                                <div className="inputHolder">
                                    <input
                                        type="text"
                                        name="login"
                                        className="inputHolder__input"
                                        defaultValue={login}
                                        placeholder="Логин"
                                        onChange={(e) => onChangeHandler(e)}
                                    />
                                </div>
                            </div>
                        </div>
                        <div className="formBlock__row">
                            <div className="formBlock__rowWrap">
                                <div className="inputHolder">
                                    <input
                                        type="text"
                                        name="password"
                                        className="inputHolder__input"
                                        defaultValue={password}
                                        placeholder="Пароль"
                                        onChange={(e) => onChangeHandler(e)}
                                    />
                                </div>
                            </div>
                        </div>
                        <div className="formBlock__row">
                            <div className="formBlock__rowWrap">
                                <div className="inputHolder">
                                    <input
                                        type="text"
                                        className="inputHolder__input"
                                        defaultValue={database}
                                        name='database'
                                        placeholder="База данных"
                                        onChange={(e) => onChangeHandler(e)}

                                    />
                                </div>
                            </div>
                        </div>
                        <div className="formBlock__row">
                            <div className="formBlock__rowWrap">
                                <div className="inputHolder">
                                    <input
                                        type="text"
                                        name="port"
                                        className="inputHolder__input"
                                        defaultValue={port}
                                        placeholder="Порт"
                                        onChange={(e) => onChangeHandler(e)}

                                    />
                                </div>
                            </div>
                        </div>
                        <div className="formBlock__row formBlock__row--textarea">
                            <div className="formBlock__rowWrap">
                                <div className="inputHolder">
                <textarea
                    name="notice"
                    className="inputHolder__input inputHolder__input--textarea"
                    placeholder="Примечания"
                    defaultValue={notice}
                    onChange={(e) => onChangeHandler(e)}

                />
                                </div>
                            </div>
                        </div>
                        <div className="formBlock__row formBlock__row--btn">
                            <div className="formBlock__rowWrap">
                                <button
                                    onClick={() => {
                                        saveAccessEdits(id.toString())
                                        setEditModalIsOpen(false)
                                    }
                                    }
                                    className="addNewAccess__btn">
                <span>
                  <svg
                      width="36"
                      height="13"
                      viewBox="0 0 31 11"
                      fill="none"
                      xmlns="http://www.w3.org/2000/svg"
                  >
                    <path
                        d="M1.25 6.0013H29.5833M29.5833 6.0013L25.4167 1.41797M29.5833 6.0013L25.4167 10.168"
                        stroke="white"
                        strokeWidth="1.5"
                        strokeLinecap="round"
                    />
                  </svg>
                </span>
                                    Сохранить
                                </button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </RenderModal>
    );
}
export default AccessDetailFormEdit;
