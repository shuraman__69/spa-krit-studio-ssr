import React from 'react';
import MyWikiTop from './MyWikiTop';
import MyWikiBottom from './MyWikiBottom';

const WikiMain = () => (
  <div className="myKritlabWiki">
    <MyWikiTop />
    <MyWikiBottom />
  </div>
);
export default WikiMain;
