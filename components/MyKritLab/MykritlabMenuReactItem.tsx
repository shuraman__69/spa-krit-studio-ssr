import Link from 'next/link';
import React, {FC} from 'react';
// @ts-ignore
import InlineSVG from 'svg-inline-react';
import {menuType} from "../../redux/reducers/menuRudecer";

type PropsType = {
    item: menuType
}

const MykritlabMenuReactItem: FC<PropsType> = ({item}) => {
    return (
        <div className="menuSectionList__item">
            <Link href={item.href} >
                <a className="menuSectionList__itemWrap">
                    <div className="menuSectionList__itemContent">
                        <div className="menuSectionList__itemIcon">
                            <InlineSVG src={item.image}/>
                        </div>
                        <div className="menuSectionList__itemCaption">{item.name}</div>
                    </div>
                </a>
            </Link>
        </div>
    );
}

export default MykritlabMenuReactItem;
