import React from 'react';
import PropTypes from 'prop-types';
import classNames from 'classnames';

const SupportDataItem = ({ item }) => (
  <div className="supportDescription__item">
    <div className="supportDescription__itemTitle">{item.title}</div>
    <div className="supportDescription__itemValue">{item.value}</div>
  </div>
);

const SupportData = ({ data, event, isOpen }) => {
  const count = 2;
  const show = data.length > count;
  const itemsShow = data.slice(0, count);
  const itemsHidden = data.slice(count);

  return (
    <div className="tasksContent__users tasksUsers">
      <div className="tasksUsers__list">
        <div className="tasksUsers__listWrap">
          {itemsShow.map((item) => (
            <SupportDataItem item={item} key={(item.id = Math.random())} />
          ))}
        </div>
        <div className={classNames({
          'js-hidden-users': true,
          'tasksUsers__listWrap--hidden': !isOpen,
        })}
        >
          <div className="tasksUsers__listWrap">
            {itemsHidden.map((item) => (
              <SupportDataItem item={item} event={event} key={(item.id = Math.random())} />
            ))}
          </div>
        </div>
      </div>
      {show && (
      <button type="button" className="tasksUsers__button" onClick={event}>
        {isOpen ? 'Скрыть' : 'Показать всех'}
      </button>
      )}
    </div>
  );
};

SupportDataItem.propTypes = {
  item: PropTypes.shape({
    title: PropTypes.string.isRequired,
    value: PropTypes.string.isRequired,
  }).isRequired,
};

SupportData.propTypes = {
  data: PropTypes.instanceOf(Array).isRequired,
  event: PropTypes.func.isRequired,
  isOpen: PropTypes.bool.isRequired,
};

export default SupportData;
