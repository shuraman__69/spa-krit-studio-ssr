import React, { Component } from 'react';
import Menu from '../Menu';
import Content from './Content';
import TasksMenu from '../TasksMenu';
import SupportCalendar from './SupportCalendar';

class Support extends Component {
  constructor(props) {
    super(props);
    this.state = {
      data: [],
      tasks: [],
    };
    this.section = document.getElementById('support').getAttribute('data-section');
  }

  componentDidMount() {
    this.getItems();
  }

  getItems() {
    const self = this;
    const xhr = new XMLHttpRequest();

    const formData = new FormData();

    formData.append('AJAX', 'Y');

    xhr.open('GET', '../../api/v3/support/data.json');
    xhr.send(formData);

    xhr.onload = function (response) {
      const result = JSON.parse(response.currentTarget.response);
      if (xhr.status === 200) {
        self.setState({
          data: result,
        });
      }
    };

    xhr.onerror = function () {
      console.log('ошибка');
    };
  }

  render() {
    const { data } = this.state;
    return (
      <div className="tasks">
        <Menu selected={this.section} items={TasksMenu} />
        {data.length > 0
          && <Content selected={this.section} menu={TasksMenu} data={data} />}
        <div>
          <SupportCalendar />
        </div>
      </div>

    );
  }
}

export default Support;
