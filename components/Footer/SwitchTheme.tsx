import React, {useEffect, useState} from 'react';
import {useDispatch, useSelector} from "react-redux";
import {changeUserThemeThunk} from "../../redux/reducers/authReducer";
import {AppStateType} from "../../redux/store";

const changeTheme = (status: any) => {
    const body = document.querySelector('body');
    body!.classList[status ? 'add' : 'remove']('dark-theme');
};

const SwitchTheme = () => {
    const dispatch = useDispatch()
    const DARKTHEME = useSelector((state: AppStateType) => state.authPage.DARKTHEME)
    useEffect(() => {
        setInputState(DARKTHEME)
        changeTheme(DARKTHEME)
    }, [DARKTHEME])

    const [inputState, setInputState] = useState<boolean>()
    const changeInputState = (e: React.ChangeEvent<HTMLInputElement>) => {
        const {checked} = e.target;
        changeTheme(checked);
        dispatch(changeUserThemeThunk(checked))
    }

    return (
        <div className="switcher">
            <button
                type="button"
                className="switcherButton switcherButton--light"
            >
                Light
            </button>
            <label htmlFor="switcherInput" className="switcherCheck">
                <input
                    id="switcherInput"
                    type="checkbox"
                    className="switcherInput"
                    onChange={(e) => changeInputState(e)}
                    checked={inputState}
                />
                <span className="switcherToggle"/>
            </label>
            <button
                type="button"
                className="switcherButton switcherButton--dark"
            >
                Dark
            </button>
        </div>
    );
}


export default SwitchTheme;
