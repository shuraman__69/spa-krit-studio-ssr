import Head from 'next/head'
import React from "react";
import HeaderContainer from "./Header/HeaderContainer";
import Footer from "./Footer/Footer";

const MainLayout = ({children, title}) => {
    return (
        <div>
            <Head>
                <title>{title}</title>
                <meta name='keywords' content='js,next,react,krit'/>
                <meta name='description' content='next js test'/>
            </Head>
            <HeaderContainer/>
            <main className='main-wrapper content'>
                {children}
                <Footer isAuth={true}/>
            </main>

        </div>
    )
}

export default MainLayout