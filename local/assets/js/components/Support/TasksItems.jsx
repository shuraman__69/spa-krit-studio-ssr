import React from 'react';
import PropTypes from 'prop-types';
import Parser from 'html-react-parser';
import classNames from 'classnames';

function TasksItems() {
  return (
    <div className="wrapper margin">
      <div className="tasksItems__list">
        <TasksItem />
        <TasksItem />
        <TasksItem />
      </div>
    </div>
  );
}

function TasksItem() {
  return (
    <div className={classNames({
      tasksItems__item: true,
    })}
    >
      <a href="# " className="tasksItems__itemWrap">
        <div className="tasksItems__itemRow">
          <div className="tasksItems__itemTitle">Минимакс</div>
          <div className="tasksItems__itemCode">555</div>
        </div>
        <div className="tasksItems__itemRow">
          <div className="tasksItems__itemData tasksData">
            <div className="tasksData__item">
              <div className="tasksData__itemCaption">Ответственный:</div>
              <div className="tasksData__itemName">Иванов Иван</div>
            </div>
            <div className="tasksData__item tasksData__item--mobile">
              <div className="tasksData__itemCaption">Постановщик:</div>
              <div className="tasksData__itemCaption tasksData__itemCaption--mobile">Пост.:</div>
              <div className="tasksData__itemName">Петров Петр</div>
            </div>
          </div>
          <div className="tasksItems__itemStatus tasksStatus">
            <div
              className={classNames({
                tasksStatus__icon: true,
                'tasksStatus__icon--ready': true,
              })}
            >
              Готово
            </div>
          </div>
        </div>
      </a>
    </div>
  );
}

export default TasksItems;
