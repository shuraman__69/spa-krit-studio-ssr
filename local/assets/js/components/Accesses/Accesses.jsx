import React, { Component } from 'react';
import Pagination from '../Pagination';
import AccessesForm from './AccessesForm';
import AccessesItem from './AccessesItem';

class Accesses extends Component {
  constructor(props) {
    super(props);
    this.state = {
      items: [],
      totalPages: null,
      currentPage: null,
    };
    this.onPageClick = this.onPageClick.bind(this);
    this.awaitRequest = false;
    this.value = '';
  }

  componentDidMount() {
    this.getItems(this.value);
  }

  handleChange(e) {
    e.preventDefault();
    if (e.target.value) {
      this.value = e.target.value;
    } else {
      this.value = e.target.querySelector('input[type="text"]').value;
    }
    this.getItems(this.value);
  }

  onPageClick(num, e) {
    e.preventDefault();
    const data = {
      cur_page: num,
      value: this.value,
    };
    this.getItems(data);
  }

  getItems(value) {
    const self = this;
    if (!self.awaitRequest) {
      self.awaitRequest = true;
      const xhr = new XMLHttpRequest();

      const formData = new FormData();

      formData.append('AJAX', 'Y');
      formData.append('PARAMS', JSON.stringify(value));

      xhr.open('GET', '../../api/v3/accesses/list.json');
      xhr.send(formData);

      xhr.onload = function (response) {
        const result = JSON.parse(response.currentTarget.response);

        if (xhr.status === 200) {
          self.setState({
            items: result.items,
            totalPages: result.totalPages,
            currentPage: result.page,
          });
        }
        self.awaitRequest = false;
      };
      xhr.onerror = function () {
        console.log('ошибка');
        self.awaitRequest = false;
      };
    }
  }

  renderItems() {
    const { items } = this.state;

    if (items.length > 0) {
      return (
        <div className="accesses__list">
          <div className="accesses__listWrap">
            <div className="accesses__item accesses__item--head">
              <div
                className="accesses__itemCol accesses__itemCol--head"
                data-title="Наимен. / Рег. дата"
                data-title-sm="Наименование / Рег. дата"
              />
              <div
                className="accesses__itemCol accesses__itemCol--head accesses__itemCol--gray"
                data-title="Регистратор"
              />
              <div
                className="accesses__itemCol accesses__itemCol--site accesses__itemCol--head"
                data-title="Сайт"
              />
            </div>
            {items.map((el) => (
              <AccessesItem item={el} key={el.id + Math.random()} />
            ))}
          </div>
        </div>
      );
    }
    return false;
  }

  renderPagination() {
    const { totalPages, currentPage } = this.state;
    if (totalPages > 1) {
      return <Pagination items={totalPages} event={this.onPageClick} current={currentPage} />;
    }
    return false;
  }

  render() {
    return (
      <div className="wrapper">
        <div className="accesses">
          <h2 className="title accesses__title">Менеджер доступов</h2>
          <div className="accesses__container">
            <AccessesForm event={(e) => this.handleChange(e)} />
            {this.renderItems()}
            {this.renderPagination()}
          </div>
        </div>
      </div>
    );
  }
}

export default Accesses;
