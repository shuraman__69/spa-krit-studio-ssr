import {InferActionsType} from "../store";
import {wikiAPI} from "../../api/api";
import {
    breadCrumbsType,
    categoryType,
    sectionMenuType,
    selectedArticleType,
    selectedSectionType,
    topMenuType
} from "../../_types";
import {SET_ERROR, SET_LOADING_FALSE, SET_LOADING_TRUE} from "./authReducer";

const SET_CATEGORIES = 'SET_CATEGORIES';
const SET_TOP_MENU_ITEMS = 'SET_TOP_MENU_ITEMS';
const SET_SELECTED = 'SET_SELECTED';
const SET_SELECTED_SECTION = 'SET_SELECTED_SECTION';
const SET_SELECT = 'SET_SELECT';
const SET_ARTICLE = 'SET_ARTICLE';
const SET_BREAD_CRUMB = 'SET_BREAD_CRUMB';
//TYPES==================


export type InitialSectionsStateType = {
    sections: categoryType[][]
    topMenuItems: topMenuType[]
    selectedItem: topMenuType[]
    selectedSection: selectedSectionType
    sectionMenu: sectionMenuType[]
    selectedArticle: selectedArticleType

}

//===========================================
const initialState: InitialSectionsStateType = {
    sections: [],
    topMenuItems: [],
    selectedItem: [],
    selectedSection: {
        categoryName: '',
        selectedSectionItems: [],
        breadCrumbs: []
    },
    sectionMenu: [],
    selectedArticle: {
        favorite: false,
        id: 1,
        links: [],
        name: '',
        section_id: 0,
        parent_section_id: 0,
        navigation: [],
        props: {
            datePublic: '',
            userPublic: '',
            dateChange: '',
            userChange: '',
        },
        relatedItems: [],
        tags: [],
        text: '',
        voiting: []
    }

}
const wikiReducer = (state = initialState, action: ActionsType) => {
    switch (action.type) {
        case SET_CATEGORIES:
            return {
                ...state,
                sections: [...action.sections]
            }
        case SET_TOP_MENU_ITEMS:
            return {
                ...state,
                topMenuItems: [...action.items]
            }
        case SET_SELECTED:
            return {
                ...state,
                selectedItem: action.selectedItem
            }
        case SET_SELECT:
            return {
                ...state,
                sectionMenu: action.item
            }
        case SET_SELECTED_SECTION:
            return {
                ...state,
                selectedSection: {
                    selectedSectionItems: [...action.selectedSectionItems],
                    categoryName: action.categoryName,
                    breadCrumbs: [...action.breadCrumbs]
                }
            }
        case SET_ARTICLE:
            return {
                ...state,
                selectedArticle: {...action.article}
            }
        case SET_BREAD_CRUMB:
            return {
                ...state,
                selectedSection: {
                    ...state.selectedSection,
                    breadCrumbs: action.breadCrumbs
                }
            }
        default:
            return state;
    }
}
type ActionsType = InferActionsType<typeof actions>
const actions = {
    setCategories: (sections: categoryType[][]) => ({type: SET_CATEGORIES, sections}) as const,
    setTopMenuItems: (items: topMenuType[]) => ({type: SET_TOP_MENU_ITEMS, items}) as const,
    setSelected: (selectedItem: topMenuType[]) => ({type: SET_SELECTED, selectedItem}) as const,
    setArticle: (article: selectedArticleType) => ({type: SET_ARTICLE, article}) as const,
    setSelect: (item: sectionMenuType[]) => ({type: SET_SELECT, item}) as const,
    setSelectedSection: (selectedSectionItems: any, categoryName: string, breadCrumbs: breadCrumbsType[]) => ({
        type: SET_SELECTED_SECTION,
        selectedSectionItems,
        categoryName,
        breadCrumbs
    }) as const,
    setBreadCrumbs: (breadCrumbs: breadCrumbsType[]) => ({
        type: SET_BREAD_CRUMB,
        breadCrumbs
    }) as const,
}

export const getSectionsThunk = () => {
    return async (dispatch: any) => {
        try {
            const response = await wikiAPI.getSections()
            if (response.status === 200) {
                dispatch(actions.setCategories(response.sections))
            }
        } catch (error) {
            await dispatch(SET_ERROR())
        } finally {
            await dispatch(SET_LOADING_FALSE())
        }
    }
}
export const getMenuItemsThunk = () => {
    return async (dispatch: any) => {
        try {
            const response = await wikiAPI.getTopMenu()
            if (response.status === 200) {
                await dispatch(actions.setTopMenuItems(response.items))
                await dispatch(setSelectedThunk("Часто используемые статьи"))
            }
        } catch (error) {
            await dispatch(SET_ERROR())
        }
    }
}
export const setSelectedThunk = (name: string) => {
    return async (dispatch: any, getState: any) => {
        try {
            const items = getState().wiki.topMenuItems
            const selectedItem = items.filter((item: topMenuType) => item.name === name)
            dispatch(actions.setSelected(selectedItem))
        } catch (error) {
            await dispatch(SET_ERROR())
        }
    }
}
export const setSelectedSectionThunk = (sectionId: string) => {
    return async (dispatch: any, getState: any) => {
        try {
            dispatch(SET_LOADING_TRUE())

            const {selectedSection, status} = await wikiAPI.getSelectedSection(sectionId)
            if (status === 200) {
                const breadcrumbs = await wikiAPI.getBreadCrumbs(sectionId)
                const select = await wikiAPI.getSelectSectionMenu(sectionId)

                if (getState().wiki.sections.length === 0) {
                    await dispatch(getSectionsThunk())
                }
                getState().wiki.sections.forEach((item: categoryType[]) => (item.forEach(async (category) => {
                        if (category.section_id === sectionId) {
                            await dispatch(actions.setSelect(select.sectionMenu))
                            await dispatch(actions.setSelectedSection(selectedSection, category.name, breadcrumbs.breadCrumbs))
                        }
                        return false
                    })
                ))
            }


        } catch (error) {
            await dispatch(SET_ERROR())
        }
        finally {
            dispatch(SET_LOADING_FALSE())
        }

    }
}
export const setSelectedItemThunk = (itemId: string) => {
    return async (dispatch: any, getState: any) => {
        try {
            const {article, status} = await wikiAPI.getSelectedItem(itemId)
            const {parent_section_id} = article
            if (status === 200) {
                const breadcrumbs = await wikiAPI.getBreadCrumbs(article.section_id)
                if (!getState().wiki.selectedSection.categoryName) {
                    await dispatch(setSelectedSectionThunk(parent_section_id.toString()))
                }
                await dispatch(actions.setBreadCrumbs(breadcrumbs.breadCrumbs))
                await dispatch(actions.setArticle(article))
            }
        } catch (error) {
            console.log(error)
        }
    }
}

export const addFavoriteThunk = (fav: boolean, id: number, parentName: string) => {
    return async (dispatch: any) => {
        try {
            await wikiAPI.addFavorite(fav, id)
            await dispatch(getMenuItemsThunk())
            await dispatch(setSelectedThunk(parentName))
        } catch (error) {
            await dispatch(SET_ERROR())
        }
    }
}

export default wikiReducer;