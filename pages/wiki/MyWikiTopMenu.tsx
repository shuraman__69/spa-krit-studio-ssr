import React, {FC} from 'react';
import classNames from 'classnames';
import {topMenuType} from "../../_types";

type PropsType = {
    item: topMenuType
    event: (e: React.MouseEvent, name: string) => void
    selectedItem: topMenuType[]
}

const MyWikiTopMenu: FC<PropsType> = ({selectedItem, item, event}) => {
    return (<li className="wikiListMenu__item">
            <button
                type="button"
                className={classNames({
                    wikiListMenu__link: true,
                    active: selectedItem[0] ? selectedItem[0].shortName === item.shortName : false,
                })}
                onClick={(e) => event(e, item.name)}
                title={item.name}
            >
                <span className="wikiListMenu__linkText wikiListMenu__linkText--short">{item.shortName}</span>
                <span className="wikiListMenu__linkText wikiListMenu__linkText--full">{item.name}</span>
            </button>
        </li>

    );
}

export default MyWikiTopMenu;
