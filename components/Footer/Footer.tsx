import React, {FC} from 'react';
import SwitchTheme from './SwitchTheme';
import {useSelector} from "react-redux";
import {AppStateType} from "../../redux/store";

type PropsType = {
    isAuth: boolean
}
const Footer: FC<PropsType> = ({}) => {
    const {name} = useSelector((state: AppStateType) => state.authPage.user)
    const isAuth=!!name
    return (
        <footer className='footer '>
            <div className="footer__info wrapper">
                {isAuth ? <SwitchTheme/> : <div/>}
                <span
                    className="footer__copyright"> © &nbsp;2016&nbsp;— 2021 Информация является конфиденциальной</span>
            </div>
        </footer>
    );
}
export default Footer;
