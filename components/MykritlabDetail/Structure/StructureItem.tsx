import React, {FC} from 'react';
import PropTypes from 'prop-types';
type ChildItemPropsType= {
    item:string
    parent : string
    event:(e:any)=>void
}
const StructureChildItem:FC<ChildItemPropsType> = ({
                                item, parent, event,
                            }) => {
    return (
        <li className="post__structureChildrenItem">
            <button
                type="button"
                className="post__structureChildrenLink"
                title={item}
                data-src={item}
                onClick={(e) => event(e)}
            >
                {item}
            </button>
        </li>
    );
};

type ItemPropsType = {
    item: { name: string, items: string[] }
   parent:string
    event: (e:any) => void
}
const StructureItem: FC<ItemPropsType> = ({item, parent, event}) => {
    const {name} = item;
    const hasChildren = item.items.length > 0;
    return (
        <li className="post__structureItem">
            {name && (
                <button
                    className="post__structureParent"
                    type="button"
                >
                    {name}
                </button>
            )}
            {hasChildren && (
                <ol className="post__structureChildren">
                    {item.items.map((elem, key) => (
                        <StructureChildItem
                            item={elem}
                            parent={parent}
                            event={event}
                            key={elem + Math.random()}
                        />
                    ))}
                </ol>
            )}
        </li>
    );
};


export default StructureItem;
