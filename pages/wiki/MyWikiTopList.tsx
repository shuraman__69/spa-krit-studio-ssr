import React, {FC} from 'react';
import Favorite from '../../components/other/Favorite';
import Link from "next/link";
import {topMenuType} from "../../_types";

//TYPES===================
export type ListType = {
    items: topMenuType
    handleFavoriteClick?: (fav: boolean, id: number, parentName: string | undefined) => void
}
export type ItemType = {
    item: {
        id: number
        href: string
        name: string
        date: string
        changeDate: string
        favorite: boolean
    }
    handleFavoriteClick?: (fav: boolean, id: number, parentName: string | undefined) => void
    parentName: string
}
//========================


const MyWikiTopListItem: FC<ItemType> = ({item, handleFavoriteClick, parentName}) => {
    const dateChange = item.changeDate;
    return (
        <div className="wikiListBlockItem">
            <Link href={item.href}>
                <a  title={item.name} className="wikiListBlockItem__wrap">
                    <div className="wikiListBlockItem__name">
                        <div className="wikiListBlockItem__link">{item.name}</div>
                    </div>
                    <div className="wikiListBlockItem__dates">
                        <div className="wikiListBlockItem__date wikiListBlockItem__date--public">
            <span className="wikiListBlockItem__dateCaption wikiListBlockItem__dateCaption--short">
              Публ.:
            </span>
                            <span className="wikiListBlockItem__dateCaption wikiListBlockItem__dateCaption--full">
              Публикация:
            </span>
                            {item.date}
                        </div>
                        {dateChange.length > 0 && (
                            <div className="wikiListBlockItem__date wikiListBlockItem__date--change">
              <span className="wikiListBlockItem__dateCaption wikiListBlockItem__dateCaption--short">
                Изм.:
              </span>
                                <span className="wikiListBlockItem__dateCaption wikiListBlockItem__dateCaption--full">
                Изменено:
              </span>
                                {item.changeDate}
                            </div>
                        )}
                    </div>
                </a>
            </Link>
            <Favorite parentName={parentName} isFavorite={item.favorite}
                      handleFavoriteClick={handleFavoriteClick} dataId={item.id}/>
        </div>
    );
}
const MyWikiTopList: FC<ListType> = ({items, handleFavoriteClick}) => {
    return (
        <div className="wikiListBlock__col">
            {items.items.map((item) => {
                return (
                    <MyWikiTopListItem handleFavoriteClick={handleFavoriteClick} parentName={items.name} item={item}
                                       key={item.name + Math.random()}/>
                )
            })}
        </div>
    );
}


export default MyWikiTopList;
