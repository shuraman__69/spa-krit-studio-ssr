import React, {FC, useEffect} from 'react';

import SelectRender from '../other/Select';
import BreadCrumbs from '../other/BreadCrumbs';
import Articles from './Articles';
import {useDispatch, useSelector} from "react-redux";
import {AppStateType} from "../../redux/store";
import {useRouter} from "next/router";
import {setSelectedSectionThunk} from "../../redux/reducers/wikiReducer";
import {sectionMenuType} from "../../_types";
import FallBack from "../other/FallBack";

const getValue = (sectionMenu: sectionMenuType[]) => {
    let value = {}
    sectionMenu.forEach((item) => {
        if (item.isSelected) {
            value = {...item}
        }
        return false
    })
    return value
}

const MykritlabArticles = () => {
    const router = useRouter()

    const {sectionMenu} = useSelector((state: AppStateType) => state.wiki)
    const {isLoading} = useSelector((state: AppStateType) => state.authPage)

    const {
        selectedSectionItems,
        categoryName,
        breadCrumbs
    } = useSelector((state: AppStateType) => state.wiki.selectedSection)

    const changeHandler = (e: sectionMenuType) => {
        router.push(e.href)
    }

    const notEmpty = Object.keys(selectedSectionItems).length > 0
    if(isLoading){
        return <FallBack/>
    }
    return (
        <div>

            <div className="whiteBack">
                <div className="wrapper">
                    <div className="d-sm-flex">
                        {notEmpty && <BreadCrumbs breadCrumbs={breadCrumbs} lastItem={categoryName}/>}
                        {notEmpty && (
                            <SelectRender
                                value={getValue(sectionMenu)}
                                select={sectionMenu}
                                onChange={(e: sectionMenuType) => changeHandler(e)}
                            />
                        )}
                    </div>
                </div>
            </div>
            <div className="wrapper">
                <div className="articles">
                    {notEmpty && <h2 className="title">{categoryName}</h2>}
                    {notEmpty && <Articles items={selectedSectionItems}/>}
                </div>
            </div>
        </div>
    );
}


export default MykritlabArticles;
