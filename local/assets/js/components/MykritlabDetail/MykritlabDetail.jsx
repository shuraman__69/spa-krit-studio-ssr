import React, { Component } from 'react';
import BreadCrumbs from './';
import SelectRender from '../Select';
import Detail from './Detail';

class MykritlabDetail extends Component {
  constructor(props) {
    super(props);
    this.setParams();
  }

  componentDidMount() {
    this.getSections();
  }

  handleChange(value) {
    const { sections } = this.state;
    const itemSelected = sections.filter((item) => {
      if (item.value.indexOf(value.value) !== -1) {
        return item;
      }
      return false;
    });
    window.location.href = itemSelected[0].href;
  }

  setParams() {
    const self = this;

    const currentBlock = document.getElementById('article-detail');

    self.state = {
      ID: currentBlock.getAttribute('data-id') !== null ? currentBlock.getAttribute('data-id') : 0,
      sections: [],
      selectedSection: {},
    };
    self.title = currentBlock.getAttribute('data-title');
  }

  getSections() {
    const self = this;
    const xhr = new XMLHttpRequest();

    const formData = new FormData();

    formData.append('AJAX', 'Y');

    xhr.open('GET', '../../api/v3/sections/sections.json');
    xhr.send(formData);

    xhr.onload = function (response) {
      const result = JSON.parse(response.currentTarget.response);

      if (xhr.status === 200) {
        const sectionsItem = result.filter((item) => item.isSelected)[0];
        self.setState({
          sections: result,
          selectedSection: sectionsItem,
          title: sectionsItem.label,
        });
      }
    };

    xhr.onerror = function () {
      console.log('ошибка');
    };
  }

  render() {
    const self = this;
    const selected = self.state.selectedSection;
    const notEmpty = Object.keys(selected).length > 0;

    return (
      <div>
        <div className="whiteBack">
          <div className="wrapper">
            <div className="d-sm-flex">
              <BreadCrumbs value={self.title} />
              {notEmpty && (
                <SelectRender
                  value={{ label: selected.label, value: selected.value }}
                  select={self.state.sections}
                  onChange={(e) => self.handleChange(e)}
                />
              )}
            </div>
          </div>
        </div>
        <div className="wrapper">
          {self.state.ID && <Detail title={self.title} detailID={self.state.ID} />}
        </div>
      </div>
    );
  }
}

export default MykritlabDetail;
