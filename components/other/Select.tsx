import React, {FC} from 'react';
import Select from 'react-select';
import {sectionMenuType} from "../../_types";

type PropsType = {
    select: sectionMenuType[]
    onChange: (e: any) => void
    value: sectionMenuType | {}
}

const SelectRender: FC<PropsType> = ({select, onChange, value}) => (
    <Select
        options={select}
        onChange={onChange}
        value={value}
        isSearchable={false}
        className="styledSelect"
        classNamePrefix="styledSelect"
    />
);

export default SelectRender;
