import React, {FC} from 'react';
import classNames from 'classnames';
import Link from "next/link";
import {categoryType} from "../../_types";

type ItemType = {
    item: categoryType,
    setSelectedSection: (sectionId: string) => void
    categoryName: string
}
type ListType = {
    category: categoryType[]
    setSelectedSection: (sectionId: string) => void
}
const MyWikiBottomItem: FC<ItemType> = ({item, setSelectedSection,}) => (
    <div
        className={classNames({
            wikiCategoryItem: true,
            'wikiCategoryItem--bordered': item.extranet,
            'wikiCategoryItem--highlighted': item.highlighted,
        })}
        onClick={() => setSelectedSection(item.section_id)}
    >
        <Link href={item.href} >
            <a className="wikiCategoryItem__link">
                <div className="wikiCategoryItem__name">{item.name}</div>
                <div className="wikiCategoryItem__count">{item.count} статей</div>
            </a>
        </Link>
    </div>
);

const MyWikiBottomList: FC<ListType> = ({category, setSelectedSection}) => (
    <div className="wikiCategoryList">
        {category.map((item) => (
            <MyWikiBottomItem setSelectedSection={setSelectedSection} categoryName={item.name} item={item}
                              key={item.name + Math.random()}/>
        ))}
    </div>
);


export default MyWikiBottomList;
