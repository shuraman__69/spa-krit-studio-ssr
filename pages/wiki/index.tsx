import React from 'react';
import MyWikiTop from './MyWikiTop';
import MyWikiBottom from './MyWikiBottom';
import MainLayout from "../../components/MainLayout";
import {NextThunk, wrapper} from "../../redux/store";
import {getMenuItemsThunk, getSectionsThunk} from "../../redux/reducers/wikiReducer";
import {getUserInfoThunk} from "../../redux/reducers/authReducer";

const Index = () => (
    <MainLayout title='Wiki | NextJS'>
        <div className="myKritlabWiki">
            <MyWikiTop/>
            <MyWikiBottom/>
        </div>
    </MainLayout>
);
export default Index;


export const getStaticProps = wrapper.getStaticProps(async ({store}) => {
    const dispatch = store.dispatch as NextThunk
    await dispatch(await getUserInfoThunk())
    await dispatch(await getMenuItemsThunk())
    await dispatch(await getSectionsThunk())
})

