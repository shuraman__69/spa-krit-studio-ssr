import React, {FC, Fragment} from 'react';
import Link from "next/link";

import {breadCrumbsType} from "../../_types";

type PropsType = {
    breadCrumbs?: breadCrumbsType[]
    lastItem?: string
}
const BreadCrumbs: FC<PropsType> = ({breadCrumbs, lastItem}) => {
    return (
        <div className="breadCrumbs">
            <ul className="breadCrumbs__list">
                {breadCrumbs && breadCrumbs.map((item) => {

                    return (
                        <Fragment key={item.label + Math.random()}>
                            <li className="breadCrumbs__item">
                                <Link href={item.href}>
                                    <a title={item.label} className="breadCrumbs__link">
                                        {item.label}
                                    </a>
                                </Link>
                            </li>
                            <li className="breadCrumbs__item">
                <span className="breadCrumbs__link breadCrumbs__link--next">
                  <svg
                      width="16"
                      height="6"
                      viewBox="0 0 16 6"
                      fill="none"
                      xmlns="http://www.w3.org/2000/svg"
                  >
                    <path d="M13 1L15 3M15 3L13 5M15 3H0.5" stroke="#8EADFF"/>
                  </svg>
                </span>
                            </li>
                        </Fragment>
                    )
                })}
                {!breadCrumbs && (
                    <>
                        <li className="breadCrumbs__item">
                            <Link href={'/'}>
                                <a title={'Главная'} className="breadCrumbs__link">
                                    Главная
                                </a>
                            </Link>
                        </li>
                        <li className="breadCrumbs__item">
                    <span className="breadCrumbs__link breadCrumbs__link--next">
                    <svg
                        width="16"
                        height="6"
                        viewBox="0 0 16 6"
                        fill="none"
                        xmlns="http://www.w3.org/2000/svg"
                    >
                    <path d="M13 1L15 3M15 3L13 5M15 3H0.5" stroke="#8EADFF"/>
                    </svg>
                    </span>
                        </li>
                        <li className="breadCrumbs__item">
                            <Link href={'/access-manager'}>
                                <a title={"Доступы"} className="breadCrumbs__link">
                                    Доступы
                                </a>
                            </Link>
                        </li>
                        <li className="breadCrumbs__item">
                    <span className="breadCrumbs__link breadCrumbs__link--next">
                    <svg
                        width="16"
                        height="6"
                        viewBox="0 0 16 6"
                        fill="none"
                        xmlns="http://www.w3.org/2000/svg"
                    >
                    <path d="M13 1L15 3M15 3L13 5M15 3H0.5" stroke="#8EADFF"/>
                    </svg>
                    </span>
                        </li>
                    </>
                )}
                <li className="breadCrumbs__item">
                    <span
                        className="breadCrumbs__link breadCrumbs__link--last">{lastItem ? lastItem : ''}</span>
                </li>
            </ul>
        </div>
    );
}
// @ts-ignore
export default BreadCrumbs
