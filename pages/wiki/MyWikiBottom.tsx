import React, {useEffect} from 'react';
import MyWikiBottomList from './MyWikiBottomList';
import {useDispatch, useSelector} from "react-redux";
import {AppStateType} from "../../redux/store";
import {getSectionsThunk, setSelectedSectionThunk} from "../../redux/reducers/wikiReducer";
import {categoryType} from "../../_types";

const MyWikiBottom = () => {
    const dispatch = useDispatch()
    const sections: categoryType[][] = useSelector((state: AppStateType) => state.wiki.sections)
    const setSelectedSection = (sectionId: string) => {
        dispatch(setSelectedSectionThunk(sectionId))
    }
    useEffect(() => {
        dispatch(getSectionsThunk())
    }, [])
    const renderItems = () => {
        return sections.map((section: categoryType[]) => (
            <MyWikiBottomList setSelectedSection={setSelectedSection} category={section} key={Math.random()}/>
        ));
    }

    return (
        <div className="wrapper">
            <div className="wikiCategory">
                <h2 className="title">Категории статей</h2>
                {renderItems()}
            </div>
        </div>
    );
}


export default MyWikiBottom;
