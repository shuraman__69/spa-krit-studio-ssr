import React, {FC, SyntheticEvent} from 'react';
import Link from "next/link";
import {selectedSectionItemsType} from "../../_types";
import classNames from "classnames";


type ListPropsType = {
    item: selectedSectionItemsType
    event: (e: React.MouseEvent | React.KeyboardEvent) => void
    isOpen: boolean
}
type ItemType = {
    item: { name: string, href: string }
}


const ArticlesItem: FC<ItemType> = ({item}) => (
    <div className="articles__listItem">
        <Link href={item.href}>
            <a title={item.name} className="articles__listLink">
                {item.name}
            </a>
        </Link>
    </div>
);

const ArticlesList: FC<ListPropsType> = ({item, event, isOpen}) => {
    item.items.sort((a, b) => {
        const nameA = a.name.toLowerCase();
        const nameB = b.name.toLowerCase();
        if (nameA < nameB) {
            return -1;
        }
        if (nameA > nameB) {
            return 1;
        }
        return 0;
    });
    return (
        <div className={classNames({
            "articles__elem js-article-elem ": true,
        })}>
            <div className="articles__caption"
                 role="button" tabIndex={0} onKeyPress={(e) => event(e)}
                 onClick={event}
            >
                {item.name}
            </div>
            <div className=
                     {classNames({
                         "articles__list js-articles-list": true,
                         "show-list": isOpen
                     })}>
                <div className="articles__listWrap js-articles-list-wrap">
                    {item.items.map((item) => (
                        <ArticlesItem item={item} key={item.name + Math.random()}/>
                    ))}
                </div>
            </div>
        </div>
    );
};


export default ArticlesList;
