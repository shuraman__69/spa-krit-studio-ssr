import React from 'react';
import PropTypes from 'prop-types';
import Parser from 'html-react-parser';
import classNames from 'classnames';

const TasksUsersItem = ({ item, event }) => (
  <label className="tasksUsers__item" htmlFor={item.idUser}>
    <input
      id={item.idUser}
      type="radio"
      name="users__tasks"
      defaultChecked={item.selected === true ? 'checked' : ''}
      value={item.idUser}
      onChange={event}
    />
    <div
      className={classNames({
        tasksUsers__itemWrap: true,
        'tasksUsers__itemWrap--all': item.idUser === 'allItems',
      })}
    >
      <div className="tasksUsers__itemName">{Parser(item.title)}</div>
      <div className="tasksUsers__itemCount">{item.tasks.length}</div>
    </div>
  </label>
);

const TasksUsers = ({
  items, event, click, isOpen, count,
}) => {
  const show = items.length > count;
  const itemsShow = items.slice(0, count);
  const itemsHidden = items.slice(count);

  return (
    <div className="tasksContent__users tasksUsers">
      <div className="tasksUsers__list">
        <div className="tasksUsers__listWrap">
          {itemsShow.map((item) => (
            <TasksUsersItem item={item} event={event} key={(item.id = Math.random())} />
          ))}
        </div>
        <div className={classNames({
          'js-hidden-users': true,
          'tasksUsers__listWrap--hidden': !isOpen,
        })}
        >
          <div className="tasksUsers__listWrap">
            {itemsHidden.map((item) => (
              <TasksUsersItem item={item} event={event} key={(item.id = Math.random())} />
            ))}
          </div>
        </div>
      </div>
      {show && (
        <button type="button" className="tasksUsers__button" onClick={click}>
          {isOpen ? 'Скрыть' : 'Показать всех'}
        </button>
      )}
    </div>
  );
};

TasksUsersItem.propTypes = {
  item: PropTypes.shape({
    idUser: PropTypes.oneOfType([PropTypes.string, PropTypes.number]).isRequired,
    selected: PropTypes.bool.isRequired,
    title: PropTypes.string.isRequired,
    tasks: PropTypes.instanceOf(Array).isRequired,
  }).isRequired,
  event: PropTypes.func.isRequired,
};

TasksUsers.propTypes = {
  items: PropTypes.instanceOf(Array).isRequired,
  count: PropTypes.number.isRequired,
  event: PropTypes.func.isRequired,
  click: PropTypes.func.isRequired,
  isOpen: PropTypes.bool.isRequired,
};

export default TasksUsers;
