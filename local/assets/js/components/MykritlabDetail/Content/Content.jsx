import React from 'react';
import PropTypes from 'prop-types';
import ContentItem from './ContentItem';

const copyClick = (e) => {
  e.preventDefault();
  window.getSelection().removeAllRanges();
  const copyText = e.target.closest('.js-code').querySelector('.js-copy-code');
  const range = document.createRange();
  range.selectNode(copyText);
  window.getSelection().addRange(range);
  document.execCommand('copy');
  window.getSelection().removeAllRanges();
};

const Content = ({ content }) => (
  <div className="post__content">
    {content.map((item, key) => (
      <ContentItem item={item} parent={key} event={copyClick} key={item.caption + Math.random()} />
    ))}
  </div>
);

Content.propTypes = {
  content: PropTypes.instanceOf(Array).isRequired,
};

export default Content;
