import React, {FC, useEffect} from 'react';
import BreadCrumbs from '../other/BreadCrumbs';
import SelectRender from '../other/Select';
import Detail from './Detail';
import {useDispatch, useSelector} from "react-redux";
import {AppStateType} from "../../redux/store";
import {setSelectedItemThunk} from "../../redux/reducers/wikiReducer";
import {sectionMenuType} from "../../_types";
import {useRouter} from "next/router";

const getValue = (sectionMenu: sectionMenuType[]) => {
    let value = {}
    sectionMenu.forEach((item) => {
        if (item.isSelected) {
            value = {...item}
        }
        return false
    })
    return value
}
const MykritlabDetail = () => {
    const dispatch = useDispatch()
    const router = useRouter()
    const pathname=router.pathname
    const {isLoading} = useSelector((state: AppStateType) => state.authPage)

    const {sectionMenu} = useSelector((state: AppStateType) => state.wiki)
    const {selectedArticle} = useSelector((state: AppStateType) => state.wiki)
    const {
        breadCrumbs
    } = useSelector((state: AppStateType) => state.wiki.selectedSection)

    const changeHandler = (e: sectionMenuType) => {
        router.push(e.href)
    }

    useEffect(() => {
        window.scrollTo(0, 0);
    }, [pathname]);
    return (
        <div>
            <div className="whiteBack">
                <div className="wrapper">
                    <div className="d-sm-flex">
                        <BreadCrumbs breadCrumbs={breadCrumbs} lastItem={selectedArticle.name}/>
                        <SelectRender
                            value={getValue(sectionMenu)}
                            select={sectionMenu}
                            onChange={(e: sectionMenuType) => changeHandler(e)}
                        />

                    </div>
                </div>
            </div>
            {selectedArticle.name && <div className="wrapper">
                <Detail {...selectedArticle} />
            </div>}
        </div>
    );
}


export default MykritlabDetail;
