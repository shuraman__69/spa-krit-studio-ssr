import React, {useEffect} from 'react';
import MyWikiBottomList from './MyWikiBottomList';
import {useDispatch, useSelector} from "react-redux";
import {getSectionsThunk} from "../../../../../redux/reducers/wikiReducer";

const MyWikiBottom = () => {
    const sections = useSelector((state) => state.sections.sections)
    const dispatch = useDispatch()
    useEffect(() => {
        dispatch(getSectionsThunk())
    },[])
    const renderItems = () => {
        return sections.map((section) => (
            <MyWikiBottomList category={section} key={Math.random()}/>
        ));
    }

    return (
        <div className="wrapper">
            <div className="wikiCategory">
                <h2 className="title">Категории статей</h2>
                {renderItems()}
            </div>
        </div>
    );
}


export default MyWikiBottom;
