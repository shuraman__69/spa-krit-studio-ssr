import React from 'react';
import PropTypes from 'prop-types';

const ArticleTags = ({ items }) => (
  <div className="post__bottomCol post__tags">
    <h5>Тэги</h5>
    {items.map((item) => (
      <a
        href={item.href}
        className="post__tagsItem"
        title={item.name}
        key={item.name + Math.random()}
      >
        {item.name}
      </a>
    ))}
  </div>
);

ArticleTags.propTypes = {
  items: PropTypes.instanceOf(Array).isRequired,
};

export default ArticleTags;
