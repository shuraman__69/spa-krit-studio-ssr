/* eslint-disable */
const getSuffix = (titles, number) => titles[number % 10 > 1
&& number % 10 < 5
    ? 1 : number % 10 === 1
        ? 0 : 2];

export default getSuffix;
