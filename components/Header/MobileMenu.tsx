import React, {FC} from 'react';
import Search from './Search';
import Logo from './Logo';
import Menu from './Menu';
import User from './User';
import Icon from './Icon';
import {HeaderPropsType} from "./Header";
import {useDispatch} from "react-redux";


const MobileMenu: FC<{ items: any, clickEvent: any } & HeaderPropsType> = ({name, lastName, items, clickEvent}) => {
    let isSimpleHead
    if (process.browser) {
        isSimpleHead = document.querySelector('.header--simple') !== null;
    }
    const dispatch = useDispatch()

    return (
        <div className="header__middleWrap">
            <div className="header__middleTop">
                <Icon clickEvent={(e: any) => clickEvent(e)}/>
                <User name={name} lastName={lastName}/>
            </div>
            <Logo/>
            {!isSimpleHead && <Search/>}
            <Menu items={items}/>
        </div>
    );
};


export default MobileMenu;
