import React, { Component } from 'react';
import PropTypes from 'prop-types';
import Parser from 'html-react-parser';

const switchEvent = () => {
  const messages = document.querySelectorAll('.messInformer__item').length;
  if (messages < 1) {
    document.querySelector('.messInformer__inner').classList.remove('-showed');
    document.querySelector('.messInformer').classList.remove('-active');
  } else {
    document.querySelector('.messInformer__inner').classList.add('-showed');
  }
};

const removeMessages = (id, e) => {
  const elem = e.target;
  const data = {
    messId: id,
    userId: 1,
  };

  const xhr = new XMLHttpRequest();

  const formData = new FormData();

  formData.append('AJAX', 'Y');
  formData.append('PARAMS', JSON.stringify(data));

  xhr.open('DELETE', '/');
  xhr.send(formData);

  xhr.onload = function (response) {
    const result = JSON.parse(response.currentTarget.response);

    if (xhr.status === 200) {
      elem.closest('.messInformer__item').remove();
      switchEvent();
    }
  };

  xhr.onerror = function () {
    console.log('ошибка');
  };
};

const InformerItem = ({ item, closeEvent }) => (
  <div className="messInformer__item" data-id={item.id}>
    <div className="messInformer__close">
      <button type="button" className="messInformer__closeBtn" onClick={closeEvent}>
        <svg
          width="18"
          height="18"
          viewBox="0 0 18 18"
          fill="none"
          xmlns="http://www.w3.org/2000/svg"
        >
          <path d="M9 0C4.02941 0 0 4.02941 0 9C0 13.9706 4.02941 18 9 18C13.9706 18 18 13.9706 18 9C18 4.02941 13.9706 0 9 0ZM12.1824 11.3853C12.4029 11.6059 12.4029 11.9618 12.1824 12.1794C11.9618 12.4 11.6059 12.4 11.3882 12.1794L9.00588 9.79706L6.60588 12.1971C6.38529 12.4176 6.02647 12.4176 5.80294 12.1971C5.58235 11.9765 5.58235 11.6176 5.80294 11.3941L8.20294 8.99412L5.82059 6.61176C5.6 6.39118 5.6 6.03529 5.82059 5.81765C6.04118 5.59706 6.39706 5.59706 6.61471 5.81765L8.99706 8.2L11.4147 5.78235C11.6353 5.56176 11.9941 5.56176 12.2176 5.78235C12.4382 6.00294 12.4382 6.36176 12.2176 6.58529L9.8 9.00294L12.1824 11.3853Z" />
        </svg>
      </button>
    </div>
    <div className="messInformer__text">{Parser(item.message)}</div>
  </div>
);

class Informer extends Component {
  constructor(props) {
    super(props);
    this.state = {
      items: [],
    };
    this.setWrapperRef = React.createRef();
    this.handleClickOutside = this.handleClickOutside.bind(this);
  }

  componentDidMount() {
    this.getMessages();
    document.addEventListener('mousedown', this.handleClickOutside);
  }

  componentWillUnmount() {
    document.removeEventListener('mousedown', this.handleClickOutside);
  }

  handleClickOutside(event) {
    const elem = this.setWrapperRef.current;
    if (elem && !elem.contains(event.target)) {
      document.querySelector('.messInformer__inner').classList.remove('-showed');
    }
  }

  getMessages() {
    const data = {
      userId: 1,
    };
    const self = this;
    const xhr = new XMLHttpRequest();

    const formData = new FormData();

    formData.append('AJAX', 'Y');
    formData.append('PARAMS', JSON.stringify(data));

    xhr.open('GET', '../../api/v3/informerMessages.json');
    xhr.send(formData);

    xhr.onload = function (response) {
      const result = JSON.parse(response.currentTarget.response);

      if (xhr.status === 200) {
        if (result.length > 1) {
          result.sort((a, b) => {
            const dateA = new Date(a.date);
            const dateB = new Date(b.date);
            return dateA - dateB;
          });
        }
        if (result.length > 0) {
          self.setState({
            items: result,
          });
          document.querySelector('.messInformer').classList.add('-active');
        }
      }
    };

    xhr.onerror = function () {
      console.log('ошибка');
    };
  }

  renderMessages() {
    const { items } = this.state;
    return items.map((item) => (
      <InformerItem
        item={item}
        closeEvent={(e) => removeMessages(item.id, e)}
        key={item.id + Math.random()}
      />
    ));
  }

  render() {
    return (
      <div className="messInformer" ref={this.setWrapperRef}>
        <div className="messInformer__inner">{this.renderMessages()}</div>
        <button type="button" className="messInformer__button" onClick={switchEvent}>
          <span className="messInformer__icon">
            <svg
              width="19"
              height="21"
              viewBox="0 0 19 21"
              fill="none"
              xmlns="http://www.w3.org/2000/svg"
            >
              <path
                d="M17.0954 14.6556V9.21338C17.0802 6.13732 15.0994 3.40141 12.1435 2.32183C12.0064 1.02042 10.8789 0 9.50762 0C8.13633 0 6.99359 1.02042 6.85646 2.30704C3.90056 3.38662 1.91981 6.12253 1.90457 9.21338V14.6556C1.417 14.7 0.959904 14.9218 0.624699 15.2472L0.594226 15.2768C0.213312 15.6761 0 16.1937 0 16.7261C0 17.2732 0.228549 17.7908 0.624699 18.1901C1.02085 18.5746 1.56937 18.7965 2.13312 18.7965H7.35926V18.9148C7.35926 19.462 7.58781 19.9944 7.98396 20.3789L8.01443 20.4084C8.41059 20.7782 8.94387 20.9852 9.49238 20.9852C10.0714 20.9852 10.6047 20.7634 11.0008 20.3789C11.397 19.9944 11.6255 19.462 11.6255 18.9148V18.7965H16.8516C17.4154 18.7965 17.9639 18.5746 18.3601 18.1901C18.7562 17.8056 18.9848 17.2732 18.9848 16.7113C19 15.6465 18.162 14.7739 17.0954 14.6556ZM9.03528 19.4028L9.02005 19.388C8.88292 19.2697 8.82197 19.0923 8.82197 18.9148V18.7965H10.1933V18.9148C10.1933 19.0923 10.1171 19.2549 9.99519 19.388C9.85806 19.5211 9.69046 19.5803 9.50762 19.5803C9.32478 19.5803 9.15718 19.5211 9.03528 19.4028ZM1.63031 16.2676L1.64555 16.2528C1.76744 16.1345 1.93504 16.0606 2.11788 16.0606H16.8516C17.2173 16.0606 17.5221 16.3563 17.5373 16.7261C17.5373 16.9035 17.4611 17.0662 17.3392 17.1845C17.2173 17.3028 17.0345 17.3768 16.8669 17.3768H2.13312C1.75221 17.3768 1.46271 17.081 1.44747 16.7113C1.46271 16.5486 1.52366 16.4007 1.63031 16.2676ZM9.2486 1.56761C9.65998 1.47887 10.0866 1.61197 10.3609 1.89296C10.2085 1.87817 10.0409 1.86338 9.88853 1.84859H9.2486H9.23336C9.03528 1.84859 8.83721 1.86338 8.63913 1.89296C8.80674 1.73028 9.00481 1.61197 9.2486 1.56761ZM9.7514 3.26831C13.0425 3.40141 15.6327 6.01901 15.648 9.21338V14.6408H3.36728V9.21338C3.38252 6.0338 5.91179 3.44577 9.17241 3.26831H9.7514Z"
                fill="#B6C2E2"
              />
            </svg>
          </span>
        </button>
      </div>
    );
  }
}

InformerItem.propTypes = {
  item: PropTypes.shape({
    id: PropTypes.number.isRequired,
    message: PropTypes.string.isRequired,
  }).isRequired,
  closeEvent: PropTypes.func.isRequired,
};

export default Informer;
