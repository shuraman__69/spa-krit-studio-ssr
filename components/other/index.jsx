import React from 'react';
import ReactDOM from 'react-dom';
import Header from './Header/Header';
import Footer from './Footer/Footer';
import MykritlabMenu from './Mykritlab/MykritlabMenu';
import WikiMain from './MykritlabWiki/WikiMain';
import MykritlabArticles from './MykritlabArticles/MykritlabArticles';
import MykritlabDetail from './MykritlabDetail/MykritlabDetail';
import MykritlabSearch from './MykritlabSearch/MykritlabSearch';
import Authorization from './Authorization/Authorization';
import Accesses from './Accesses/Accesses';
import AccessDetail from './AccessDetail/AccessDetail';
import Tasks from './Tasks/Tasks';
import Support from './Support/Support';

if (document.getElementById('header') !== null) {
    ReactDOM.render(<Header/>, document.getElementById('header'));
}
if (document.getElementById('root') !== null) {
    ReactDOM.render(<MykritlabMenu/>, document.getElementById('root'));
}
if (document.getElementById('learning') !== null) {
    ReactDOM.render(<WikiMain/>, document.getElementById('learning'));
}
if (document.getElementById('articles') !== null) {
    ReactDOM.render(<MykritlabArticles/>, document.getElementById('articles'));
}
if (document.getElementById('article-detail') !== null) {
    ReactDOM.render(<MykritlabDetail/>, document.getElementById('article-detail'));
}
if (document.getElementById('search-page') !== null) {
    ReactDOM.render(<MykritlabSearch/>, document.getElementById('search-page'));
}
if (document.getElementById('auth') !== null) {
    ReactDOM.render(<Authorization/>, document.getElementById('auth'));
}
if (document.getElementById('accesses-page') !== null) {
    ReactDOM.render(<Accesses/>, document.getElementById('accesses-page'));
}
if (document.getElementById('access-detail') !== null) {
    ReactDOM.render(<AccessDetail/>, document.getElementById('access-detail'));
}
if (document.getElementById('tasks') !== null) {
    ReactDOM.render(<Tasks/>, document.getElementById('tasks'));
}
if (document.getElementById('support') !== null) {
    ReactDOM.render(<Support/>, document.getElementById('support'));
}
ReactDOM.render(<Footer/>, document.getElementById('footer'));
