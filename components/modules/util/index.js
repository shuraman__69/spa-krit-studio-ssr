export {default as getSuffix} from './get-suffix';
export {slideUp, slideDown, slideToggle} from './slide-toggle';
