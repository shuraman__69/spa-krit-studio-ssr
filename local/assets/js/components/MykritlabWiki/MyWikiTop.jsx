import React, { Component } from 'react';
import Slider from 'react-slick';

import MyWikiTopMenu from './MyWikiTopMenu';
import MyWikiTopList from './MyWikiTopList';
import MyWikiTopListEmpty from './MyWikiTopListEmpty';

class MyWikiTop extends Component {
  constructor(props) {
    super(props);
    this.state = {
      items: null,
      slider: false,
    };
    this.listType = '';
    this.listName = '';
    this.menuWiki = [];
    this.settings = {
      dots: true,
      arrows: false,
      infinite: false,
      speed: 500,
      slidesToShow: 1,
      slidesToScroll: 1,
      variableWidth: true,
    };
  }

  componentDidMount() {
    this.getMenuItems();
    window.addEventListener('resize', this.setSlider);
  }

  componentWillUnmount() {
    window.removeEventListener('resize', this.setSlider);
  }

  handleClick(e) {
    e.preventDefault();

    function isActive(element) {
      element.active = element.type === e.currentTarget.getAttribute('data-type');
    }
    if (this.listType !== e.currentTarget.getAttribute('data-type')) {
      const items = this.menuWiki.concat();
      items.find(isActive);
      this.menuWiki = [...items];
      this.setProps();
      this.getItems();
    }
  }

  handleFavoriteClick(e) {
    e.preventDefault();
    const item = e.currentTarget;
    const itemParent = item.closest('.wikiListBlockItem');
    const isFavorite = item.classList.contains('favorite--added');
    const data = {
      articleID: item.getAttribute('data-id'),
      isFavorite: !isFavorite,
    };
    if (isFavorite) {
      itemParent.classList.add('removed');
      const timer = setTimeout(() => {
        const self = this;
        const xhr = new XMLHttpRequest();

        const formData = new FormData();

        formData.append('AJAX', 'Y');
        formData.append('PARAMS', JSON.stringify(data));

        xhr.open('POST', '../../api/v3/wiki/list.json');
        xhr.send(formData);

        xhr.onload = function () {
          if (xhr.status === 200) {
            self.getItems();
          }
        };

        xhr.onerror = function () {
          console.log('ошибка');
        };
      }, 1000);
      return () => clearTimeout(timer);
    }
  }

  getMenuItems() {
    const self = this;
    const xhr = new XMLHttpRequest();

    const formData = new FormData();

    formData.append('AJAX', 'Y');

    xhr.open('GET', '../../api/v3/menu/wiki.json');
    xhr.send(formData);

    xhr.onload = function (response) {
      const result = JSON.parse(response.currentTarget.response);

      if (xhr.status === 200) {
        self.menuWiki = [...result];
        self.setProps();
        self.getItems();
      }
    };

    xhr.onerror = function () {
      console.log('ошибка');
    };
  }

  getItems() {
    const self = this;
    const xhr = new XMLHttpRequest();

    const formData = new FormData();

    formData.append('AJAX', 'Y');

    xhr.open('GET', `../../api/v3/wiki/list.json?type=${self.listType}`);
    xhr.send(formData);

    xhr.onload = function (response) {
      const result = JSON.parse(response.currentTarget.response);

      if (xhr.status === 200) {
        const res = result.filter((item) => {
          if (item.type.indexOf(self.listType) !== -1) {
            return item;
          }
          return false;
        });
        const sliderInit = window.innerWidth < 768;
        self.setState({
          items: [...res],
          slider: sliderInit,
        });
      }
    };

    xhr.onerror = function () {
      console.log('ошибка');
    };
  }

  setSlider = () => {
    const { slider } = this.state;
    if (window.innerWidth < 768 && !slider) {
      this.setState({
        slider: true,
      });
    }
    if (window.innerWidth >= 768 && slider) {
      this.setState({
        slider: false,
      });
    }
  };

  setProps() {
    function isActive(element) {
      if (element.active === true) {
        return element;
      }
      return false;
    }
    const items = this.menuWiki.concat();
    const itemActive = items.find(isActive);
    this.listType = itemActive.type;
    this.listName = itemActive.name;
  }

  getNewArray() {
    const arraySize = 3;
    const newItemsArray = [];
    const { items } = this.state;
    for (let i = 0; i < Math.ceil(items.length / arraySize); i += 1) {
      newItemsArray[i] = items.slice(i * arraySize, i * arraySize + arraySize);
    }
    return newItemsArray;
  }

  renderItemsList(articlesItem, favoritList) {
    return articlesItem.map((items, key) => {
      if (favoritList) {
        return (
          <MyWikiTopList
            items={items}
            event={(e) => this.handleFavoriteClick(e)}
            key={key + Math.random()}
          />
        );
      }
      return <MyWikiTopList items={items} key={key + Math.random()} />;
    });
  }

  renderList() {
    const { items, slider } = this.state;
    if (items !== null) {
      const favoritList = this.listType === 'FAV';
      const articlesItem = this.getNewArray();
      const articles = articlesItem.length;
      if (articles > 0) {
        if (slider) {
          return (
            <div className="wikiListBlock__list">
              <div className="wikiListBlock__slider js-slider">
                <Slider {...this.settings}>
                  {this.renderItemsList(articlesItem, favoritList)}
                </Slider>
              </div>
            </div>
          );
        }
        return (
          <div className="wikiListBlock__list">
            <div className="wikiListBlock__slider js-slider">
              {this.renderItemsList(articlesItem, favoritList)}
            </div>
          </div>
        );
      }
      return (
        <div className="wikiListBlock__list">
          <MyWikiTopListEmpty type={this.listType} />
        </div>
      );
    }
    return false;
  }

  renderMenu() {
    return this.menuWiki.map((item) => (
      <MyWikiTopMenu
        item={item}
        event={(e) => this.handleClick(e)}
        key={item.name + Math.random()}
      />
    ));
  }

  render() {
    return (
      <div className="wikiListTop">
        <div className="wikiListMenu">
          <div className="wrapper">
            <div className="wikiListMenu__wrap">
              <ul className="wikiListMenu__list">{this.renderMenu()}</ul>
            </div>
          </div>
        </div>
        <div className="wrapper">
          <div className="wikiListBlock">
            <h2 className="title wikiListBlock__title">{this.listName}</h2>
            {this.renderList()}
          </div>
        </div>
      </div>
    );
  }
}

export default MyWikiTop;
