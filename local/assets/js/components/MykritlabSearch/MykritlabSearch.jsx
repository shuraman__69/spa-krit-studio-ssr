import React, { Component } from 'react';
import BreadCrumbs from '../BreadCrumbs';
import Pagination from '../Pagination';
import SearchResult from './SearchResult';

class MykritlabSearch extends Component {
  constructor(props) {
    super(props);
    this.state = {
      itemsResult: [],
      title: '',
      totalPages: null,
      currentPage: null,
    };
    this.onPageClick = this.onPageClick.bind(this);
    this.awaitRequest = false;
    this.value = '';
  }

  componentDidMount() {
    let value = decodeURIComponent(window.location.search);
    value = value.replace('?q=', '');
    this.getSearch(value);
  }

  handleSubmit(e) {
    e.preventDefault();
    this.getSearch(this.value);
  }

  handleChange(e) {
    const { value } = e.target;
    this.value = value;
    window.history.replaceState(null, null, `?q=${value}`);
    this.getSearch(value);
  }

  onPageClick(num, e) {
    e.preventDefault();
    const data = {
      cur_page: num,
      value: this.value,
    };
    this.getItems(data);
  }

  getItems(data) {
    const self = this;
    if (!self.awaitRequest) {
      self.awaitRequest = true;
      const xhr = new XMLHttpRequest();

      const formData = new FormData();

      formData.append('AJAX', 'Y');
      formData.append('PARAMS', JSON.stringify(data));

      xhr.open('POST', '../../api/v3/search.json');
      xhr.send(formData);

      xhr.onload = function (response) {
        const result = JSON.parse(response.currentTarget.response);

        if (xhr.status === 200) {
          self.setState({
            itemsResult: result.data,
            totalPages: result.total_pages,
            currentPage: result.page,
          });
        }
        self.awaitRequest = false;
      };

      xhr.onerror = function () {
        console.log('ошибка');
        self.awaitRequest = false;
      };
    }
  }

  getSearch(value) {
    if (value.trim() !== '') {
      this.setState({
        title: `Результаты поиска по запросу «${value}»`,
      });
      this.value = value;
      const data = {
        value,
      };
      this.getItems(data);
    } else {
      this.setState({
        title: 'Результаты поиска',
        itemsResult: [],
        totalPages: null,
        currentPage: null,
      });
    }
  }

  renderResult() {
    const { itemsResult } = this.state;
    if (itemsResult.length > 0) {
      const arr = [];
      for (let i = 0; i < itemsResult.length; i += 1) {
        arr.push(this.renderRow(itemsResult[i]));
      }
      return <div className="searchPage__containerResult">{arr.map((el) => el)}</div>;
    }
    return (
      <div className="searchPage__empty">
        <div className="searchPage__emptyTitle">Ничего не найдено.</div>
        <div className="searchPage__emptyText">
          Попробуйте другой поисковый запрос
          <br />
          или вернитесь
          {' '}
          <a href="/" title="на главную">
            на главную
          </a>
          .
        </div>
      </div>
    );
  }

  renderRow(item) {
    return (
      <div className="searchPage__wrapper" key={item.category + Math.random()}>
        {item.items.map((el) => (
          <SearchResult
            item={el.items}
            parent={item.category}
            mark={this.value}
            child={el.category}
            key={item.category + Math.random()}
          />
        ))}
      </div>
    );
  }

  renderPagination() {
    const { totalPages, currentPage } = this.state;
    if (totalPages > 1) {
      return <Pagination items={totalPages} event={this.onPageClick} current={currentPage} />;
    }
    return false;
  }

  render() {
    const { title } = this.state;
    return (
      <div>
        <div className="whiteBack">
          <div className="wrapper">
            <div className="d-sm-flex">
              <BreadCrumbs value={title} />
            </div>
          </div>
        </div>
        <div className="wrapper">
          <div className="searchPage">
            <h2 className="title searchPage__title">{title}</h2>
            <div className="searchPage__container">
              <form onSubmit={(e) => this.handleSubmit(e)} className="searchPage__form">
                <input
                  type="text"
                  placeholder="Поиск..."
                  onChange={(e) => this.handleChange(e)}
                  value={this.value}
                  className="searchPage__input"
                />
              </form>
              {this.renderResult()}
              {this.renderPagination()}
            </div>
          </div>
        </div>
      </div>
    );
  }
}

export default MykritlabSearch;
