import React from 'react';
import PropTypes from 'prop-types';

const Links = ({ items }) => (
  <div className="post__bottomCol post__links">
    <h5>Ссылки на внешние источники</h5>
    {items.map((item) => (
      <div className="post__linksItem" key={item.name + Math.random()}>
        <a href={item.href} className="post__linksLink" title={item.name}>
          {item.name}
        </a>
      </div>
    ))}
  </div>
);

Links.propTypes = {
  items: PropTypes.instanceOf(Array).isRequired,
};

export default Links;
