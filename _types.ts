//WIKI STATE TYPES
export type topMenuType = {
    name: string
    shortName: string
    items: {
        id: number
        href: string
        name: string
        date: string
        changeDate: string
        favorite: boolean
    }[]
}
export type categoryType = {
    name: string
    href: string
    section_id: string
    highlighted: boolean
    extranet: boolean
    count: number
}
export type selectedSectionType = {
    categoryName: string
    selectedSectionItems: selectedSectionItemsType[]
    breadCrumbs: breadCrumbsType[]

}
export type selectedSectionItemsType = {
    name: string
    href: string
    section_id: string
    items: { name: string, href: string }[]
}
export type secBlockType = categoryType[]

export type breadCrumbsType = { label: string, href: string }
export type sectionMenuType = {
    href: string
    label: string
    isSelected: boolean
    value: string
    id: string
}

export type selectedArticleType = {
    favorite: boolean
    id: number
    links: any[]
    name: string
    section_id: number
    parent_section_id: number
    props: {
        datePublic: string
        userPublic: string
        dateChange: string
        userChange: string
    }
    relatedItems: any[]
    tags: string[]
    text: string
    voiting: {
        icon: string
        text: string
        isVoted: boolean
        users: string[]
        voteID: number | null
    }[]
    navigation: { name: string, items: string[] }[]
}
//==========================================


//ACCESSES STATE TYPES
export type accessesItemType = {
    creator: string
    href: string
    site: string
    date: string
    name: string
}

export type selectedAccessDataType = {
    creator: string
    date: string
    href: string
    id: string
    name: string
    site: string
}
export type selectedAccessItemType = {
    canChange: boolean
    database: string
    host: string
    id: number
    like: boolean
    login: string
    notice: string
    password: string
    port: string
    type: string
}

//================================