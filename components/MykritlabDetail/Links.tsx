import React, {FC} from 'react';
import PropTypes from 'prop-types';

const Links: FC<{ items: any[] }> = ({items}) => (
    <div className="post__bottomCol post__links">
        <h5>Ссылки на внешние источники</h5>
        {items.map((item) => (
            <div className="post__linksItem" key={item.name + Math.random()}>
                <a href={item} target='_blank' className="post__linksLink">
                    {item}
                </a>
            </div>
        ))}
    </div>
);

Links.propTypes = {
    items: PropTypes.instanceOf(Array).isRequired,
};

export default Links;
