import React from 'react';
import PropTypes from 'prop-types';

const Icon = ({ clickEvent }) => (
  <div className="header__icon">
    <a
      href="#menuMobile"
      className="header__iconBtn js-open-side"
      onClick={clickEvent}
      title="Открыть меню"
    >
      <svg
        width="18"
        height="18"
        viewBox="0 0 18 18"
        fill="none"
        xmlns="http://www.w3.org/2000/svg"
      >
        <circle cx="2.8125" cy="2.8125" r="2.8125" fill="#E7E7F4" />
        <circle cx="15.1875" cy="2.8125" r="2.8125" fill="#E7E7F4" />
        <circle cx="15.1875" cy="15.1875" r="2.8125" fill="#7C89AB" />
        <circle cx="2.8125" cy="15.1875" r="2.8125" fill="#E7E7F4" />
      </svg>
    </a>
  </div>
);

Icon.propTypes = {
  clickEvent: PropTypes.func.isRequired,
};

export default Icon;
