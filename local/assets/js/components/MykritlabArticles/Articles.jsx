import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { CSSTransition } from 'react-transition-group';
import ArticlesList from './ArticlesList';
import { slideToggle } from '../../modules/util';

const handleClick = (e) => {
  const parent = e.currentTarget.closest('.js-article-elem');
  const list = parent.querySelector('.js-articles-list');
  slideToggle(list, 500);
  if (parent.classList.contains('is-open')) {
    parent.classList.remove('is-open');
  } else {
    parent.classList.add('is-open');
  }
};

class Articles extends Component {
  constructor(props) {
    super(props);
    this.state = {
      sectionID: null,
      itemsEng: [],
      itemsRus: [],
      showList: false,
    };
  }

  componentDidMount() {
    this.getItems();
  }

  shouldComponentUpdate(nextProps) {
    const { sectionID } = this.state;
    return nextProps.itemID !== sectionID;
  }

  componentDidUpdate() {
    this.getItems();
  }

  getItems() {
    const self = this;
    const data = {
      id: self.props.itemID,
    };
    const xhr = new XMLHttpRequest();

    const formData = new FormData();

    formData.append('AJAX', 'Y');
    formData.append('PARAMS', JSON.stringify(data));

    xhr.open('GET', `../../api/v3/sections/${self.props.itemID}/section.json`);
    xhr.send(formData);

    xhr.onload = function (response) {
      const result = JSON.parse(response.currentTarget.response);

      if (xhr.status === 200) {
        const eng = [];
        const rus = [];
        if (result.length > 0) {
          result.sort((a, b) => {
            const nameA = a.name.toLowerCase();
            const nameB = b.name.toLowerCase();
            if (nameA < nameB) {
              return -1;
            }
            if (nameA > nameB) {
              return 1;
            }
            return 0;
          });
          result.forEach((item) => {
            let name = item.name.replace(/[0-9]/g, '');
            name = name.slice(0, 1);
            if (name.match(/[а-яА-Яё]/g)) {
              rus.push(item);
            } else {
              eng.push(item);
            }
          });
          self.setState({
            sectionID: self.props.itemID,
            itemsEng: [...eng],
            itemsRus: [...rus],
            showList: true,
          });
        }
      }
    };

    xhr.onerror = function () {
      console.log('ошибка');
    };
  }

  render() {
    const { itemsEng, itemsRus, showList } = this.state;
    return (
      <div className="articles__block">
        {itemsEng.length > 0 && (
          <CSSTransition
            in={showList}
            timeout={400}
            classNames="articles__catalog"
            unmountOnExit
            appear
          >
            <div className="articles__catalog articles__catalog--eng">
              {itemsEng.map((items) => (
                <ArticlesList items={items} event={handleClick} key={items.name + Math.random()} />
              ))}
            </div>
          </CSSTransition>
        )}
        {itemsRus.length > 0 && (
          <CSSTransition
            in={showList}
            timeout={400}
            classNames="articles__catalog"
            unmountOnExit
            appear
          >
            <div className="articles__catalog articles__catalog--rus">
              {itemsRus.map((items) => (
                <ArticlesList items={items} event={handleClick} key={items.name + Math.random()} />
              ))}
            </div>
          </CSSTransition>
        )}
      </div>
    );
  }
}

Articles.propTypes = {
  itemID: PropTypes.number.isRequired,
};

export default Articles;
