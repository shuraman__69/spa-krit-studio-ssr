import React, {useEffect, useState} from 'react';
import Slider from 'react-slick';

import MyWikiTopMenu from './MyWikiTopMenu';
import MyWikiTopList from './MyWikiTopList';
import MyWikiTopListEmpty from './MyWikiTopListEmpty';
import {useDispatch, useSelector} from "react-redux";
import {addFavoriteThunk, getMenuItemsThunk, setSelectedThunk} from "../../redux/reducers/wikiReducer";
import {AppStateType} from "../../redux/store";
import {topMenuType} from "../../_types";


const MyWikiTop = () => {
    const dispatch = useDispatch()
    const [_slider, set_Slider] = useState(false)
    const [listType, setListType] = useState('')
    const [listName, setListName] = useState('')
    const {isLoading} = useSelector((state: AppStateType) => state.authPage)


    const menuWiki: topMenuType[] = useSelector((state: AppStateType) => state.wiki.topMenuItems)
    const selectedItem: topMenuType[] = useSelector((state: AppStateType) => state.wiki.selectedItem) || []

    const settings = {
        dots: true,
        arrows: false,
        infinite: false,
        speed: 500,
        slidesToShow: 1,
        slidesToScroll: 1,
        variableWidth: true,
    };


    const handleClick = (e: React.MouseEvent, name: string) => {
        e.preventDefault()
        dispatch(setSelectedThunk(name))
    }

    const handleFavoriteClick = (fav: boolean, id: number, parentName: string | undefined = 'defaultvalue') => {
        dispatch(addFavoriteThunk(fav, id, parentName))
    }
    const setSlider = () => {
        if (window.innerWidth < 768 && !_slider) {
            set_Slider(true)
        }
        if (window.innerWidth >= 768 && _slider) {
            set_Slider(false)
        }
    };
    useEffect(() => {
        dispatch(getMenuItemsThunk())
    }, [])

    useEffect(() => window.removeEventListener('resize', setSlider))

    useEffect(() => {
        setSlider();
        window.addEventListener('resize', setSlider)
    }, [setSlider])
    useEffect(() => {
        if (selectedItem.length) {
            setListName(selectedItem[0].name)
        }
    }, [selectedItem])


    const renderItemsList = (items: topMenuType[]) => {
        return items.map((items, key: any) => {
            return (
                <MyWikiTopList
                    items={items}
                    handleFavoriteClick={(fav, id, parentName) => handleFavoriteClick(fav, id, parentName)}
                    key={key + Math.random()}
                />
            );
        });
    }

    const renderList = () => {
        if (selectedItem[0] && selectedItem[0].items.length > 0) {
            if (_slider) {
                return (
                    <div className="wikiListBlock__list">
                        <div className="wikiListBlock__slider js-slider">
                            <Slider {...settings}>
                                {renderItemsList(selectedItem)}
                            </Slider>
                        </div>
                    </div>
                );
            }
            return (
                <div className="wikiListBlock__list">
                    <div className="wikiListBlock__slider js-slider">
                        {renderItemsList(selectedItem)}
                    </div>
                </div>
            );
        }
        if(!isLoading){

            return (
                <div className="wikiListBlock__list">
                    <MyWikiTopListEmpty type={listType}/>
                </div>
            );
        }
        return false;
    }

    const renderMenu = () => {

        return menuWiki.map((item: topMenuType, index: number) => (
            <MyWikiTopMenu
                item={item}
                selectedItem={selectedItem}
                event={(e: React.MouseEvent, name: string) => handleClick(e, name)}
                key={index}
            />
        ));
    }


    return (
        <div className="wikiListTop">
            <div className="wikiListMenu">
                <div className="wrapper">
                    <div className="wikiListMenu__wrap">
                        <ul className="wikiListMenu__list">{renderMenu()}</ul>
                    </div>
                </div>
            </div>
            <div className="wrapper">
                <div className="wikiListBlock">
                    <h2 className="title wikiListBlock__title">{listName}</h2>
                    {renderList()}
                </div>
            </div>
        </div>
    );
}
export default MyWikiTop;
