import React from 'react';
import PropTypes from 'prop-types';
import classNames from 'classnames';

import AccessDetailListItem from './AccessDetailListItem';

const AccessDetailList = ({
  items, modalDeleteEvent, modalEditEvent, likeEvent, event,
}) => {
  const notice = items.filter((item) => item.notice.length > 0);
  return (
    <div
      className={classNames({
        accessDetail__list: true,
        'accessDetail__list--wide': notice.length > 0,
      })}
    >
      <div className="accessDetail__listWrap">
        <div className="accessDetail__item accessDetail__item--head">
          <div
            className="accessDetail__itemCol accessDetail__itemCol--head accessDetail__itemCol--type"
            data-title="Вид"
          />
          <div
            className="accessDetail__itemCol accessDetail__itemCol--head accessDetail__itemCol--gray accessDetail__itemCol--host"
            data-title="Хост"
          />
          <div
            className="accessDetail__itemCol accessDetail__itemCol--head accessDetail__itemCol--gray accessDetail__itemCol--notice"
            data-title="Прим."
            data-title-sm="Примечания"
          />
          <div
            className="accessDetail__itemCol accessDetail__itemCol--head accessDetail__itemCol--border accessDetail__itemCol--login"
            data-title="Логин"
          />
          <div
            className="accessDetail__itemCol accessDetail__itemCol--head accessDetail__itemCol--border accessDetail__itemCol--pass"
            data-title="Пароль"
          />
          <div className="accessDetail__itemCol accessDetail__itemCol--head accessDetail__itemCol--actions" />
        </div>
        {items.map((item) => (
          <AccessDetailListItem
            modalDeleteEvent={modalDeleteEvent}
            modalEditEvent={modalEditEvent}
            likeEvent={likeEvent}
            item={item}
            event={event}
            key={item.id + Math.random()}
          />
        ))}
      </div>
    </div>
  );
};

AccessDetailList.propTypes = {
  items: PropTypes.instanceOf(Array).isRequired,
  modalDeleteEvent: PropTypes.func.isRequired,
  modalEditEvent: PropTypes.func.isRequired,
  likeEvent: PropTypes.func.isRequired,
  event: PropTypes.func.isRequired,
};

export default AccessDetailList;
