import React, {FC} from 'react';
import PropTypes from 'prop-types';

type PropsType = {
    date: string
    user: string
    siteUrl: string
    site: string
}
const AccessDetailCard: FC<PropsType> = ({
                                             date, user, siteUrl, site,
                                         }) => (
    <div className="accessDetail__card">
        <div className="accessDetail__cardRow">
            <div className="accessDetail__cardCaption">Дата регистрации:</div>
            <div className="accessDetail__cardValue">{date}</div>
        </div>
        <div className="accessDetail__cardRow">
            <div className="accessDetail__cardCaption">Регистратор:</div>
            <div className="accessDetail__cardValue">{user}</div>
        </div>
        <div className="accessDetail__cardRow">
            <div className="accessDetail__cardCaption">Сайт:</div>
            <div className="accessDetail__cardValue">
                <a href={siteUrl}>{site}</a>
            </div>
        </div>
    </div>
);

export default AccessDetailCard;
