import React, {FC} from 'react';
import classNames from 'classnames';
import {menuType} from "../../redux/reducers/menuRudecer";
import Link from 'next/link';
import {useRouter} from "next/router";

const MenuItem: FC<{ item: menuType, id: number }> = ({item}) => {
    const {pathname} = useRouter()
    const isWiki = pathname.includes('wiki')
    const isAccess = pathname.includes('access')
    return (
        <li className="header__menuItem">
            <a
                href={item.href}
                className={classNames({
                    header__menuLink: true,
                    current: item.href.includes('wiki') && isWiki || item.href.includes('access') && isAccess
                })}
                target='_blank'
            >
                {item.name}
            </a>
        </li>
    )
};
const MenuReactItem: FC<{ item: menuType }> = ({item}) => {
    const {pathname} = useRouter()
    const isWiki = pathname.includes('wiki')
    const isAccess = pathname.includes('access')
    return (
        <li className="header__menuItem">
            <Link
                href={item.href}
            >
                <a className={classNames({
                    header__menuLink: true,
                    current: item.href.includes('wiki') && isWiki || item.href.includes('access') && isAccess
                })}
                >
                    {item.name}
                </a>
            </Link>
        </li>
    )
};

const Menu: FC<{ items: menuType[] }> = ({items}) => (
    <div className="header__menu">
        <ul className="header__menuList">
            {items && items.map((item: menuType) => {
                if (item.react) {
                    return <MenuReactItem item={item}
                                          key={item.id}/>

                } else {
                    return <MenuItem item={item} id={item.id}
                                     key={item.id}/>

                }
            })
            }
        </ul>
    </div>
);


export default Menu;
