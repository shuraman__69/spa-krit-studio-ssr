import React from 'react';
import PropTypes from 'prop-types';
import Parser from 'html-react-parser';
import classNames from 'classnames';

import SelectRender from '../Select';

const TasksItem = ({ item }) => (
  <div className={classNames({
    tasksItems__item: true,
    'tasksItems__item--active': item.status === 'active',
  })}
  >
    <a href="# " className="tasksItems__itemWrap">
      <div className="tasksItems__itemRow">
        <div className="tasksItems__itemTitle">{item.title}</div>
        <div className="tasksItems__itemCode">{item.code}</div>
      </div>
      <div className="tasksItems__itemRow">
        <div className="tasksItems__itemData tasksData">
          <div className="tasksData__item">
            <div className="tasksData__itemCaption">Ответственный: </div>
            <div className="tasksData__itemName">{item.responsible}</div>
          </div>
          <div className="tasksData__item tasksData__item--mobile">
            <div className="tasksData__itemCaption">Постановщик: </div>
            <div className="tasksData__itemCaption tasksData__itemCaption--mobile">Пост.: </div>
            <div className="tasksData__itemName">{item.producer}</div>
          </div>
        </div>
        <div className="tasksItems__itemStatus tasksStatus">
          <div
            className={classNames({
              tasksStatus__icon: true,
              'tasksStatus__icon--ready': item.status === 'ready',
              'tasksStatus__icon--control': item.status === 'oncontrol',
              'tasksStatus__icon--postponed': item.status === 'postponed',
            })}
          >
            {item.label}
          </div>
        </div>
      </div>
    </a>
  </div>
);

const TasksItems = ({
  item, select, selected, event,
}) => {
  const filteredItems = selected.value === 'allItems'
    ? item.tasks
    : item.tasks.filter((task) => task.value === selected.value);

  return (
    <div className="tasksContent__items tasksItems">
      <div className="tasksItems__title title">
        Ответственный:
        <span>{Parser(item.title)}</span>
      </div>
      <div className="tasksItems__sort">
        <span className="tasksItems__sortTitle">Статус задачи:</span>

        <SelectRender value={selected} select={select} onChange={event} />
      </div>
      <div className="tasksItems__list">
        {filteredItems.map((elem) => (
          <TasksItem item={elem} key={elem.parentID + Math.random()} />
        ))}
      </div>
    </div>
  );
};

TasksItem.propTypes = {
  item: PropTypes.shape({
    status: PropTypes.string.isRequired,
    title: PropTypes.string.isRequired,
    code: PropTypes.oneOfType([PropTypes.string, PropTypes.number]),
    responsible: PropTypes.string.isRequired,
    producer: PropTypes.string.isRequired,
    label: PropTypes.string.isRequired,
  }).isRequired,
};

TasksItems.propTypes = {
  item: PropTypes.shape({
    tasks: PropTypes.instanceOf(Array).isRequired,
    title: PropTypes.string.isRequired,
    value: PropTypes.oneOfType([PropTypes.string, PropTypes.number]),
  }).isRequired,
  event: PropTypes.func.isRequired,
  selected: PropTypes.instanceOf(Object).isRequired,
  select: PropTypes.instanceOf(Array).isRequired,
};

export default TasksItems;
