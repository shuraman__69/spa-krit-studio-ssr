import React, {useEffect, useState} from 'react';
import {useRouter} from "next/router";

import {useDispatch, useSelector} from "react-redux";
import {
    getSelectedAccessItemThunk,
    likeEventThunk,
    removeAccessThunk,
    setSelectedAccess
} from "../../../redux/reducers/accessesReducer";
import {AppStateType} from "../../../redux/store";
import AccessDetailCard from "./AccessDetailCard";
import AccessDetailList from "./AccessDetailList";
import {selectedAccessItemType} from "../../../_types";
import OftenUsedList from "./OftenUsedList";
import AccessDetailFormAdd from "./AccessDetailFormAdd";
import AccessDetailForm from "./AccessDetailForm";
import AccessDetailFormDelete from "./AccessDetailFormDelete";
import BreadCrumbs from "../../other/BreadCrumbs";
import {toast, Toaster} from "react-hot-toast";

const copyRowClick = async (e: any) => {
    e.preventDefault();
    window.getSelection()!.removeAllRanges();
    let copyText;
    if (e.target.closest('.js-copy-row')) {
        copyText = e.target.closest('.js-copy-row');
        console.log(copyText.innerText)
    }
    if (e.target.classList.contains('js-copy-element')) {
        copyText = e.target;
    }
    navigator.clipboard.writeText(copyText.innerText.trim()).then(() => undefined);
    toast.success('Скопировано!')

};

const AccessDetail = () => {
    const dispatch = useDispatch()
    const {query} = useRouter()

    const [deleteModalIsOpen, setDeleteModalIsOpen] = useState(false)
    const [addModalIsOpen, setAddModalIsOpen] = useState(false)
    const [editModalIsOpen, setEditModalIsOpen] = useState(false)
    const {items, data} = useSelector((state: AppStateType) => state.accesses.selectedAccessItem)

    const closeModals = () => {
        setDeleteModalIsOpen(false)
        setAddModalIsOpen(false)
        setEditModalIsOpen(false)
    }

    const getOUItems = (items: selectedAccessItemType[]) => {
        let itemsOU: any = []
        if (items.length > 0) {
            items.forEach(item => {
                if (item.like) {
                    itemsOU = [...itemsOU, item]
                }
            })
        }
        return itemsOU
    }

    const openEditModal = (id: number) => {
        setEditModalIsOpen(true)
        dispatch(setSelectedAccess(id, query.id.toString()))
    }

///////////////////////////////DELETE MODAL//////////////////
    const openDeleteModal = (id: number) => {
        setDeleteModalIsOpen(true)
        dispatch(setSelectedAccess(id, query.id.toString()))
    }

    const removeItem = () => {
        dispatch(removeAccessThunk())
        setDeleteModalIsOpen(false)
    }

    ///////////////////////////////////////////////////////////

    const likeEvent = (accessID: number, itemLike: boolean) => {
        dispatch(likeEventThunk(accessID, query.id.toString(), itemLike))
    }


    return (
        <div>
            <div><Toaster
                position="top-right"
                reverseOrder={false}
            /></div>
            <div className="whiteBack">
                <div className="wrapper">
                    <div className="d-sm-flex">
                        <BreadCrumbs lastItem={data.name}/>
                    </div>
                </div>
            </div>
            <div className="wrapper">
                <div className="accessDetail">
                    <h2 className="title accessDetail__title">{data.name}</h2>
                    <div className="accessDetail__top">
                        <div className="accessDetail__topRow">
                            <AccessDetailCard
                                date={data.date}
                                user={data.creator}
                                siteUrl={data.href}
                                site={data.site}
                            />
                            {getOUItems(items) && getOUItems(items).length > 0 && (
                                <OftenUsedList event={copyRowClick} items={getOUItems(items)}/>
                            )}
                        </div>
                        <div className="accessDetail__topRow">
                            <AccessDetailFormAdd
                                setAddModalIsOpen={setAddModalIsOpen}
                                closeModals={closeModals}
                                open={addModalIsOpen}
                            />
                            <AccessDetailForm contrAgentID={data.id}/>
                        </div>
                    </div>
                    {/*{itemsSearch && itemsSearch.length > 0 && (
              <div className="accessDetail__bottom">
                <AccessDetailList
                  event={copyRowClick}
                  modalDeleteEvent={(e) => this.openModal(e, 'delete')}
                  likeEvent={(e) => this.setFavorite(e)}
                  modalEditEvent={(e) => this.getEditData(e)}
                  items={itemsSearch}
                />

            */}
                    <AccessDetailList
                        event={copyRowClick}
                        openDeleteModal={openDeleteModal}
                        likeEvent={(id: number, itemLike: boolean) => likeEvent(id, itemLike)}
                        items={items}
                        openEditModal={(id: number) => openEditModal(id)}
                        editModalIsOpen={editModalIsOpen}
                        setEditModalIsOpen={setEditModalIsOpen}
                        closeModals={closeModals}
                    />
                </div>
                <AccessDetailFormDelete
                    removeItem={() => removeItem()}
                    closeModals={closeModals}
                    open={deleteModalIsOpen}
                />
            </div>
        </div>
    );

}

export default AccessDetail;
