import React, {FC} from 'react';
import PropTypes from 'prop-types';
import VoitingItem from './VoitingItem';

import {getSuffix} from '../../modules/util';
import {wikiAPI} from "../../../api/api";
import {setSelectedItemThunk} from "../../../redux/reducers/wikiReducer";
import {useDispatch} from "react-redux";


export type votingItemsType = {
    icon: string
    isVoted: boolean
    text: string
    users: string[]
    voteID: number | null

}
export type VotingPropsType = {
    items: votingItemsType[]
    articleID: number
}
const Voiting: FC<VotingPropsType> = ({items, articleID}) => {
    const dispatch = useDispatch()
    const setLikeVote = async (id: number) => {
        await wikiAPI.setVote(id, true)
        dispatch(setSelectedItemThunk(id.toString()))
    };

    const setDisLikeVote = async (id: number) => {
        await wikiAPI.setVote(id, false)
        dispatch(setSelectedItemThunk(id.toString()))
    };

    let isVoted = 0;
    for (let i = 0; i < items.length; i += 1) {
        if (items[i].isVoted) {
            isVoted += 1;
        }
    }
    return (
        <div className="post__rating">
            {items.map((item) => (
                <VoitingItem item={item}
                             setVote={item.voteID === 1 ? (id: number) => setLikeVote(id) : (id: number) => setDisLikeVote(id)}
                             articleID={articleID}
                             key={item.voteID}/>
            ))}
        </div>
    );
};

export default Voiting;
