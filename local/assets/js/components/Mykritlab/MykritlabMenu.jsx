import React, {Component} from 'react';
import Slider from 'react-slick';
import MykritlabMenuItem from './MykritlabMenuItem';

class MykritlabMenu extends Component {
    constructor(props) {
        super(props);

        this.setParams();
    }

    componentDidMount() {
        this.getSettings();
        window.addEventListener('resize', this.setSlider);
    }

    componentWillUnmount() {
        window.removeEventListener('resize', this.setSlider);
    }

    setParams() {
        this.state = {
            menu: [],
            slider: false,
        };

        this.settings = {
            dots: true,
            arrows: false,
            infinite: true,
            speed: 500,
            slidesToShow: 1,
            slidesToScroll: 1,
        };
    }

    setSlider = () => {
        const {slider} = this.state;

        if (window.innerWidth < 768 && !slider) {
            this.setState({
                slider: true,
            });
        }
        if (window.innerWidth >= 768 && slider) {
            this.setState({
                slider: false,
            });
        }
    };

    getSettings() {
        const self = this;

        xhr.onerror = function () {
            console.log('ошибка');
        };
    }

    renderItems() {
        const self = this;
        return self.state.menu.map((item) => {
                return <MykritlabMenuItem item={item} key={item.name + Math.random()}/>

            }
        )
    }

    render() {
        const {slider} = this.state;
        return (
            <div className="menuSectionListWrap">
                <div className="menuSectionList">
                    <h1 className="title title--main">My kritlab</h1>
                    <div className="menuSectionList__slider js-menu-slider">
                        {slider && <Slider {...this.settings}>{this.renderItems()}</Slider>}
                        {!slider && this.renderItems()}
                    </div>
                </div>
            </div>
        );
    }
}

export default MykritlabMenu;
