import React, { Component } from 'react';
import BreadCrumbs from '../BreadCrumbs';
import AccessDetailCard from './AccessDetailCard';
import OftenUsedList from './OftenUsedList';
import AccessDetailForm from './AccessDetailForm';
import AccessDetailFormAdd from './AccessDetailFormAdd';
import AccessDetailFormDelete from './AccessDetailFormDelete';
import AccessDetailFormEdit from './AccessDetailFormEdit';
import AccessDetailList from './AccessDetailList';

const copyRowClick = (e) => {
  e.preventDefault();
  window.getSelection().removeAllRanges();
  let copyText;
  if (e.target.closest('.js-copy-row')) {
    copyText = e.target.closest('.js-copy-row');
  }
  if (e.target.classList.contains('js-copy-element')) {
    copyText = e.target;
  }
  const range = document.createRange();
  range.selectNode(copyText);
  window.getSelection().addRange(range);
  document.execCommand('copy');
  window.getSelection().removeAllRanges();
};

class AccessDetail extends Component {
  constructor(props) {
    super(props);
    this.state = {
      data: {},
      itemsOU: [],
      items: [],
      itemsSearch: [],
      deleteModalIsOpen: false,
      editModalIsOpen: false,
      addModalIsOpen: false,
    };
    this.title = document.getElementById('access-detail').getAttribute('data-title');
    this.clickBtnId;
    this.editData = {};
  }

  componentDidMount() {
    this.getItems();
  }

  handleFormSubmit(e) {
    e.preventDefault();
    const self = this;
    const form = e.currentTarget;
    const formType = form.getAttribute('data-form');
    const data = new FormData(form);

    const xhr = new XMLHttpRequest();

    const formData = new FormData();

    formData.append('AJAX', 'Y');
    formData.append('PARAMS', JSON.stringify(data));

    xhr.open('POST', '../../api/v3/accesses/id/detail.json');
    xhr.send(formData);

    xhr.onload = function () {
      // const result = JSON.parse(response.currentTarget.response);

      if (xhr.status === 200) {
        self.closeModal(formType);
        self.getItems();
      }
    };
    xhr.onerror = function () {
      console.log('ошибка');
    };
  }

  handleChange(e) {
    e.preventDefault();
    const value = e.target.value.toLowerCase();
    const { items } = this.state;

    if (value.length > 0) {
      const itemsNew = items.filter((item) => {
        const type = item.type.toLowerCase();
        const host = item.host.toLowerCase();
        const login = item.login.toLowerCase();
        if (
          type.indexOf(value) !== -1
          || host.indexOf(value) !== -1
          || login.indexOf(value) !== -1
        ) {
          return item;
        }
        return false;
      });
      this.setState({
        itemsSearch: itemsNew,
      });
    } else {
      this.setState({
        itemsSearch: items,
      });
    }
  }

  getEditData(e) {
    fetch(
      `../../api/v3/accesses/id/edit.json?id=${e.currentTarget
        .closest('[data-id]')
        .getAttribute('data-id')}`,
      {
        headers: {
          'Content-Type': 'application/json',
        },
      },
    )
      .then((response) => response.json())
      .then((result) => {
        this.editData = result;
        this.openModal(e, 'edit');
      })
      .catch(() => {
        console.log('ошибка');
      });
  }

  setFavorite(e) {
    const data = {
      itemID: e.currentTarget.closest('[data-id]').getAttribute('data-id'),
      is_favorite: e.currentTarget.closest('[data-id]').querySelector('.likeIcon--liked') !== null,
    };
    fetch('../../assets/js/api/v3/accesses/id/detail.json', {
      method: 'POST',
      body: data,
      headers: {
        'Content-Type': 'application/json',
      },
    })
      .then((response) => response.json())
      .then(() => {
        this.getItems();
      }),
    () => {
      console.log('ошибка');
    };
  }

  getItems() {
    const self = this;
    const xhr = new XMLHttpRequest();

    const formData = new FormData();

    formData.append('AJAX', 'Y');

    xhr.open('GET', '../../api/v3/accesses/id/detail.json');
    xhr.send(formData);

    xhr.onload = function (response) {
      const result = JSON.parse(response.currentTarget.response);

      if (xhr.status === 200) {
        const { items } = result;
        const itemsOU = items.filter((item) => item.oftenused === true);
        items.sort((a, b) => {
          if (a.sort < b.sort) {
            return -1;
          }
          if (a.sort > b.sort) {
            return 1;
          }
          return 0;
        });
        self.setState({
          data: result.data,
          items,
          itemsSearch: items,
          itemsOU,
        });
      }
    };
    xhr.onerror = function () {
      console.log('ошибка');
    };
  }

  removeItem() {
    fetch(`../../api/v3/accesses/id/detail.json?id=${this.clickBtnId}`, {
      method: 'DELETE',
      headers: {
        'Content-Type': 'application/json',
      },
    })
      .then((response) => response.json())
      .then(() => {
        this.clickBtnId = '';
        this.getItems();
        this.closeModal('delete');
      }),
    () => {
      console.log('ошибка');
    };
  }

  openModal(e, type) {
    this.setState({ [`${type}ModalIsOpen`]: true });
    if (type === 'remove') {
      this.clickBtnId = e.currentTarget.closest('[data-id]').getAttribute('data-id');
    }
  }

  closeModal(type) {
    this.setState({ [`${type}ModalIsOpen`]: false });
  }

  render() {
    const {
      data,
      itemsOU,
      addModalIsOpen,
      itemsSearch,
      editModalIsOpen,
      deleteModalIsOpen,
    } = this.state;
    return (
      <div>
        <div className="whiteBack">
          <div className="wrapper">
            <div className="d-sm-flex">
              <BreadCrumbs value={this.title} />
            </div>
          </div>
        </div>
        <div className="wrapper">
          <div className="accessDetail">
            <h2 className="title accessDetail__title">{this.title}</h2>
            <div className="accessDetail__top">
              <div className="accessDetail__topRow">
                <AccessDetailCard
                  date={data.date}
                  user={data.user}
                  siteUrl={data.siteurl}
                  site={data.site}
                />
                {itemsOU && itemsOU.length > 0 && (
                  <OftenUsedList event={copyRowClick} items={itemsOU} />
                )}
              </div>
              <div className="accessDetail__topRow">
                <AccessDetailFormAdd
                  event={(e) => this.handleFormSubmit(e)}
                  openEvent={(e) => this.openModal(e, 'add')}
                  closeEvent={() => this.closeModal('add')}
                  open={addModalIsOpen}
                />
                <AccessDetailForm event={(e) => this.handleChange(e)} />
              </div>
            </div>
            {itemsSearch && itemsSearch.length > 0 && (
              <div className="accessDetail__bottom">
                <AccessDetailList
                  event={copyRowClick}
                  modalDeleteEvent={(e) => this.openModal(e, 'delete')}
                  likeEvent={(e) => this.setFavorite(e)}
                  modalEditEvent={(e) => this.getEditData(e)}
                  items={itemsSearch}
                />
              </div>
            )}
            <AccessDetailFormDelete
              eventRemove={(e) => this.removeItem(e)}
              closeEvent={() => this.closeModal('delete')}
              open={deleteModalIsOpen}
            />
            <AccessDetailFormEdit
              data={this.editData}
              host={data.host}
              type={data.type}
              login={data.login}
              password={data.password}
              database={data.database}
              port={data.port}
              notice={data.notice}
              event={(e) => this.handleFormSubmit(e)}
              closeEvent={() => this.closeModal('edit')}
              open={editModalIsOpen}
            />
          </div>
        </div>
      </div>
    );
  }
}

export default AccessDetail;
