import React from 'react';
import PropTypes from 'prop-types';

const RelatedItems = ({ items }) => (
  <div className="post__bottomCol post__related">
    <h5>Связанные статьи</h5>
    {items.map((item) => (
      <div className="post__relatedItem" key={item.name + Math.random()}>
        <a href={item.href} className="post__relatedLink" title={item.name}>
          {item.name}
        </a>
      </div>
    ))}
  </div>
);

RelatedItems.propTypes = {
  items: PropTypes.instanceOf(Array).isRequired,
};

export default RelatedItems;
