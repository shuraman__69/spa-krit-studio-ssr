import React, {Component} from 'react';

import SelectRender from '../Select';
import BreadCrumbs from '../BreadCrumbs';
import Articles from './Articles';

class MykritlabArticles extends Component {
    constructor(props) {
        super(props);
        this.state = {
            sections: [],
            selectedSection: {},
        };
    }

    componentDidMount() {
        this.getSections();
    }

    handleChange(value) {
        const {sections} = this.state;
        const items = sections.concat();

        items.find(this.isSelected, value.value);
        const sectionsItem = items.filter((item) => item.isSelected)[0];
        this.setState({
            sections: items,
            selectedSection: sectionsItem,
        });
    }

    getSections() {
        const xhr = new XMLHttpRequest();
        const formData = new FormData();

        formData.append('AJAX', 'Y');

        xhr.open('GET', '../../api/v3/sections/sections.json');
        xhr.send(formData);

        xhr.onload = (response) => {
            const result = JSON.parse(response.currentTarget.response);

            if (xhr.status === 200) {
                const sectionsItem = result.filter((item) => item.isSelected)[0];
                this.setState({
                    sections: result,
                    selectedSection: sectionsItem,
                });
            }
        };

        xhr.onerror = function () {
            console.log('ошибка');
        };
    }

    isSelected(element) {
        if (element.isSelected) {
            element.isSelected = element.value === this;
        }
    }

    render() {
        const {selectedSection, sections} = this.state;
        const notEmpty = Object.keys(selectedSection).length > 0;

        return (
            <div>
                <div className="whiteBack">
                    <div className="wrapper">
                        <div className="d-sm-flex">
                            {notEmpty && <BreadCrumbs value={selectedSection.label}/>}
                            {notEmpty && (
                                <SelectRender
                                    value={{label: selectedSection.label}}
                                    select={sections}
                                    onChange={(e) => this.handleChange(e)}
                                />
                            )}
                        </div>
                    </div>
                </div>
                <div className="wrapper">
                    <div className="articles">
                        {notEmpty && <h2 className="title">{selectedSection.label}</h2>}
                        {notEmpty && <Articles itemID={selectedSection.id}/>}
                    </div>
                </div>
            </div>
        );
    }
}

export default MykritlabArticles;
