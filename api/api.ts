import axios from 'axios'
import {AddOrEditFormType} from "../hooks/useAccess";


//TYPES and instance
export type MyResponseType<D = {}, RC = null> = {
    data: D
    resultCode: RC
}
type menuType = {
    href: string
    image: string
    name: string
    status?: number
}
export const apiKey = "tJmPKbUDEoW9bK7yvex8ohEu"
export const instance = axios.create({
    baseURL: "https://deploy-krit-studio.krit.studio/api/v3/",
    headers: {
        apiKey:apiKey
    }
    ,
    validateStatus: function (status) {
        return status >= 200 && status <= 500; // default
    }
})
//=============================================


//USER API
export const userAPI = {
    getUserInfo: () => {
        return instance.get(`user/info/`)
            .then((response) => {
                return {
                    userInfo: response.data.data,
                    status: response.status
                }
            })
    },
    getAuthLink: () => {
        return instance.get('user/authLink/').then(res => {

            console.log("RESPONSE" + res.data)
            if(res.data.data){
                return res.data.data.link
            }
            else {
                return 'link'
            }
        })
    },
    logout: () => {
        return instance.get('user/logout/')
    },
}
//===================================

//MENU API
export const myKritLabMenuAPI = {
    getMenu: () => {
        return instance.get<{ data: menuType }>('main/menu/').then((res) => {
            return {menu: res.data.data, status: res.status}
        })
    }
}
//===================================

//SETTINGS API

export const settingsAPI = {
    getUserTheme: () => {
        return instance.get<MyResponseType<{ darkTheme: boolean }>>('user/settings/').then(res => {
            if (res.data.data === null) {
                return {darkTheme: false, status: res.status}
            }
            return {darkTheme: res.data.data.darkTheme, status: res.status}
        })
    },
    changeUserTheme: (newTheme: boolean) => {
        return instance.post<MyResponseType<{ darkTheme: boolean }>>('user/settings/', {darkTheme: newTheme}).then(res => {
            return {darkTheme: res.data.data.darkTheme, status: res.status}
        })
    }
}

//===================================

//WIKI API


export const wikiAPI = {
    getSections: () => {
        return instance.get('wiki/sections/').then(res => ({sections: res.data.data, status: res.status}))
    },
    getTopMenu: () => {
        return instance.get('wiki/items/').then(res => ({
            items: res.data.data,
            status: res.status
        }))
    },
    getSelectedSection: (sectionId: string) => {
        return instance.get(`wiki/sections/${sectionId}/`).then(res => {
            return {selectedSection: res.data.data, status: res.status}
        })
    },
    getBreadCrumbs: (sectionId: string) => {
        return instance.get(`wiki/breadcrumb/${sectionId}/`).then(res => {
            return {breadCrumbs: res.data.data, status: res.status}
        })
    },
    getSelectSectionMenu: (sectionId: string) => {
        return instance.get(`wiki/sectionMenu/${sectionId}/`).then(res => {
            return {sectionMenu: res.data.data, status: res.status}
        })
    },
    getSelectedItem: (itemId: string) => {
        return instance.get(`wiki/item/${itemId}/`).then(res => {

            return {article: res.data.data, status: res.status}
        })
    },
    addFavorite: (fav: boolean, id: number) => {
        return instance.post(`wiki/item/${id}/`, {favorite: !fav})
    },
    setVote: (id: number, vote: boolean) => {
        return instance.post(`wiki/item/${id}/`, {vote: vote})
    }
}

//===================================


//ACCESSES API

export const accessesAPI = {
    getAccessesItems: () => {
        return instance.get('access/items/').then(res => {
            return {
                items: res.data.items,
                totalPages: res.data.totalPages,
                currentPage: res.data.page,
                status: res.status
            }
        })
    },
    changePage: (page: number) => {
        return instance.get(`access/items/?page=${page}`).then(res => {
            return {
                items: res.data.items,
                totalPages: res.data.totalPages,
                currentPage: res.data.page
            }
        })
    },
    contrAgentSearch: (value: string) => {

        return instance.get(`access/items/?search=${value}`).then(res => {
            return {
                items: res.data.items,
                totalPages: res.data.totalPages,
                currentPage: res.data.page
            }
        })
    },
    accessSearch: (id: string, value: string) => {
        return instance.get(`access/item/${id}/?search=${value}`).then(res => {
            return {
                data: res.data.data,
                items: res.data.items
            }
        })
    },
    getSelectedAccessItem: (id: string) => {
        return instance.get(`access/item/${id}/`).then(res => {
            return {
                data: res.data.data,
                items: res.data.items,
                status: res.status
            }
        })
    },
    deleteAccess: (id: number, contragentID: string) => {
        return instance.delete(`access/item/${contragentID}/`, {data: {id: id}})
    },
    addAccess: (form: AddOrEditFormType, contragentID: string) => {
        return instance.put(`access/item/${contragentID}/`, {...form})
    },
    saveAccessEdits: (form: AddOrEditFormType, contragentID: string, id: number) => {
        return instance.post(`access/item/${contragentID}/`, {...form, id: id})
    },
    likeEvent: (id: number, contragentID: string, itemLike: boolean) => {
        return instance.post(`access/item/${contragentID}/`, {id, like: !itemLike})
    }

}

//====================================