import React from 'react';
import PropTypes from 'prop-types';
import InlineSVG from 'svg-inline-react';

const MykritlabMenuItem = ({ item }) => (
  <div className="menuSectionList__item">
    <a href={item.href} className="menuSectionList__itemWrap">
      <div className="menuSectionList__itemContent">
        <div className="menuSectionList__itemIcon">
          <InlineSVG src={item.image} />
        </div>
        <div className="menuSectionList__itemCaption">{item.name}</div>
      </div>
    </a>
  </div>
);

MykritlabMenuItem.propTypes = {
  item: PropTypes.shape({
    href: PropTypes.string.isRequired,
    name: PropTypes.string.isRequired,
    image: PropTypes.string.isRequired,
  }).isRequired,
};

export default MykritlabMenuItem;
