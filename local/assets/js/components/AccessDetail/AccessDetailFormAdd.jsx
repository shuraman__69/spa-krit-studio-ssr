import React from 'react';
import Select from 'react-select';
import PropTypes from 'prop-types';
import RenderModal from '../RenderModal';

const requiredFields = ['host', 'type', 'login', 'password'];

const hasValue = (element) => document.querySelector(`[name="${element}"]`).value.length > 0;

const onChangeInput = () => {
  setTimeout(() => {
    if (!requiredFields.every(hasValue)) {
      document.querySelector('.addNewAccess__btn').setAttribute('disabled', 'disabled');
    } else {
      document.querySelector('.addNewAccess__btn').removeAttribute('disabled');
    }
  }, 10);
};

const AccessDetailFormAdd = ({
  openEvent, open, closeEvent, event,
}) => (
  <div className="accessDetail__add">
    <button type="button" className="accessDetail__addLink" onClick={openEvent}>
      <span>
        <svg
          width="31"
          height="11"
          viewBox="0 0 31 11"
          fill="none"
          xmlns="http://www.w3.org/2000/svg"
        >
          <path
            d="M1.25 6.0013H29.5833M29.5833 6.0013L25.4167 1.41797M29.5833 6.0013L25.4167 10.168"
            stroke="white"
            strokeWidth="1.5"
            strokeLinecap="round"
          />
        </svg>
      </span>
      Добавить новый доступ
    </button>
    <RenderModal isOpen={open} eventClose={closeEvent}>
      <div className="addNewAccess">
        <form className="addNewAccess__form formBlock" onSubmit={event} data-form="add">
          <div className="formBlock__title title">Создать новый доступ</div>
          <div className="formBlock__content">
            <div className="formBlock__row">
              <div className="formBlock__rowWrap">
                <div className="inputHolder">
                  <input
                    type="text"
                    name="HOST"
                    className="inputHolder__input"
                    placeholder="Хост"
                    onChange={(e) => onChangeInput(e)}
                  />
                </div>
              </div>
            </div>
            <div className="formBlock__row">
              <div className="formBlock__rowWrap">
                <div className="inputHolder">
                  <Select
                    name="TYPE"
                    options={[
                      {
                        label: 'CMS',
                        value: '1',
                      },
                      {
                        label: 'Basic auth',
                        value: '2',
                      },
                      {
                        label: 'SSH',
                        value: '3',
                      },
                      {
                        label: 'VPN',
                        value: '4',
                      },
                    ]}
                    placeholder="Вид доступа"
                    isSearchable={false}
                    className="styledSelect"
                    classNamePrefix="styledSelect"
                    onChange={(e) => onChangeInput(e)}
                  />
                </div>
              </div>
            </div>
            <div className="formBlock__row">
              <div className="formBlock__rowWrap">
                <div className="inputHolder">
                  <input
                    type="text"
                    name="LOGIN"
                    className="inputHolder__input"
                    placeholder="Логин"
                    onChange={(e) => onChangeInput(e)}
                  />
                </div>
              </div>
            </div>
            <div className="formBlock__row">
              <div className="formBlock__rowWrap">
                <div className="inputHolder">
                  <input
                    type="text"
                    name="PASSWORD"
                    className="inputHolder__input"
                    placeholder="Пароль"
                    onChange={(e) => onChangeInput(e)}
                  />
                </div>
              </div>
            </div>
            <div className="formBlock__row">
              <div className="formBlock__rowWrap">
                <div className="inputHolder">
                  <input type="text" className="inputHolder__input" placeholder="База данных" />
                </div>
              </div>
            </div>
            <div className="formBlock__row">
              <div className="formBlock__rowWrap">
                <div className="inputHolder">
                  <input
                    type="text"
                    name="PORT"
                    className="inputHolder__input"
                    placeholder="Порт"
                  />
                </div>
              </div>
            </div>
            <div className="formBlock__row formBlock__row--textarea">
              <div className="formBlock__rowWrap">
                <div className="inputHolder">
                  <textarea
                    type="text"
                    name="COMMENT"
                    className="inputHolder__input inputHolder__input--textarea"
                    placeholder="Примечания"
                  />
                </div>
              </div>
            </div>
            <div className="formBlock__row formBlock__row--btn">
              <div className="formBlock__rowWrap">
                <button type="submit" className="addNewAccess__btn" disabled>
                  <span>
                    <svg
                      width="36"
                      height="13"
                      viewBox="0 0 31 11"
                      fill="none"
                      xmlns="http://www.w3.org/2000/svg"
                    >
                      <path
                        d="M1.25 6.0013H29.5833M29.5833 6.0013L25.4167 1.41797M29.5833 6.0013L25.4167 10.168"
                        stroke="white"
                        strokeWidth="1.5"
                        strokeLinecap="round"
                      />
                    </svg>
                  </span>
                  Создать
                </button>
              </div>
            </div>
          </div>
        </form>
      </div>
    </RenderModal>
  </div>
);

AccessDetailFormAdd.propTypes = {
  openEvent: PropTypes.func.isRequired,
  open: PropTypes.bool.isRequired,
  closeEvent: PropTypes.func.isRequired,
  event: PropTypes.func.isRequired,
};

export default AccessDetailFormAdd;
