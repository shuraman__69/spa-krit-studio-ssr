import React, {useEffect, useState} from 'react';
import Pagination from '../other/Pagination';
import AccessesForm from './AccessesForm';
import AccessesItem from './AccessesItem';
import {useDispatch, useSelector} from "react-redux";
import {changePageThunk, searchThunk} from "../../redux/reducers/accessesReducer";

import {AppStateType} from "../../redux/store";

let searchInputTimer = 1

const Accesses = () => {
    const dispatch = useDispatch()
    let windowWidth;
    const {items, currentPage, totalPages} = useSelector((state: AppStateType) => state.accesses)
    const [searchValue, setSearchValue] = useState<string>()
    const handleClick = (number: number, e: React.MouseEvent) => {
        e.preventDefault()
        dispatch(changePageThunk(number))
    }
    const handleChange = (event: React.ChangeEvent<HTMLInputElement>) => {
        setSearchValue(event.target.value)
    }
    useEffect(() => {
        if (searchValue === undefined) {
            return
        }
        clearTimeout(searchInputTimer)
        // @ts-ignore
        searchInputTimer = setTimeout(() => {
            dispatch(searchThunk(searchValue))
        }, 500)
    }, [searchValue])

    if (process.browser) {
        windowWidth = window.innerWidth
    }


    const renderItems = () => {
        if (items.length > 0) {
            return (
                <div className="accesses__list">
                    <div className="accesses__listWrap">
                        <div className="accesses__item accesses__item--head">
                            <div
                                className="accesses__itemCol accesses__itemCol--head"
                                data-title="Наимен. / Рег. дата"
                                data-title-sm="Наименование / Рег. дата"
                            >{windowWidth < 600 ? "Наимен. / Рег. дата" : "Наименование / Рег. дата"}
                            </div>
                            <div
                                className="accesses__itemCol accesses__itemCol--head accesses__itemCol--gray"
                                data-title="Регистратор"
                            >Регистратор
                            </div>
                            <div
                                className="accesses__itemCol accesses__itemCol--site accesses__itemCol--head"
                                data-title="Сайт"
                            >Сайт
                            </div>
                        </div>
                        {items.map((el) => (
                            <AccessesItem item={el} key={el.date + Math.random()}/>
                        ))}
                    </div>
                </div>
            );
        }
        return false;
    }

    const renderPagination = () => {
        if (totalPages > 1) {
            return <Pagination items={totalPages}
                               event={(number: number, e: React.MouseEvent) => handleClick(number, e)}
                               currentPage={currentPage}/>;
        }
        return false;
    }
    return (
        <div className="wrapper">

            <div className="accesses">
                <h2 className="title accesses__title">Менеджер доступов</h2>
                <div className="accesses__container">
                    <AccessesForm searchValue={searchValue}
                                  handleChange={(e: React.ChangeEvent<HTMLInputElement>) => handleChange(e)}/>
                    {renderItems()}
                    {renderPagination()}
                </div>
            </div>
        </div>
    );
}

export default Accesses;
