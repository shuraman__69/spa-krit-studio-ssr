import {InferActionsType} from "../store";
import {myKritLabMenuAPI} from "../../api/api";
import {SET_LOADING_FALSE} from "./authReducer";


const SET_MAIN_MENU = 'SET_MAIN_MENU';

export type menuType = {
    href: string
    name: string
    image: string
    react: boolean
    id: number
}
export type InitialDataStateType = {
    menu: menuType[]
};
const initialState: InitialDataStateType = {
    menu: []
}
const menuReducer = (state = initialState, action: ActionsType) => {
    switch (action.type) {
        case SET_MAIN_MENU:
            return {
                ...state,
                menu: [...action.data]
            }
        default:
            return state;
    }
}
type ActionsType = InferActionsType<typeof actions>
export const actions = {
    setMainMenu: (data: any) => ({type: SET_MAIN_MENU, data}) as const,
}

export const getMenuThunk = () => {
    return async (dispatch: any) => {
        try {
            let response = await myKritLabMenuAPI.getMenu()
            if (response.status === 200) {
                dispatch(actions.setMainMenu(response.menu))
            }
        } catch (error) {
            console.log(error)
        } finally {
            dispatch(SET_LOADING_FALSE())
        }
    }
}

export default menuReducer;