import React from 'react';
import PropTypes from 'prop-types';
import Highlighter from 'react-highlight-words';

const ResultItem = ({ item, mark }) => (
  <div className="searchPage__item">
    <a href={item.href} className="searchPage__itemLink" title={item.caption}>
      <Highlighter
        highlightClassName="searchPage__itemLinkMarked"
        searchWords={[mark]}
        autoEscape
        textToHighlight={item.caption}
      />
    </a>
  </div>
);

const SearchResult = ({
  parent, child, item, mark,
}) => (
  <div className="searchPage__row">
    <div className="searchPage__rowTitle">
      <span>
        {parent}
        {' '}
        /
        {child}
      </span>
    </div>
    {item.map((elem) => (
      <ResultItem item={elem} mark={mark} key={elem.caption + Math.random()} />
    ))}
  </div>
);

ResultItem.propTypes = {
  item: PropTypes.shape({
    href: PropTypes.string.isRequired,
    caption: PropTypes.string.isRequired,
  }).isRequired,
  mark: PropTypes.string.isRequired,
};

SearchResult.propTypes = {
  parent: PropTypes.string.isRequired,
  child: PropTypes.string.isRequired,
  item: PropTypes.instanceOf(Array).isRequired,
  mark: PropTypes.string.isRequired,
};

export default SearchResult;
