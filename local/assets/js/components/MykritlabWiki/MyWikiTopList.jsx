import React from 'react';
import PropTypes from 'prop-types';
import Favorite from '../Favorite';

const MyWikiTopListItem = ({ item, event }) => {
  const dateChange = item.changeDate;
  return (
    <div className="wikiListBlockItem">
      <a href={item.href} title={item.name} className="wikiListBlockItem__wrap">
        <div className="wikiListBlockItem__name">
          <div className="wikiListBlockItem__link">{item.name}</div>
        </div>
        <div className="wikiListBlockItem__dates">
          <div className="wikiListBlockItem__date wikiListBlockItem__date--public">
            <span className="wikiListBlockItem__dateCaption wikiListBlockItem__dateCaption--short">
              Публ.:
            </span>
            <span className="wikiListBlockItem__dateCaption wikiListBlockItem__dateCaption--full">
              Публикация:
            </span>
            {item.date}
          </div>
          {dateChange.length > 0 && (
            <div className="wikiListBlockItem__date wikiListBlockItem__date--change">
              <span className="wikiListBlockItem__dateCaption wikiListBlockItem__dateCaption--short">
                Изм.:
              </span>
              <span className="wikiListBlockItem__dateCaption wikiListBlockItem__dateCaption--full">
                Изменено:
              </span>
              {item.changeDate}
            </div>
          )}
        </div>
      </a>
      <Favorite isFavorite={item.favorite} event={event} dataId={item.id} />
    </div>
  );
};

const MyWikiTopList = ({ items, event }) => (
  <div className="wikiListBlock__col">
    {items.map((item) => (
      <MyWikiTopListItem event={event} item={item} key={item.name + Math.random()} />
    ))}
  </div>
);

MyWikiTopListItem.propTypes = {
  item: PropTypes.shape({
    changeDate: PropTypes.string.isRequired,
    href: PropTypes.string.isRequired,
    name: PropTypes.string.isRequired,
    date: PropTypes.string.isRequired,
    favorite: PropTypes.bool.isRequired,
    id: PropTypes.number.isRequired,
  }).isRequired,
  event: PropTypes.oneOfType([PropTypes.func, PropTypes.bool]),
};

MyWikiTopList.propTypes = {
  items: PropTypes.instanceOf(Array).isRequired,
  event: PropTypes.oneOfType([PropTypes.func, PropTypes.bool]),
};

MyWikiTopListItem.defaultProps = {
  event: false,
};

MyWikiTopList.defaultProps = {
  event: false,
};

export default MyWikiTopList;
