import MainLayout from "../../components/MainLayout";

const About = () => {
    return (
        <MainLayout title={'AboutPage | NextJS'}>
            <h1 className={'about_title'}>About Page | NEXT JS</h1>
            <p className='about_text'>ТЕСТОВОЕ ПРИЛОЖЕНИЕ НА NEXT JS</p>
        </MainLayout>
    )
}
export default About