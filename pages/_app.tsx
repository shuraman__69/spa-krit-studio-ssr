import '../local/assets/scss/entries/global.scss'

import React, {useEffect} from 'react';
import {AppStateType, wrapper} from "../redux/store";
import {useSelector} from "react-redux";
import {useRouter} from "next/router";

const WrappedApp = ({Component, pageProps}) => {
    const {name} = useSelector((state: AppStateType) => state.authPage.user)
    const router = useRouter()
    useEffect(() => {
        if (name === '') {
            router.push('/auth')
        }
    },[name])

    return (
        <Component {...pageProps} />
    );
}


export default wrapper.withRedux(WrappedApp);

