import React from 'react';
import MainLayout from "../../../components/MainLayout";
import MykritlabDetail from "../../../components/MykritlabDetail/MykritlabDetail";
import {NextThunk, wrapper} from "../../../redux/store";
import {setSelectedItemThunk, setSelectedSectionThunk} from "../../../redux/reducers/wikiReducer";
import {getUserInfoThunk} from "../../../redux/reducers/authReducer";

const Index = () => {
    return (
        <MainLayout title='Item | NextJS'>
            <MykritlabDetail />
        </MainLayout>
    );
}
export default Index;

export const getServerSideProps = wrapper.getServerSideProps(async ({store, params,}) => {
    const dispatch = store.dispatch as NextThunk
    const id:string=params.id.toString()
    await dispatch(await getUserInfoThunk())

    await dispatch(setSelectedItemThunk(id))
    return {
        props: {
        }
    }
})
