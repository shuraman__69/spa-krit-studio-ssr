import React, {useEffect} from "react"
import MainLayout from "../components/MainLayout";
import {AppStateType, NextThunk, wrapper} from "../redux/store";
import {useRouter} from "next/router";
import {useSelector} from "react-redux";
import MyKritLabMenu from "../components/MyKritLab/MyKritLabMenu";
import {getMenuThunk} from "../redux/reducers/menuRudecer";
import {getUserInfoThunk} from "../redux/reducers/authReducer";


const Main = () => {
    const router = useRouter()
    const name = useSelector((state: AppStateType) => state.authPage.user.name)
    useEffect(() => {
        if (!name) {
            router.push('/auth')
        }
    }, [])
    return (
        <MainLayout title={'MainPage | NextJS'}>
            <MyKritLabMenu/>
        </MainLayout>
    )
}

export const getStaticProps = wrapper.getStaticProps(async ({store}) => {
    const dispatch = store.dispatch as NextThunk
    await dispatch(await getUserInfoThunk())
    await dispatch(await getMenuThunk())
})


export default Main
