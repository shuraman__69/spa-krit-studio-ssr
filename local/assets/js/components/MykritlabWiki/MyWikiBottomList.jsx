import React from 'react';
import PropTypes from 'prop-types';
import classNames from 'classnames';

const MyWikiBottomItem = ({ item }) => (
  <div
    className={classNames({
      wikiCategoryItem: true,
      'wikiCategoryItem--bordered': item.extranet,
      'wikiCategoryItem--highlighted': item.highlighted,
    })}
  >
    <a href={item.href} className="wikiCategoryItem__link">
      <div className="wikiCategoryItem__name">{item.name}</div>
      <div className="wikiCategoryItem__count">{item.count}</div>
    </a>
  </div>
);

const MyWikiBottomList = ({ category }) => (
  <div className="wikiCategoryList">
    {category.map((item) => (
      <MyWikiBottomItem item={item} key={item.name + Math.random()} />
    ))}
  </div>
);

MyWikiBottomItem.propTypes = {
  item: PropTypes.shape({
    extranet: PropTypes.string,
    name: PropTypes.string.isRequired,
    highlighted: PropTypes.string,
    href: PropTypes.string.isRequired,
    count: PropTypes.string.isRequired,
  }).isRequired,
};

MyWikiBottomList.propTypes = {
  category: PropTypes.instanceOf(Array).isRequired,
};

export default MyWikiBottomList;
