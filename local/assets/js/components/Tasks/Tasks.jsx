import React, { Component } from 'react';
import Menu from '../Menu';
import Content from './Content';
import TasksMenu from '../TasksMenu';

class Tasks extends Component {
  constructor(props) {
    super(props);
    this.state = {
      users: [],
      tasks: [],
    };
    this.section = document.getElementById('tasks').getAttribute('data-section');
  }

  componentDidMount() {
    this.getItems();
  }

  getItems() {
    const self = this;
    const xhr = new XMLHttpRequest();

    const formData = new FormData();

    formData.append('AJAX', 'Y');

    xhr.open('GET', '../../api/v3/tasks/tasks.json');
    xhr.send(formData);

    xhr.onload = function (response) {
      const result = JSON.parse(response.currentTarget.response);

      if (xhr.status === 200) {
        self.setState({
          users: result.users,
          tasks: result.tasks,
        });
      }
    };

    xhr.onerror = function () {
      console.log('ошибка');
    };
  }

  render() {
    const { users, tasks } = this.state;
    return (
      <div className="tasks">
        <Menu selected={this.section} items={TasksMenu} />
        {users.length > 0 && tasks.length > 0
          && <Content selected={this.section} menu={TasksMenu} users={users} tasks={tasks} />}
      </div>
    );
  }
}

export default Tasks;
