import React from 'react';
import PropTypes from 'prop-types';
import VoitingItem from './VoitingItem';

import { getSuffix } from '../../../modules/util';

const setVote = (e) => {
  e.preventDefault();
  const isVoted = document.querySelector('.post__rating').classList.contains('post__rating--voted');
  const item = e.currentTarget;
  const itemCount = item.querySelector('.js-count-rating').innerHTML;
  let likeText = '';
  const titles = ['голос', 'голоса', 'голосов'];

  if (!isVoted) {
    item.querySelector('.js-count-rating').innerHTML = `${Number(itemCount) + 1} `;
    likeText = getSuffix(titles, Number(itemCount) + 1);
    item.querySelector('.js-count-text').innerHTML = likeText;

    item.classList.add('post__ratingItem--voted');
    document.querySelector('.post__rating').classList.add('post__rating--voted');
    const data = {
      articleID: document.getElementById('article-detail').getAttribute('data-id'),
      voteID: e.currentTarget.getAttribute('data-id'),
    };
    const xhr = new XMLHttpRequest();

    const formData = new FormData();

    formData.append('AJAX', 'Y');
    formData.append('PARAMS', JSON.stringify(data));

    xhr.open('POST', '/');
    xhr.send(formData);

    // xhr.onload = function (response) {
    //   const result = JSON.parse(response.currentTarget.response);

    //   if (xhr.status === 200) {
    //   }
    // };

    xhr.onerror = function () {
      console.log('ошибка');
    };
  }
};

const Voiting = ({ items }) => {
  let isVoted = 0;
  for (let i = 0; i < items.length; i += 1) {
    if (items[i].isVoted) {
      isVoted += 1;
    }
  }
  if (isVoted) {
    return (
      <div className="post__rating post__rating--voted">
        {items.map((item) => (
          <VoitingItem item={item} key={item.voteID + Math.random()} />
        ))}
      </div>
    );
  }
  return (
    <div className="post__rating">
      {items.map((item) => (
        <VoitingItem item={item} event={(e) => setVote(e)} key={item.voteID + Math.random()} />
      ))}
    </div>
  );
};

Voiting.propTypes = {
  items: PropTypes.instanceOf(Array).isRequired,
};

export default Voiting;
