import React from 'react';
import PropTypes from 'prop-types';

const AccessesItem = ({ item }) => (
  <div className="accesses__item">
    <div className="accesses__itemCol">
      <div className="accesses__itemInner">
        <a href={item.href} className="accesses__itemName">
          {item.name}
        </a>
        <div className="accesses__itemDate">{item.date}</div>
      </div>
    </div>
    <div className="accesses__itemCol accesses__itemCol--gray">
      <div className="accesses__itemInner">{item.creator}</div>
    </div>
    <div className="accesses__itemCol accesses__itemCol--site">
      <div className="accesses__itemInner">
        {!item.site && '\u2014'}
        {item.site && (
          <a href={item.siteurl} className="accesses__itemSite">
            {item.site}
          </a>
        )}
      </div>
    </div>
  </div>
);

AccessesItem.propTypes = {
  item: PropTypes.shape({
    href: PropTypes.string.isRequired,
    name: PropTypes.string.isRequired,
    creator: PropTypes.string.isRequired,
    date: PropTypes.string.isRequired,
    site: PropTypes.string.isRequired,
    siteurl: PropTypes.string.isRequired,
  }).isRequired,
};

export default AccessesItem;
