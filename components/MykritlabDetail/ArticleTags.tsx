import React, {FC} from 'react';
import PropTypes from 'prop-types';

type PropsType = {
    items: string[]
}
const ArticleTags: FC<PropsType> = ({items}) => {
    return (
        <div className="post__bottomCol post__tags">
            <h5>Теги</h5>
            {items.map((item) => (
                <a
                    className="post__tagsItem"
                    key={item + Math.random()}
                    href={`/wiki/?search=${item}`}
                >
                    #{item}
                </a>
            ))}
        </div>
    );
}

export default ArticleTags;
