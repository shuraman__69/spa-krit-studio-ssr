import React from 'react';
import TasksItems from './TasksItems';

function SupportCalendar() {
  return (
    <>
      <div className="supportCalendar">
        <div className="wrapper">
          <div className="supportCalendar-header">
            <div className="supportCalendar-header__date">
              Февраль
              <span className="year">, 2020</span>
            </div>
            <div className="supportCalendar-change">
              <a href="# " className="supportCalendar-change__button prev">
                <svg width="9" height="14" fill="none" xmlns="http://www.w3.org/2000/svg"><path d="M8 13L2 7l6-6" stroke="#3262E0" strokeWidth="2" /></svg>
              </a>
              <div className="supportCalendar-header__date">
                Март
                <span className="year">, 2020</span>
              </div>
              <a href="# " className="supportCalendar-change__button next disabled">
                <svg width="9" height="14" fill="none" xmlns="http://www.w3.org/2000/svg"><path d="M8 13L2 7l6-6" stroke="#3262E0" strokeWidth="2" /></svg>
              </a>
            </div>
            <div className="supportCalendar-header__date">
              Апрель
              <span className="year">, 2020</span>
            </div>
          </div>
        </div>
        <SupportCalendarMain />
      </div>
      <SupportTasks />
      <TasksItems />
    </>
  );
}

function SupportCalendarMain() {
  return (
    <div className="overflow-auto-xs overflow-auto-sm wrapper table-wrapper">
      <table className="ui-table calendarTable">
        <thead>
          <tr>
            <th>Клиент</th>
            <th className="bg-gray ui-table-round-left">
              Запланировано
            </th>
            <th className="bg-gray">
              Осталось
            </th>
            <th className="bg-gray">
              Сверх пакета
            </th>
            <th className="bg-gray ui-table-round-right">
              На контроле
            </th>
            <th className="ui-table-underline-blue">
              Пакет
            </th>
            <th className="ui-table-underline-blue">
              Отгружено
            </th>
          </tr>
        </thead>
        <tbody>
          <SupportCalendarMainItem />
          <SupportCalendarMainItem />
          <SupportCalendarMainItem />
        </tbody>
      </table>
    </div>
  );
}

function SupportCalendarMainItem() {
  return (
    <tr>
      <td>
        <div className="itemDetail">
          <div className="itemDetail-wrapper">
            <div className="itemDetail__name">Комус-Реклама</div>
            <div className="itemDetail__task">
              Задач:
              <span> 3</span>
            </div>
          </div>
          <a href="# " className="itemDetail__icon" download>
            <svg width="14" height="16" fill="none" xmlns="http://www.w3.org/2000/svg"><path d="M10.589 4.132a1.084 1.084 0 01-1.082-1.083V0H1.742C.782 0 0 .782 0 1.742v12.453c0 .961.782 1.742 1.742 1.742h10.212c.96 0 1.742-.781 1.742-1.742V4.132H10.59zm-1.067 7.946H3.508a.467.467 0 110-.934h6.014a.467.467 0 110 .934zM3.04 9.743c0-.258.209-.467.467-.467h5.366a.467.467 0 110 .934H3.508a.467.467 0 01-.467-.467zm7.048-1.4h-6.58a.467.467 0 110-.935h6.581a.467.467 0 110 .934z" fill="#B6C2E2" /></svg>
          </a>
        </div>
      </td>
      <td className="bg-gray">200</td>
      <td className="bg-gray">200</td>
      <td className="bg-gray">200</td>
      <td className="bg-gray">200</td>
      <td>200</td>
      <td>200</td>
    </tr>
  );
}

function SupportTasks() {
  return (
    <div className="wrapper">
      <h2 className="title tasksContent__title support-info--title">Задачи отчетного месяца, без крайнего срока</h2>
      <div className="support-info">
        <div className="support-info__icon">
          <svg width="68" height="55" fill="none" viewBox="0 0 68 55" xmlns="http://www.w3.org/2000/svg">
            <path d="M29.595 26.762h-3.562v-1.357a1.406 1.406 0 00-2.81 0v1.357H7.767v-1.357a1.406 1.406 0 00-2.81 0v1.357H1.405c-.77 0-1.405.625-1.405 1.406v25.426C0 54.366.624 55 1.405 55h28.19c.77 0 1.405-.625 1.405-1.406V28.168a1.4 1.4 0 00-1.405-1.406zM8.792 49.153H6.82a1.689 1.689 0 010-3.377h1.97c.928 0 1.689.752 1.689 1.689a1.702 1.702 0 01-1.688 1.688zm0-6.578H6.82a1.689 1.689 0 010-3.378h1.97c.928 0 1.689.752 1.689 1.689a1.702 1.702 0 01-1.688 1.689zm0-6.58H6.82a1.689 1.689 0 010-3.376h1.97c.928 0 1.689.751 1.689 1.688a1.695 1.695 0 01-1.688 1.689zm7.864 13.1h-1.97a1.689 1.689 0 010-3.377h1.97c.927 0 1.688.751 1.688 1.688 0 .927-.76 1.689-1.688 1.689zm0-6.579h-1.97a1.689 1.689 0 010-3.377h1.97a1.689 1.689 0 010 3.377zm0-6.579h-1.97a1.689 1.689 0 010-3.377h1.97a1.689 1.689 0 010 3.377zm4.157 11.41c0-.927.751-1.688 1.688-1.688h1.99c.918 0 1.67.742 1.689 1.66a1.683 1.683 0 01-1.688 1.717h-1.99a1.683 1.683 0 01-1.689-1.688zm0-6.568c0-.928.751-1.689 1.688-1.689h1.99c.918 0 1.67.742 1.689 1.66a1.683 1.683 0 01-1.688 1.717h-1.99a1.695 1.695 0 01-1.689-1.688zm0-6.58c0-.926.751-1.688 1.688-1.688h1.99c.918 0 1.67.742 1.689 1.66a1.683 1.683 0 01-1.688 1.718h-1.99a1.695 1.695 0 01-1.689-1.69z" fill="#C4A2FC" />
            <path d="M65.043 4.96H54.696l-2.07-2.778a2.952 2.952 0 00-2.365-1.19H39.888L39.79.88A3.007 3.007 0 0037.683 0H21.681a2.959 2.959 0 00-2.956 2.975v31.739a2.959 2.959 0 002.956 2.975h43.362A2.959 2.959 0 0068 34.714V7.934a2.959 2.959 0 00-2.957-2.975zM50.261 2.974a.99.99 0 01.788.397l1.183 1.587h-8.044c-.259 0-.518-.112-.703-.297l-1.65-1.687h8.426zm15.768 31.739a.992.992 0 01-.986.991H21.681a.992.992 0 01-.985-.991V2.975c0-.545.443-.991.985-.991h16.002c.259 0 .518.111.703.297l3.695 3.781c.554.57 1.318.88 2.107.88h20.855c.542 0 .986.447.986.992v26.78z" fill="#B6C2E2" />
            <path d="M62.087 23.805H45.333a.992.992 0 00-.985.992v5.95c0 .546.443.992.985.992h16.754a.992.992 0 00.985-.992v-5.95a.992.992 0 00-.985-.992zm-.986 5.95H46.32v-3.967H61.1v3.968z" fill="#B6C2E2" />
          </svg>
        </div>
        <div className="support-info__title">Важно!</div>
        <div className="support-info__text">Для того, чтобы задачи считались отгруженными, им необходимо указать крайний срок с месяцем, в отчет которого они должны попасть.</div>
      </div>
    </div>
  );
}

export default SupportCalendar;
