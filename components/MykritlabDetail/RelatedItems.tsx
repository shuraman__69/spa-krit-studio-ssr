import React, {FC} from 'react';
import PropTypes from 'prop-types';
import Link from "next/link";

type PropsType = {
    items: any[]
}
const RelatedItems: FC<PropsType> = ({items}) => (
    <div className="post__bottomCol post__related">
        <h5>Связанные статьи</h5>
        {items.map((item) => (
            <div className="post__relatedItem" key={item.name + Math.random()}>
                <Link href={item.href}>
                    <a className="post__relatedLink" title={item.name}>
                        {item.name}
                    </a>
                </Link>
            </div>
        ))}
    </div>
);

export default RelatedItems;
