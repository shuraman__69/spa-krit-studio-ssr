import React from 'react';
import Select from 'react-select';
import PropTypes from 'prop-types';

const SelectRender = ({ select, onChange, value }) => (
  <Select
    options={select}
    onChange={onChange}
    value={value}
    isSearchable={false}
    className="styledSelect"
    classNamePrefix="styledSelect"
  />
);

SelectRender.propTypes = {
  select: PropTypes.instanceOf(Array).isRequired,
  onChange: PropTypes.func.isRequired,
  value: PropTypes.instanceOf(Object).isRequired,
};

export default SelectRender;
