import {InferActionsType} from "../store";
import {accessesAPI} from "../../api/api";
import {accessesItemType, selectedAccessDataType, selectedAccessItemType} from "../../_types";
import {AddOrEditFormType} from "../../hooks/useAccess";
import {SET_ERROR} from './authReducer'


const SET_ACCESSES_ITEMS = 'SET_ACCESSES_ITEMS'
const SET_SELECTED_ACCESS_ITEM = 'SET_SELECTED_ACCESS_ITEM'
const SET_SELECTED_ACCESS = 'SET_SELECTED_ACCESS'

export type InitialAuthStateType = {
    items: accessesItemType[]
    totalPages: number
    currentPage: number
    selectedAccessItem: {
        items: selectedAccessItemType[]
        data: selectedAccessDataType
    },
    selectedAccess: selectedAccessItemType
    contragentID: number

};
const initialState: InitialAuthStateType = {
    items: [],
    totalPages: 0,
    currentPage: 1,
    selectedAccessItem: {
        items: [],
        data: {
            creator: '',
            date: '',
            href: '',
            id: '',
            name: '',
            site: '',
        }
    },
    selectedAccess: {
        canChange: false,
        database: '',
        host: '',
        id: 0,
        like: false,
        login: '',
        notice: '',
        password: '',
        port: '',
        type: ''
    },
    contragentID: 0,
}

const accessesReducer = (state = initialState, action: ActionsType) => {
    switch (action.type) {
        case SET_ACCESSES_ITEMS:
            return {
                ...state,
                items: [...action.items],
                totalPages: action.totalPages,
                currentPage: action.currentPage
            }
        case SET_SELECTED_ACCESS_ITEM:
            return {
                ...state,
                selectedAccessItem: {
                    items: [...action.items],
                    data: {...action.data}
                }
            }
        case SET_SELECTED_ACCESS:
            return {
                ...state,
                selectedAccess: action.item,
                contragentID: action.contragentID
            }
        default:
            return state;
    }
}
type ActionsType = InferActionsType<typeof actions>
const actions = {
    setContrAgentItems: (items: accessesItemType[], totalPages: number, currentPage: number) => ({
        type: SET_ACCESSES_ITEMS,
        items,
        totalPages,
        currentPage
    }) as const,
    setSelectedAccessItem: (items: selectedAccessItemType[], data: selectedAccessDataType) => ({
        type: SET_SELECTED_ACCESS_ITEM,
        items,
        data
    }) as const,
    setSelectedAccess: (item: selectedAccessItemType, contragentID: number) => ({
        type: SET_SELECTED_ACCESS,
        item,
        contragentID
    }) as const,
}
export const getAccessesItemsThunk = () => {
    return async (dispatch: any) => {
        try {
            let {items, currentPage, totalPages, status} = await accessesAPI.getAccessesItems()
            if (status === 200) {
                dispatch(actions.setContrAgentItems(items, totalPages, currentPage))
            }
        } catch (error) {
            await dispatch(SET_ERROR())
        }
    }
}
export const changePageThunk = (page: number) => {
    return async (dispatch: any) => {
        try {
            let {items, currentPage, totalPages} = await accessesAPI.changePage(page)
            dispatch(actions.setContrAgentItems(items, totalPages, currentPage))
        } catch (error) {
            await dispatch(SET_ERROR())
        }
    }
}
export const searchThunk = (value: string) => {
    return async (dispatch: any) => {
        try {
            let {items, currentPage, totalPages} = await accessesAPI.contrAgentSearch(value)
            dispatch(actions.setContrAgentItems(items, totalPages, currentPage))
        } catch (error) {
            await dispatch(SET_ERROR())
        }
    }
}
export const searchInsideContrAgentThunk = (id: string, value: string) => {
    return async (dispatch: any) => {
        try {
            let {items, data} = await accessesAPI.accessSearch(id, value)
            await dispatch(actions.setSelectedAccessItem(items, data))
        } catch (error) {
            console.log(error)
            await dispatch(SET_ERROR())
        }
    }
}
export const getSelectedAccessItemThunk = (id: string) => {
    return async (dispatch: any) => {
        try {
            let {items, data, status} = await accessesAPI.getSelectedAccessItem(id)
            if (status === 200) {
                dispatch(actions.setSelectedAccessItem(items, data))
            }
        } catch (error) {
            await dispatch(SET_ERROR())
        }
    }
}
export const setSelectedAccess = (id: number | string, contragentID: string) => {
    return async (dispatch: any, getState: any) => {
        try {
            const accessesItem = getState().accesses.selectedAccessItem
            accessesItem.items.forEach((item: selectedAccessItemType) => {
                if (item.id === id) {
                    dispatch(actions.setSelectedAccess(item, +contragentID))
                }
                return false
            })
        } catch (error) {
            await dispatch(SET_ERROR())
        }
    }
}
export const removeAccessThunk = () => {
    return async (dispatch: any, getState: any) => {

        try {
            const {id} = getState().accesses.selectedAccess
            const contragentID = getState().accesses.contragentID
            await accessesAPI.deleteAccess(id, contragentID)
            dispatch(getSelectedAccessItemThunk(contragentID))
        } catch (error) {
            await dispatch(SET_ERROR())
        }
    }
}
export const addAccessThunk = (form: AddOrEditFormType, id: string) => {
    return async (dispatch: any, getState: any) => {

        try {
            await accessesAPI.addAccess(form, id)
            dispatch(getSelectedAccessItemThunk(id))
        } catch (error) {
            await dispatch(SET_ERROR())
        }
    }
}
export const saveAccessEditsThunk = (form: AddOrEditFormType, id: string, contragentID: string) => {
    return async (dispatch: any, getState: any) => {
        try {
            await accessesAPI.saveAccessEdits(form, contragentID, +id)
            dispatch(getSelectedAccessItemThunk(contragentID))
        } catch (error) {
            await dispatch(SET_ERROR())
        }
    }
}
export const likeEventThunk = (id: number, contragentID: string, itemLike: boolean) => {
    return async (dispatch: any) => {
        try {
            await accessesAPI.likeEvent(id, contragentID, itemLike)
            dispatch(getSelectedAccessItemThunk(contragentID))
        } catch (error) {
            await dispatch(SET_ERROR())
        }
    }
}


export default accessesReducer;