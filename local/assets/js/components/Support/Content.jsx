import React, { Component } from 'react';
import PropTypes from 'prop-types';
import Parser from 'html-react-parser';

import SupportData from './SupportData';
import { slideToggle } from '../../modules/util';

class Content extends Component {
  constructor(props) {
    super(props);
    this.state = {
      data: [],
      dataIsOpen: false,
    };
    this.section = document.getElementById('support').getAttribute('data-section');
  }

  componentDidMount() {
    const { data } = this.props;
    this.setState({
      data,
    });
  }

  handleClick() {
    const list = document.querySelector('.js-hidden-users');
    slideToggle(list, 500);
    this.setState((prevState) => ({ dataIsOpen: !prevState.dataIsOpen }));
  }

  getTitle() {
    const { selected, menu } = this.props;
    const itemMenu = menu.filter((item) => item.id.toString() === selected);

    return itemMenu[0].title;
  }

  render() {
    const { data, dataIsOpen } = this.state;
    return (
      <div className="wrapper">
        <div className="tasksContent">
          <h2 className="title tasksContent__title">{Parser(this.getTitle())}</h2>
          {data.length > 0 && (
            <SupportData data={data} event={(e) => this.handleClick(e)} click={(e) => this.handleClick(e)} isOpen={dataIsOpen} />
          )}
        </div>
      </div>
    );
  }
}

Content.propTypes = {
  data: PropTypes.instanceOf(Array).isRequired,
  selected: PropTypes.string.isRequired,
  menu: PropTypes.instanceOf(Array).isRequired,
};

export default Content;
