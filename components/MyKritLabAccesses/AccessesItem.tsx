import React, {FC} from 'react';
import {accessesItemType} from "../../_types";
import Link from "next/link";

type PropsType = {
    item: accessesItemType
}
const AccessesItem: FC<PropsType> = ({item}) => (
    <div className="accesses__item">
        <div className="accesses__itemCol">
            <div className="accesses__itemInner">
                <Link href={item.href}>
                    <a className="accesses__itemName">
                        {item.name}
                    </a>
                </Link>
                <div className="accesses__itemDate">{item.date}</div>
            </div>
        </div>
        <div className="accesses__itemCol accesses__itemCol--gray">
            <div className="accesses__itemInner">{item.creator}</div>
        </div>
        <div className="accesses__itemCol accesses__itemCol--site">
            <div className="accesses__itemInner">
                {!item.site && '\u2014'}
                {item.site && (
                    <a href={item.site} className="accesses__itemSite">
                        {item.site}
                    </a>
                )}
            </div>
        </div>
    </div>
);

export default AccessesItem;
