import React, {FC, useEffect} from 'react';
import Parser from 'html-react-parser';
import {toast, Toaster} from "react-hot-toast";


const copyClick = (e: any) => {
    e.preventDefault();
    window.getSelection()!.removeAllRanges();
    const copyText = e.target.closest('code')
    const range = document.createRange();
    range.selectNode(copyText);
    window!.getSelection()!.addRange(range);
    document.execCommand('copy');
    window!.getSelection()!.removeAllRanges();
    toast.success('Скопировано!')
};
// @ts-ignore
const Content: FC<{ content: string }> = ({content}) => {
    useEffect(() => {
        const codes = document.querySelectorAll("code")

        if (codes.length > 0) {
            codes.forEach(item => {
                item.addEventListener("dblclick", copyClick
                )
            })
        }
    }, [])
    return (
        <div className='post__content post__contentText'>
            {Parser(content)}
            <div><Toaster
                position="top-right"
                reverseOrder={false}
            /></div>
        </div>

    );
}

export default Content;
