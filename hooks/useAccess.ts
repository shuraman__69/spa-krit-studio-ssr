import {FormEvent, useState} from 'react'
import {useDispatch, useSelector} from "react-redux";
import {addAccessThunk, saveAccessEditsThunk} from "../redux/reducers/accessesReducer";
import {useRouter} from "next/router";
import {AppStateType} from "../redux/store";


export type AddOrEditFormType = {
    login: string
    password: string
    type: string
    host: string
    notice?: string | ''
    port?: string | ''
    database?: string | ''
}

export const useAccess = () => {
    const dispatch = useDispatch()
    const {query} = useRouter()
    const {
        login,
        password,
        type,
        database,
        port,
        notice,
        host
    } = useSelector((state: AppStateType) => state.accesses.selectedAccess)
    const [form, setForm] = useState<AddOrEditFormType>({
        login: login,
        password: password,
        type: type,
        database: database,
        port: port,
        notice: notice,
        host: host,
    })

    const onChangeHandler = (event: any) => {
        if (event.target !== undefined) {
            setForm({...form, [event.target.name]: event.target.value})
        } else {
            setForm({...form, type: event.label})
        }
    }
    const saveAccessEdits = (id: string) => {

        dispatch(saveAccessEditsThunk(form, id, query.id.toString()))

    }
    const addAccess = () => {
        dispatch(addAccessThunk(form, query.id.toString()))
    }

    return {onChangeHandler, addAccess, saveAccessEdits, ...form, setForm}
}