import {AnyAction, applyMiddleware, combineReducers, createStore} from 'redux';
import {Context, createWrapper, HYDRATE, MakeStore} from 'next-redux-wrapper';
import menuReducer from "./reducers/menuRudecer";
import authReducer from "./reducers/authReducer";
import wikiReducer from "./reducers/wikiReducer";
import accessesReducer from "./reducers/accessesReducer";
import thunk, {ThunkAction, ThunkDispatch} from "redux-thunk";

type RootReducerType = ReturnType<typeof rootReducer>
export type AppStateType = RootReducerType

type PropertiesType<T> = T extends { [key: string]: infer U } ? U : never
export type InferActionsType<T extends { [key: string]: (...args: any[]) => any }> = ReturnType<PropertiesType<T>>


const rootReducer = combineReducers({
    authPage: authReducer,
    data: menuReducer,
    wiki: wikiReducer,
    accesses: accessesReducer
})
const reducer = (state, action) => {
    if (action.type === HYDRATE) {
        const nextState = {
            ...state, // use previous state
            ...action.payload, // apply delta from hydration
        }
        if (state.count) nextState.count = state.count // preserve count value on client side navigation
        return nextState
    } else {
        return rootReducer(state, action)
    }
}

const makeStore: MakeStore<RootReducerType> = (context: Context) => createStore(reducer, applyMiddleware(thunk));

// export an assembled wrapper
export const wrapper = createWrapper<RootReducerType>(makeStore, {debug: true});

if(process.browser){
    // @ts-ignore
    window.store=makeStore()
}
export type NextThunk = ThunkDispatch<RootReducerType, void, AnyAction>