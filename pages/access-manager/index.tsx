import React from 'react';
import MainLayout from "../../components/MainLayout";
import {NextThunk, wrapper} from "../../redux/store";
import {getUserInfoThunk} from "../../redux/reducers/authReducer";
import Accesses from "../../components/MyKritLabAccesses/Accesses";
import {getAccessesItemsThunk} from "../../redux/reducers/accessesReducer";

const Index = () => (
    <MainLayout title='Access Manager | NextJS'>
        <Accesses/>
    </MainLayout>
);
export default Index;


export const getStaticProps = wrapper.getStaticProps(async ({store}) => {
    const dispatch = store.dispatch as NextThunk
    await dispatch(await getUserInfoThunk())
    await dispatch(await getAccessesItemsThunk())
})

