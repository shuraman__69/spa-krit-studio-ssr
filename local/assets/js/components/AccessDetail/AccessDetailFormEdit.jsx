import React from 'react';
import Select from 'react-select';
import PropTypes from 'prop-types';
import RenderModal from '../RenderModal';

const requiredFields = ['HOST', 'TYPE', 'LOGIN', 'PASSWORD'];

const hasValue = (element) => document.querySelector(`[name="${element}"]`).value.length > 0;

const onChangeInput = () => {
  setTimeout(() => {
    if (!requiredFields.every(hasValue)) {
      document.querySelector('.addNewAccess__btn').setAttribute('disabled', 'disabled');
    } else {
      document.querySelector('.addNewAccess__btn').removeAttribute('disabled');
    }
  }, 10);
};

const AccessDetailFormEdit = ({
  open,
  closeEvent,
  event,
  host,
  type,
  login,
  password,
  database,
  port,
  notice,
}) => (
  <RenderModal isOpen={open} eventClose={closeEvent}>
    <div className="editAccess">
      <form className="editAccess__form formBlock" onSubmit={event} data-form="edit">
        <div className="formBlock__title title">Редактировать доступ</div>
        <div className="formBlock__content">
          <div className="formBlock__row">
            <div className="formBlock__rowWrap">
              <div className="inputHolder">
                <input
                  type="text"
                  name="HOST"
                  className="inputHolder__input"
                  defaultValue={host}
                  placeholder="Хост"
                  onChange={(e) => onChangeInput(e)}
                />
              </div>
            </div>
          </div>
          <div className="formBlock__row">
            <div className="formBlock__rowWrap">
              <div className="inputHolder">
                <Select
                  name="TYPE"
                  options={[
                    {
                      label: 'CMS',
                      value: '1',
                    },
                    {
                      label: 'Basic auth',
                      value: '2',
                    },
                    {
                      label: 'SSH',
                      value: '3',
                    },
                    {
                      label: 'VPN',
                      value: '4',
                    },
                  ]}
                  placeholder="Вид доступа"
                  isSearchable={false}
                  className="styledSelect"
                  classNamePrefix="styledSelect"
                  onChange={(e) => onChangeInput(e)}
                  defaultValue={type}
                />
              </div>
            </div>
          </div>
          <div className="formBlock__row">
            <div className="formBlock__rowWrap">
              <div className="inputHolder">
                <input
                  type="text"
                  name="LOGIN"
                  className="inputHolder__input"
                  defaultValue={login}
                  placeholder="Логин"
                  onChange={(e) => onChangeInput(e)}
                />
              </div>
            </div>
          </div>
          <div className="formBlock__row">
            <div className="formBlock__rowWrap">
              <div className="inputHolder">
                <input
                  type="text"
                  name="PASSWORD"
                  className="inputHolder__input"
                  defaultValue={password}
                  placeholder="Пароль"
                  onChange={(e) => onChangeInput(e)}
                />
              </div>
            </div>
          </div>
          <div className="formBlock__row">
            <div className="formBlock__rowWrap">
              <div className="inputHolder">
                <input
                  type="text"
                  className="inputHolder__input"
                  defaultValue={database}
                  placeholder="База данных"
                />
              </div>
            </div>
          </div>
          <div className="formBlock__row">
            <div className="formBlock__rowWrap">
              <div className="inputHolder">
                <input
                  type="text"
                  name="PORT"
                  className="inputHolder__input"
                  defaultValue={port}
                  placeholder="Порт"
                />
              </div>
            </div>
          </div>
          <div className="formBlock__row formBlock__row--textarea">
            <div className="formBlock__rowWrap">
              <div className="inputHolder">
                <textarea
                  type="text"
                  name="COMMENT"
                  className="inputHolder__input inputHolder__input--textarea"
                  placeholder="Примечания"
                  defaultValue={notice}
                />
              </div>
            </div>
          </div>
          <div className="formBlock__row formBlock__row--btn">
            <div className="formBlock__rowWrap">
              <button type="submit" className="addNewAccess__btn">
                <span>
                  <svg
                    width="36"
                    height="13"
                    viewBox="0 0 31 11"
                    fill="none"
                    xmlns="http://www.w3.org/2000/svg"
                  >
                    <path
                      d="M1.25 6.0013H29.5833M29.5833 6.0013L25.4167 1.41797M29.5833 6.0013L25.4167 10.168"
                      stroke="white"
                      strokeWidth="1.5"
                      strokeLinecap="round"
                    />
                  </svg>
                </span>
                Сохранить
              </button>
            </div>
          </div>
        </div>
      </form>
    </div>
  </RenderModal>
);

AccessDetailFormEdit.propTypes = {
  open: PropTypes.bool.isRequired,
  closeEvent: PropTypes.func.isRequired,
  event: PropTypes.func.isRequired,
  host: PropTypes.string,
  type: PropTypes.string,
  login: PropTypes.string,
  password: PropTypes.string,
  database: PropTypes.string,
  port: PropTypes.string,
  notice: PropTypes.string,
};

AccessDetailFormEdit.defaultProps = {
  host: 'str',
  type: 'str',
  login: 'str',
  password: 'str',
  database: 'str',
  port: 'str',
  notice: 'str',
};
export default AccessDetailFormEdit;
