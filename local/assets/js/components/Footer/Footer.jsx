import React from 'react';
import Informer from './Informer';
import SwitchTheme from './SwitchTheme';

const Footer = () => (
  <div className="footer__info">
    <Informer />
    <SwitchTheme />
    <span className="footer__copyright"> © &nbsp;2016&nbsp;— 2020 Информация является конфиденциальной</span>
  </div>
);

export default Footer;
