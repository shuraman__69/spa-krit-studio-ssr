import React, {FC} from 'react';
import Content from './Content/Content';
import ArticleTags from './ArticleTags';
import RelatedItems from './RelatedItems';
import Links from './Links';
import PostInfo from './PostInfo';
import Voiting from './Voiting/Voiting';
import {setSelectedItemThunk} from "../../redux/reducers/wikiReducer";
import {useDispatch} from "react-redux";
import {selectedArticleType} from "../../_types";
import Favorite from "../other/Favorite";
import {wikiAPI} from "../../api/api";
import Structure from "./Structure/Structure";


const Detail: FC<selectedArticleType> = ({id, name, text, props, favorite, tags, links, relatedItems, voiting,navigation}) => {
    const dispatch = useDispatch()
    const detailFavoriteClick = async (fav: boolean, id: number) => {
        await wikiAPI.addFavorite(fav, id)
        dispatch(setSelectedItemThunk(id.toString()))
    }


    return (
        <div className="post">
            <h2 className="title post__title">{name}</h2>
            <div className="post__container">
                {navigation && <Structure content={navigation} />}
                {text && <Content content={text}/>}
                <div className="post__bottom">
                    <div className="post__bottomRow">
                        {tags !== null && <ArticleTags items={tags}/>}
                        {relatedItems.length > 0 && <RelatedItems items={relatedItems}/>}
                        {links !== null && <Links items={links}/>}
                    </div>
                    <div className="post__bottomRow">
                        {props && <PostInfo info={props}/>}
                        <div className="post__bottomActions">
                            <Favorite detailFavoriteClick={detailFavoriteClick} isFavorite={favorite} dataId={id}/>
                            {voiting && <Voiting articleID={id} items={voiting}/>}
                        </div>
                    </div>
                </div>
            </div>
        </div>
    );
}


export default Detail