import React from 'react';
import PropTypes from 'prop-types';
import classNames from 'classnames';
import InlineSVG from 'svg-inline-react';
import PerfectScrollbar from 'react-perfect-scrollbar';

import { getSuffix } from '../../../modules/util';

const VoitingItemUsers = ({ item }) => <div className="post__ratingItemUsersItem">{item}</div>;

const VoitingItem = (props) => {
  const { item, event } = props;
  let likeCount = 0;
  let likeText = 'голосов';
  const titles = ['голос', 'голоса', 'голосов'];

  if (item.users) {
    likeCount = item.users.length;
    likeText = getSuffix(titles, likeCount);
  }

  return (
    <div
      className={classNames({
        post__ratingItem: true,
        'post__ratingItem--dislike': item.voteID === 1,
        'post__ratingItem--like': item.voteID !== 1,
      })}
    >
      <div
        role="button"
        tabIndex={0}
        onKeyPress={(e) => event(e)}
        className={classNames({
          post__ratingItemWrap: true,
          'post__ratingItem--voted': item.isVoted,
        })}
        data-id={item.voteID}
        onClick={event}
      >
        <div className="post__ratingItemIcon">
          <InlineSVG src={item.icon} />
        </div>
        <div className="post__ratingItemText">{item.text}</div>
        <div className="post__ratingItemLike">
          <span className="js-count-rating">
            {likeCount}
            {' '}
          </span>
          <span className="js-count-text">{likeText}</span>
        </div>
      </div>
      {item.users.length > 0 && (
        <div className="post__ratingItemUsers">
          <div className="post__ratingItemUsersCaption">Проголосовали так же:</div>
          <PerfectScrollbar>
            <div className="post__ratingItemUsersWrap">
              {item.users.map((elem) => (
                <VoitingItemUsers item={elem} key={elem + Math.random()} />
              ))}
            </div>
          </PerfectScrollbar>
        </div>
      )}
    </div>
  );
};

VoitingItemUsers.propTypes = {
  item: PropTypes.string.isRequired,
};

VoitingItem.propTypes = {
  item: PropTypes.shape({
    users: PropTypes.instanceOf(Array).isRequired,
    voteID: PropTypes.number.isRequired,
    isVoted: PropTypes.bool.isRequired,
    icon: PropTypes.string.isRequired,
    text: PropTypes.string.isRequired,
  }).isRequired,
  event: PropTypes.func.isRequired,
};

export default VoitingItem;
