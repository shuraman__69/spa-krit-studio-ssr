import React from 'react';
import MainLayout from "../../../components/MainLayout";
import {NextThunk, wrapper} from "../../../redux/store";
import {setSelectedSectionThunk} from "../../../redux/reducers/wikiReducer";
import {getUserInfoThunk} from "../../../redux/reducers/authReducer";
import AccessDetail from "../../../components/MyKritLabAccesses/AccessDetail/AccessDetail";
import {getSelectedAccessItemThunk} from "../../../redux/reducers/accessesReducer";

const Index = () => {
    return (
        <MainLayout title='Access Detail | NextJS'>
            <AccessDetail/>
        </MainLayout>
    );
}
export default Index;


export const getServerSideProps = wrapper.getServerSideProps(async ({store, params,}) => {
    const dispatch = store.dispatch as NextThunk
    const id: string = params.id.toString()
    await dispatch(await getUserInfoThunk())
    await dispatch(getSelectedAccessItemThunk(id))
    return {
        props: {}
    }
})
