import React from 'react';
import Loader from "react-loader-spinner";


const FallBack = () => (
    <div className='loader-container' >
        <Loader type="Bars" color="#3262E0" height={80} width={80} />
    </div>
);



export default FallBack;
