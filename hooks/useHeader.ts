import React, {useState} from "react";
import {useRouter} from "next/router";

export const useHeader = () => {
    const [menuIsOpen, setMenuIsOpen] = useState<boolean>(true)
    const {pathname} = useRouter()
    const onClickHandler = () => {
        setMenuIsOpen(menuIsOpen)
        menuActions()
    }

    const menuActions = () => {
        const container = document.querySelector('html');
        if (menuIsOpen) {
            container!.classList[(!container!.classList.contains('menu-is-open')) ? 'add' : 'remove']('menu-is-open');
        } else {
            container!.classList.remove('menu-is-open');
        }
    }
    const onLinkClickHandler = () => {
        setMenuIsOpen(true)
        menuActions()
    }
    return {onClickHandler, pathname, onLinkClickHandler}

}