import React from 'react';
import PerfectScrollbar from 'react-perfect-scrollbar';
import PropTypes from 'prop-types';
import Parser from 'html-react-parser';

const OftenUsedRow = ({ item, event }) => (
  <div className="oftenUsed__tableRow js-copy-row">
    <div className="oftenUsed__tableCol oftenUsed__tableCol--type">
      <div className="oftenUsed__tableColInner">{Parser(item.type)}</div>
    </div>
    <div className="oftenUsed__tableCol oftenUsed__tableCol--host oftenUsed__tableCol--gray">
      <div className="oftenUsed__tableColInner js-copy-element" role="button" tabIndex={0} onKeyPress={(e) => event(e)} onClick={(e) => event(e)}>
        {item.host}
      </div>
    </div>
    <div className="oftenUsed__tableCol oftenUsed__tableCol--login">
      <div className="oftenUsed__tableColInner js-copy-element" role="button" tabIndex={0} onKeyPress={(e) => event(e)} onClick={(e) => event(e)}>
        {item.login}
      </div>
    </div>
    <div className="oftenUsed__tableCol oftenUsed__tableCol--pass">
      <div className="oftenUsed__tableColInner js-copy-element" role="button" tabIndex={0} onKeyPress={(e) => event(e)} onClick={(e) => event(e)}>
        {item.password}
      </div>
      <button type="button" className="accessDetail__icon copyIcon" onClick={(e) => event(e)}>
        <svg
          width="19"
          height="20"
          viewBox="0 0 19 20"
          fill="none"
          xmlns="http://www.w3.org/2000/svg"
        >
          <path d="M13.5776 19.0916H0.682338C0.305402 19.0916 0 18.7862 0 18.4093V5.51437C0 5.13743 0.305402 4.83203 0.682338 4.83203H13.5776C13.9545 4.83203 14.2599 5.13773 14.2599 5.51437V18.4093C14.2599 18.7859 13.9545 19.0916 13.5776 19.0916Z" />
          <path d="M18.3173 14.2599H16.9319C16.555 14.2599 16.2496 13.9542 16.2496 13.5776C16.2496 13.2006 16.555 12.8952 16.9319 12.8952H17.635V1.36497H6.10442V2.1241C6.10442 2.50103 5.79902 2.80643 5.42208 2.80643C5.04515 2.80643 4.73975 2.50074 4.73975 2.1241V0.682338C4.73975 0.305694 5.04515 0 5.42208 0H18.3173C18.6942 0 18.9997 0.305694 18.9997 0.682338V13.5776C18.9997 13.9542 18.6942 14.2599 18.3173 14.2599Z" />
        </svg>
      </button>
    </div>
  </div>
);
const OftenUsedList = ({ items, event }) => (
  <div className="accessDetail__oftenUsed oftenUsed">
    <PerfectScrollbar>
      <div className="oftenUsed__wrap">
        <div className="oftenUsed__table">
          <div className="oftenUsed__tableRow oftenUsed__tableRow--head">
            <div className="oftenUsed__tableCol oftenUsed__tableCol--type oftenUsed__tableCol--head">
              <div className="oftenUsed__tableColInner">Вид</div>
            </div>
            <div className="oftenUsed__tableCol oftenUsed__tableCol--host oftenUsed__tableCol--head">
              <div className="oftenUsed__tableColInner">Хост</div>
            </div>
            <div className="oftenUsed__tableCol oftenUsed__tableCol--login oftenUsed__tableCol--head">
              <div className="oftenUsed__tableColInner">Логин</div>
            </div>
            <div className="oftenUsed__tableCol oftenUsed__tableCol--pass oftenUsed__tableCol--head">
              <div className="oftenUsed__tableColInner">Пароль</div>
            </div>
          </div>
          {items.map((item) => (
            <OftenUsedRow item={item} event={event} key={item.id + Math.random()} />
          ))}
        </div>
      </div>
    </PerfectScrollbar>
  </div>
);

OftenUsedList.propTypes = {
  items: PropTypes.instanceOf(Array).isRequired,
  event: PropTypes.func.isRequired,
};
OftenUsedRow.propTypes = {
  item: PropTypes.shape({
    id: PropTypes.number.isRequired,
    host: PropTypes.string.isRequired,
    type: PropTypes.string.isRequired,
    login: PropTypes.string.isRequired,
    password: PropTypes.string.isRequired,
  }).isRequired,
  event: PropTypes.func.isRequired,
};

export default OftenUsedList;
