import React, { Component } from 'react';
import Logo from './Logo';
import User from './User';
import Icon from './Icon';
import Menu from './Menu';
import MobileMenu from './MobileMenu';

class Header extends Component {
  constructor(props) {
    super(props);
    this.state = {
      menu: [],
    };
    this.main = !!parseInt(document.getElementById('header').getAttribute('data-main'), 10);
  }

  componentDidMount() {
    if (!this.main) {
      this.getMenuItems();
    }
  }

  getMenuItems() {
    const self = this;
    const xhr = new XMLHttpRequest();

    const formData = new FormData();

    formData.append('AJAX', 'Y');

    xhr.open('GET', '../../api/v3/menu/main.json');
    xhr.send(formData);

    xhr.onload = function (response) {
      const result = JSON.parse(response.currentTarget.response);

      if (xhr.status === 200) {
        self.setState({
          menu: result,
        });
      }
    };

    xhr.onerror = function () {
      console.log('ошибка');
    };
  }

  render() {
    const { menu } = this.state;
    const menuActions = (e) => {
      e.preventDefault();
      const container = document.querySelector('html');
      if (e.currentTarget.classList.contains('js-open-side')) {
        container.classList[(!container.classList.contains('menu-is-open')) ? 'add' : 'remove']('menu-is-open');
      } else {
        container.classList.remove('menu-is-open');
      }
    };

    if (this.main) {
      return (
        <div className="header__wrapper">
          <Logo />
          <User />
        </div>
      );
    }
    return (
      <div className="header__main">
        <div className="header__wrapper header__wrapper--secondary">
          <div className="wrapper">
            <div className="header__left">
              <Icon clickEvent={(e) => menuActions(e)} />
              <Logo />
            </div>
            <div className="header__middle js-side-wrapper">
              <MobileMenu items={menu} clickEvent={(e) => menuActions(e)} />
              <div className="header__close">
                <button type="button" className="header__closeLink js-close-side" onClick={(e) => menuActions(e)} title="Закрыть">
                  <svg width="37" height="11" viewBox="0 0 37 11" fill="none" xmlns="http://www.w3.org/2000/svg"><path d="M1 10L18.5 1.5L35.5 10" stroke="#E7E7F4" strokeWidth="2" strokeLinecap="round" /></svg>
                </button>
              </div>
            </div>
            <div className="header__right">
              <User />
            </div>
          </div>
        </div>
        <div className="header__bottom js-side-wrapper">
          <div className="wrapper">
            <Menu items={menu} />
            <div className="header__close">
              <button type="button" className="header__closeLink js-close-side" onClick={(e) => menuActions(e)} title="Закрыть">
                <svg width="37" height="11" viewBox="0 0 37 11" fill="none" xmlns="http://www.w3.org/2000/svg"><path d="M1 10L18.5 1.5L35.5 10" stroke="#E7E7F4" strokeWidth="2" strokeLinecap="round" /></svg>
              </button>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

export default Header;
