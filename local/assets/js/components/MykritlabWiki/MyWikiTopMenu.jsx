import React from 'react';
import PropTypes from 'prop-types';
import classNames from 'classnames';

const MyWikiTopMenu = ({ item, event }) => (
  <li className="wikiListMenu__item">
    <button
      type="button"
      className={classNames({
        wikiListMenu__link: true,
        active: item.active,
      })}
      onClick={event}
      data-type={item.type}
      title={item.name}
    >
      <span className="wikiListMenu__linkText wikiListMenu__linkText--short">{item.shortName}</span>
      <span className="wikiListMenu__linkText wikiListMenu__linkText--full">{item.name}</span>
    </button>
  </li>
);

MyWikiTopMenu.propTypes = {
  item: PropTypes.shape({
    active: PropTypes.bool.isRequired,
    type: PropTypes.string.isRequired,
    name: PropTypes.string.isRequired,
    shortName: PropTypes.string.isRequired,
  }).isRequired,
  event: PropTypes.func.isRequired,
};

export default MyWikiTopMenu;
