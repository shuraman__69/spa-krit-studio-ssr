import React, { Component } from 'react';
import PropTypes from 'prop-types';
import Structure from './Structure/Structure';
import Content from './Content/Content';
import ArticleTags from './ArticleTags';
import RelatedItems from './RelatedItems';
import Links from './Links';
import PostInfo from './PostInfo';
import Favorite from '../Favorite';
import Voiting from './Voiting/Voiting';

class Detail extends Component {
  constructor(props) {
    super(props);
    this.setParams();
  }

  componentDidMount() {
    this.getContent();
  }

  setParams() {
    const self = this;

    self.state = {
      ID: self.props.detailID,
      content: [],
      props: {},
      voting: [],
      isFavorite: false,
      tags: [],
      relatedItems: [],
      links: [],
    };
  }

  getContent() {
    const self = this;
    const xhr = new XMLHttpRequest();

    const formData = new FormData();

    formData.append('AJAX', 'Y');
    formData.append('PARAMS', JSON.stringify({ ID: self.props.detailID }));

    xhr.open('GET', '../../api/v3/item/detail.json');
    xhr.send(formData);

    xhr.onload = function (response) {
      const result = JSON.parse(response.currentTarget.response);

      if (xhr.status === 200) {
        self.setState({
          content: result.content,
          props: result.props,
          voting: result.voiting,
          articleID: self.props.detailID,
          isFavorite: result.favorite,
          tags: result.tags,
          relatedItems: result.relatedItems,
          links: result.links,
        });
      }
    };

    xhr.onerror = function () {
      console.log('ошибка');
    };
  }

  render() {
    const {
      content, tags, relatedItems, links, props, voting, isFavorite, ID,
    } = this.state;
    const { title } = this.props;

    return (
      <div className="post">
        <h2 className="title post__title">{title}</h2>
        <div className="post__container">
          {content && <Structure content={content} />}
          {content && <Content content={content} />}
          <div className="post__bottom">
            <div className="post__bottomRow">
              {tags.length > 0 && <ArticleTags items={tags} />}
              {relatedItems.length > 0 && <RelatedItems items={relatedItems} />}
              {links.length > 0 && <Links items={links} />}
            </div>
            <div className="post__bottomRow">
              {props && <PostInfo info={props} />}
              <div className="post__bottomActions">
                <Favorite isFavorite={isFavorite} dataId={ID} />
                {voting && <Voiting items={voting} />}
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

Detail.propTypes = {
  title: PropTypes.string.isRequired,
};

export default Detail;
