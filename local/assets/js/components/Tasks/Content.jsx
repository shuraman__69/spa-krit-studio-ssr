import React, { Component } from 'react';
import PropTypes from 'prop-types';
import Parser from 'html-react-parser';
import TasksUsers from './TasksUsers';
import TasksItems from './TasksItems';
import { slideToggle } from '../../modules/util';

class Content extends Component {
  constructor(props) {
    super(props);
    this.state = {
      items: [],
      selectedItem: {},
      filter: [],
      selectedFilter: {},
      usersIsOpen: false,
      usersCount: window.innerWidth < 1024 ? 6 : window.innerWidth < 1440 ? 8 : 12,
    };
    this.section = document.getElementById('tasks').getAttribute('data-section');
  }

  componentDidMount() {
    const { users, tasks } = this.props;
    const items = this.getItems(users, tasks);
    const selectedItem = items.filter((item) => item.selected)[0];
    const filter = this.getFilter(selectedItem.tasks);
    const selectedFilter = filter.filter((item) => item.selected)[0];
    this.setState({
      items,
      selectedItem,
      filter,
      selectedFilter,
    });
    window.addEventListener('resize', this.setUserBlock);
  }

  componentWillUnmount() {
    window.removeEventListener('resize', this.setUserBlock);
  }

  handleChange(e) {
    const { items } = this.state;
    const itemsArr = items.concat();
    itemsArr.find(this.isSelected, e.target.value);
    const selectedItem = itemsArr.filter((item) => item.selected)[0];
    const filter = this.getFilter(selectedItem.tasks);
    const selectedFilter = filter.filter((item) => item.selected)[0];
    this.setState({
      items: itemsArr,
      selectedItem,
      filter,
      selectedFilter,
    });
  }

  handleClick() {
    const list = document.querySelector('.js-hidden-users');
    slideToggle(list, 500);
    this.setState((prevState) => ({ usersIsOpen: !prevState.usersIsOpen }));
  }

  getFilter(arr) {
    const array = arr.map((item) => ({ ...item }));
    for (let i = 0; i < array.length; i += 1) {
      delete array[i].code;
      delete array[i].parentID;
      delete array[i].producer;
      delete array[i].responsible;
      delete array[i].title;
      delete array[i].status;
      array[i].selected = false;
    }
    const select = this.removeDuplicates(array);
    const all = [
      {
        label: 'Все',
        value: 'allItems',
        selected: true,
      },
    ];
    const items = [...all, ...select];
    return items;
  }

  getItems(arr, arr2) {
    const array = arr.map((item) => ({ ...item }));
    for (let i = 0; i < array.length; i += 1) {
      array[i].tasks = arr2.filter((item) => item.parentID === array[i].idUser);
      array[i].selected = false;
    }

    const all = [
      {
        idUser: 'allItems',
        value: 'allItems',
        title: 'Все',
        selected: true,
        tasks: arr2,
      },
    ];
    const items = [...all, ...array];
    return items;
  }

  getTitle() {
    const { selected, menu } = this.props;
    const itemMenu = menu.filter((item) => item.id.toString() === selected);
    return itemMenu[0].title;
  }

  setUserBlock = () => {
    this.setState({
      usersCount: window.innerWidth < 1024 ? 6 : window.innerWidth < 1440 ? 8 : 12,
    });
  };

  filterChange(value) {
    const { filter } = this.state;
    const items = filter.concat();
    items.find(this.isSelected, value.value);
    const selectedFilter = items.filter((item) => item.selected)[0];
    this.setState({
      filter: items,
      selectedFilter,
    });
  }

  removeDuplicates(arr) {
    const result = [];
    const duplicatesIndices = [];

    arr.forEach((current, index) => {
      if (duplicatesIndices.includes(index)) return;
      result.push(current);

      for (let comparisonIndex = index + 1; comparisonIndex < arr.length; comparisonIndex += 1) {
        const comparison = arr[comparisonIndex];
        const currentKeys = Object.keys(current);
        const comparisonKeys = Object.keys(comparison);

        if (currentKeys.length !== comparisonKeys.length) continue;

        const currentKeysString = currentKeys.sort().join('').toLowerCase();
        const comparisonKeysString = comparisonKeys.sort().join('').toLowerCase();
        if (currentKeysString !== comparisonKeysString) continue;

        let valuesEqual = true;
        for (let i = 0; i < currentKeys.length; i += 1) {
          const key = currentKeys[i];
          if (current[key] !== comparison[key]) {
            valuesEqual = false;
            break;
          }
        }
        if (valuesEqual) duplicatesIndices.push(comparisonIndex);
      }
    });
    return result;
  }

  isSelected(element) {
    element.selected = element.value == this;
  }

  render() {
    const {
      items, selectedItem, filter, selectedFilter, usersIsOpen, usersCount,
    } = this.state;
    return (
      <div className="wrapper">
        <div className="tasksContent">
          <h2 className="title tasksContent__title">{Parser(this.getTitle())}</h2>
          {items.length > 0 && (
            <TasksUsers
              items={items}
              event={(e) => this.handleChange(e)}
              click={(e) => this.handleClick(e)}
              isOpen={usersIsOpen}
              count={usersCount}
            />
          )}
          {Object.keys(selectedItem).length > 0 && (
            <TasksItems
              item={selectedItem}
              select={filter}
              selected={selectedFilter}
              event={(e) => this.filterChange(e)}
            />
          )}
        </div>
      </div>
    );
  }
}

Content.propTypes = {
  users: PropTypes.instanceOf(Array).isRequired,
  tasks: PropTypes.instanceOf(Array).isRequired,
  menu: PropTypes.instanceOf(Array).isRequired,
  selected: PropTypes.string.isRequired,
};

export default Content;
